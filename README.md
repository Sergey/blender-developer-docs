# Blender Developer Documentation

Source files for the [Blender Developer
Documentation](https://developer.blender.org/docs/), built using [Material for
MkDocs](https://squidfunk.github.io/mkdocs-material/).

This is the successor of the developer Wiki, which was
[archived](https://archive.blender.org/wiki/2024/) in January 2024.

## Contributing

Contributions to the documentation are welcome! Check out the [contribution
guide](https://developer.blender.org/docs/contribute/) to get started.

It is possible to edit the documentation online. For more advanced editing, the
documentation is easy to build locally. Check out the [build
instructions](https://developer.blender.org/docs/contribute/build_instructions/).

Feel free to reach out on the
[#developer-documentation](https://chat.blender.org/#/room/#developer-documentation:blender.org)
chat channel.

## License

Except where otherwise noted, the content of the Blender Developer Documentation
is available under a [Creative Commons Attribution-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-sa/4.0/), or any later
version.<br/> Excluded from the CC-BY-SA are logos, trademarks, icons, source
code and Python scripts.

More information: https://developer.blender.org/docs/license/
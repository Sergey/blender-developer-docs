#!/usr/bin/env python3
# SPDX-License-Identifier: GPL-2.0-or-later

"""
Utility functions for make update and make tests
"""

__all__ = (
    "call",
    "check_output",
    "command_missing",
    "git_branch",
    "git_branch_exists",
    "git_remote_exist",
)

import os
import shutil
import subprocess
import sys
from pathlib import Path

if sys.version_info >= (3, 9):
    from collections.abc import (
        Sequence,
    )
else:
    from typing import (
        Callable,
        Sequence,
    )


def call(
        cmd: Sequence[str],
        exit_on_error: bool = True,
        silent: bool = False,
        env: "dict[str, str] | None" = None,
) -> int:
    if not silent:
        cmd_str = ""
        if env:
            cmd_str += " ".join([f"{item[0]}={item[1]}" for item in env.items()])
            cmd_str += " "
        cmd_str += " ".join([str(x) for x in cmd])
        print(cmd_str)

    env_full = None
    if env:
        env_full = os.environ.copy()
        for key, value in env.items():
            env_full[key] = value

    # Flush to ensure correct order output on Windows.
    sys.stdout.flush()
    sys.stderr.flush()

    if silent:
        retcode = subprocess.call(
            cmd, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, env=env_full)
    else:
        retcode = subprocess.call(cmd, env=env_full)

    if exit_on_error and retcode != 0:
        sys.exit(retcode)
    return retcode


def check_output(cmd: Sequence[str], exit_on_error: bool = True) -> str:
    # Flush to ensure correct order output on Windows.
    sys.stdout.flush()
    sys.stderr.flush()

    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        if exit_on_error:
            sys.stderr.write(" ".join(cmd) + "\n")
            sys.stderr.write(e.output + "\n")
            sys.exit(e.returncode)
        output = ""

    return output.strip()


def git_local_branch_exists(git_command: str, branch: str) -> bool:
    return (
        call([git_command, "rev-parse", "--verify", branch], exit_on_error=False, silent=True) == 0
    )


def git_remote_branch_exists(git_command: str, remote: str, branch: str) -> bool:
    return call([git_command, "rev-parse", "--verify", f"remotes/{remote}/{branch}"],
                exit_on_error=False, silent=True) == 0


def git_branch_exists(git_command: str, branch: str) -> bool:
    return (
        git_local_branch_exists(git_command, branch) or
        git_remote_branch_exists(git_command, "upstream", branch) or
        git_remote_branch_exists(git_command, "origin", branch)
    )


def git_remote_exist(git_command: str, remote_name: str) -> bool:
    """Check whether there is a remote with the given name"""
    # `git ls-remote --get-url upstream` will print an URL if there is such remote configured, and
    # otherwise will print "upstream".
    remote_url = check_output((git_command, "ls-remote", "--get-url", remote_name))
    return remote_url != remote_name


def git_branch(git_command: str) -> str:
    """Get current branch name."""

    try:
        branch = subprocess.check_output([git_command, "rev-parse", "--abbrev-ref", "HEAD"])
    except subprocess.CalledProcessError:
        # No need to print the exception, error text is written to the output already.
        sys.stderr.write("Failed to get Blender git branch\n")
        sys.exit(1)

    return branch.strip().decode('utf8')


def command_missing(command: str) -> bool:
    # Support running with Python 2 for macOS
    if sys.version_info >= (3, 0):
        return shutil.which(command) is None
    return False

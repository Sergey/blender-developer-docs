# SPDX-FileCopyrightText: 2024 Blender Authors
#
# SPDX-License-Identifier: GPL-2.0-or-later

define HELP_TEXT
Convenience Targets
   Provided for building.

   * setup:         Install mkdocs in virtual environment.
   * html:          (Default) Build HTML documentation for browsing locally from the file system.
                    Internal links resolve to .html files as opposed to directories, the search index will be made
                    local (browsers block fetching remote resources for local browsing).
   * html_servable: Build HTML documentation for browsing via a web server.
                    Internal links resolve to directories which the server can forward to contained index.html files,
                    making URLs nicer.
   * serve:         Build HTML documentation and run a local development server for live editing.
   * clean:         Clean the build.


Utilities

   * update               to update the repository to the most recent version.

endef
# HELP_TEXT (end)

# Needed to ensure actions use this directory (especially removing files).
BASE_DIR := ${CURDIR}

.DEFAULT_GOAL := html

MKDOCS := $(BASE_DIR)/.venv/bin/mkdocs
ifeq (,$(wildcard $(MKDOCS)))
	MKDOCS := mkdocs
endif

setup: .FORCE
	python3 -m venv .venv
	$(BASE_DIR)/.venv/bin/python3 -m pip install pip --upgrade
	$(BASE_DIR)/.venv/bin/python3 -m pip install -r $(BASE_DIR)/requirements.txt --upgrade

html: .FORCE
	@cd $(BASE_DIR) && OFFLINE=true $(MKDOCS) build

	@echo
	@echo "Docs built: ./site/index.html"

html_servable: .FORCE
	@cd $(BASE_DIR) && $(MKDOCS) build

	@echo
	@echo "Docs built: ./site/index.html"

serve: .FORCE
	@cd $(BASE_DIR) && $(MKDOCS) serve

clean: .FORCE
	rm -rf $(BASE_DIR)/site

	@echo
	@echo "Docs cleaned from ./site/*"

export HELP_TEXT
help: .FORCE
	@echo "$$HELP_TEXT"

update:
	@python3 ./build_files/utils/make_update.py

.FORCE:

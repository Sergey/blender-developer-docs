site_name: Blender Developer Documentation
site_url: https://developer.blender.org/docs/
repo_url: https://projects.blender.org/blender/blender-developer-docs
repo_name: projects.blender.org
edit_uri: _edit/main/docs
# Some random port to avoid collisions with other software.
dev_addr: 127.0.0.1:3917

theme:
  name: material
  favicon: resources/img/favicon.svg
  custom_dir: theme_overrides
  features:
    # Edit page button
    - content.action.edit
    # Copy code button in code snippets.
    - content.code.copy
    # Code annotations
    - content.code.annotate
    # For the tabs extension: Link tabs across the whole documentation, so that
    # e.g. clicking the "Linux" tab will ensure all pages have the "Linux"
    # tab activated by default.
    - content.tabs.link
    # Top level navigation header using tabs.
    - navigation.tabs
    # "Back to Top" button
    - navigation.top
    # Preview/Next page links in the footer, convenient navigation.
    - navigation.footer
    # Dynamically reload contents, don't fully reload pages while navigating.
    # Much smoother and more instant experience.
    - navigation.instant
    # Search suggestion (autocomplete with right arrow key).
    - search.suggest
  palette:
    # Toggle for light mode
    - media: "(prefers-color-scheme: light)"
      scheme: default
      accent: cyan
      primary: light blue
      toggle:
        icon: material/brightness-7
        name: Switch to dark mode
    # Toggle for dark mode
    - media: "(prefers-color-scheme: dark)"
      scheme: slate
      accent: cyan
      primary: light blue
      toggle:
        icon: material/brightness-4
        name: Switch to light mode
  icon:
    # Blender logo comes with material icons :)
    logo: material/blender-software
    edit: material/pencil
    view: material/eye

# Navigation is managed via https://oprypin.github.io/mkdocs-literate-nav/.
# This enables defining the navigation of a subsection using markdown files
# within directories (called README.md)
nav:
  - Home: 'index.md'
  - 'Developer Handbook': 'handbook/'
  - 'Features': 'features/'
  # Linked from the developer handbook for now.
  # - 'Contributor Programs': 'programs/'
  - 'Compatibility': 'release_notes/compatibility/'
  - 'Release Notes': 'release_notes/'
  - 'Summer of Code': 'programs/gsoc/'
  - 'Contribute Documentation': 'contribute/'

not_in_nav: |
  /license.md
  /programs/index.md
  /programs/navigation.md
  /handbook/guidelines/design/a4_design.md
  /handbook/guidelines/design/examples/assets.md
  /handbook/guidelines/design/examples/camera_lens.md
  /handbook/guidelines/design/examples/geometry_nodes.md

plugins:
  - search
  - section-index
  - literate-nav:
      nav_file: navigation.md
      implicit_index: true
      tab_length: 2
  - glightbox
  # For browsing locally from the file system.
  # See: https://squidfunk.github.io/mkdocs-material/plugins/offline/
  # Used by default for `make html`. Makes internal links work without a server, as well as search
  # and some other features that usually use external resources which browsers block for security
  # reasons.
  - offline:
      enabled: !ENV [OFFLINE, false]
  # mkdocs-redirects
  - redirects:
      redirect_maps:
        'features/interface/human_interface_guidelines/anatomy_of_an_editor.md': 'features/interface/human_interface_guidelines/editors.md'
        'features/animation/animation_system/baklava/index.md': 'features/animation/animation_system/layered.md'
        'handbook/guidelines/hosted_extensions.md': 'handbook/extensions/hosted.md'
        'handbook/addons/index.md': 'handbook/extensions/index.md'
        'handbook/addons/guidelines/index.md': 'handbook/extensions/addon_guidelines.md'
        'handbook/addons/addon_meta_info.md.md': 'handbook/extensions/index.md'
        'handbook/guidelines/design/a4_design.md': 'handbook/design/a4_design.md'
        'handbook/guidelines/design/communication_presentation.md': 'handbook/design/communication_presentation.md'
        'handbook/guidelines/design/decision_tree.md': 'handbook/design/decision_tree.md'
        'handbook/guidelines/design/examples/assets.md': 'handbook/design/examples/assets.md'
        'handbook/guidelines/design/examples/camera_lens.md': 'handbook/design/examples/camera_lens.md'
        'handbook/guidelines/design/examples/geometry_nodes.md': 'handbook/design/examples/geometry_nodes.md'
        'handbook/guidelines/design/examples/index.md': 'handbook/design/examples/index.md'
        'handbook/guidelines/design/ideas_concept.md': 'handbook/design/ideas_concept.md'
        'handbook/guidelines/design/index.md': 'handbook/design/index.md'
        'handbook/guidelines/design/navigation.md': 'handbook/design/navigation.md'
        'handbook/guidelines/design/toolkit/icons.md': 'handbook/design/toolkit/icons.md'
        'handbook/guidelines/design/toolkit/index.md': 'handbook/design/toolkit/index.md'
        # Examples:
        # 'old.md': 'new.md'
        # 'old/file.md': 'new/file.md'
        # 'some_file.md': 'http://external.url.com/foobar'

markdown_extensions:
  # Admonitions (content boxes like notes, warnings, etc.)
  - admonition
  # Alternative syntax for admonitions
  - callouts
  # Definition lists
  - def_list
  - md_in_html
  # Use image alt as caption, instead of manual <figure>
  - markdown_captions
  - attr_list
  # Enable arbitrary nesting
  - pymdownx.superfences:
      custom_fences:
        # Marmaid (generated diagrams)
        - name: mermaid
          class: mermaid
          format: !!python/name:pymdownx.superfences.fence_code_format
  # Syntax highlighting
  - pymdownx.highlight:
      anchor_linenums: true
      line_spans: __span
      pygments_lang_class: true
      use_pygments: true
  # Task (checkbox) lists
  - pymdownx.tasklist:
      custom_checkbox: true
  # Inline syntax highlighting
  - pymdownx.inlinehilite
  # Collapsible admonitions
  - pymdownx.details
  # Include code snippets from other files.
  - pymdownx.snippets
  # Enable tabs
  - pymdownx.tabbed:
      alternate_style: true
  - pymdownx.emoji:
      emoji_index: !!python/name:material.extensions.emoji.twemoji
      emoji_generator: !!python/name:material.extensions.emoji.to_svg
  # Wrapper around sane_lists extension that allows 2 spaces for indenting list
  # items, like other platforms do. See
  # https://github.com/Python-Markdown/markdown/issues/3.
  - mdx_truly_sane_lists
  # MathJax syntax:
  # https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference
  - pymdownx.arithmatex:
      generic: true
  - toc:
      # Show paragraph symbol to copy link to paragraph in headers.
      permalink: True
  # Enable _italic text_ and **bold text** formatting.
  - pymdownx.betterem
  # Footnotes via the [^1] syntax.
  - footnotes

hooks:
  - hooks.py

extra:
  generator: false

extra_javascript:
  - javascripts/mathjax.js
  - https://cdnjs.cloudflare.com/polyfill/v3/polyfill.min.js?features=es6
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - stylesheets/extra.css

copyright: '&copy; <a href="/docs/license">Copyright</a>: Content is available under a CC-BY-SA 4.0 Int. license unless otherwise noted. (<a href="/docs/license">More Info</a>)'

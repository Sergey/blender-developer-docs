# Build Instructions

These instructions only apply if you want to build a local/offline version of
the documentation. This is convenient for more elaborate edits, using an editor
and workflow of your choice.

=== "Windows"
    **Install required software:**

    0. Download the [Python installation package](https://www.python.org/downloads/) for Windows.
    0. Install Python with the installation wizard.
       **Make sure to enable the "Add Python to PATH" option**.
    0. Download and install [Git for Windows](https://git-scm.com/download/win).

    **Set up Git LFS**<br/>
    Open a command line window and run the following command:
    ```shell
    git lfs install
    ```

    **Clone the documentation sources:**
    ```shell
    git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
    cd developer-docs
    ```
    This will clone the sources into a `developer-docs` directory inside the current
    one and change into it.

    !!! tip ""
        **Recommended: Setup and activate a virtual Python environment where
        dependencies will be installed:**
        ```sh
        python3 -m venv .venv
        .venv/Scripts/activate # (1)!
        ```

        1. Repeat this command to re-activate the virtual environment whenever
        you open a new terminal session to build the documentation.

    **Install all dependencies, such as [Material for
    MkDocs](https://squidfunk.github.io/mkdocs-material/):**
    ```sh
    python -m pip install -r requirements.txt
    ```

=== "macOS"
    **Install required software**<br/>
    Install [PIP](https://pip.pypa.io/en/latest/installation/),
    [Git](https://git-scm.com/download/mac) and [Git LFS](https://git-lfs.com/).
    When using [Homebrew](https://brew.sh/), run the following commands in the
    terminal:
    ```sh
    python3 -m ensurepip
    brew install git git-lfs
    ```

    **Set up Git LFS**<br/>
    Open a terminal and run the following command:
    ```sh
    git lfs install
    ```

    **Clone the documentation sources:**
    ```sh
    git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
    cd developer-docs
    ```
    This will clone the sources into a `developer-docs` directory inside the
    current one and changes into it.

    !!! tip ""
        **Recommended: Setup and activate a virtual Python environment where
        dependencies will be installed:**
        ```sh
        python3 -m venv .venv
        source .venv/bin/activate # (1)!
        ```

        1. Repeat this command to re-activate the virtual environment whenever
        you open a new terminal session to build the documentation.

    **Install all dependencies, such as [Material for
    MkDocs](https://squidfunk.github.io/mkdocs-material/):**
    ```sh
    python3 -m pip install -r requirements.txt
    ```

=== "Linux"
    **Install required software**<br/>
    Install Python, PIP, Git and Git LFS using your package manager:

    === "Debian"
        ```sh
        sudo apt install python3 python3-pip git git-lfs
        ```
    === "Redhat/Fedora"
        ```sh
        sudo yum install python python-pip git git-lfs
        ```
    === "Arch Linux"
        ```sh
        sudo pacman -S python python-pip git git-lfs
        ```

    **Set up Git LFS:**<br/>
    ```sh
    git lfs install --skip-repo
    ```

    **Clone the documentation sources:**

    ```sh
    git clone https://projects.blender.org/blender/blender-developer-docs.git developer-docs
    cd developer-docs
    ```

    This will clone the sources into a `developer-docs` directory inside the
    current one and change into it.

    !!! tip ""
        **Recommended: Setup and activate a virtual Python environment where
        dependencies will be installed:**
        ```sh
        python3 -m venv .venv
        source .venv/bin/activate # (1)!
        ```

        1. Repeat this command to re-activate the virtual environment, whenever
        you open a new terminal to build the documentation.

    **Install all dependencies, such as [Material for
    MkDocs](https://squidfunk.github.io/mkdocs-material/):**
    ```sh
    python3 -m pip install -r requirements.txt
    ```

**Build this documentation with live reloading:**{#build-with-live-reloading}
```sh
mkdocs serve --dirty # (1)!
```

1. Use the `--dirty` option when editing single pages, as it makes rebuilds much
faster. It only rebuilds the HTML for modified files. Do not use it when
modifying the navigation hierarchy, since the option often fails to reflect such
changes.

Alternatively `mkdocs build` will generate the documentation as HTML into a
`site/` directory. Simply open `site/index.html` in a browser.

---

## Updating

The following commands assume you're inside the `developer-docs` directory.

**Update the documentation sources:**

```
git pull --rebase
```


!!! tip ""
    **Re-enable Virtual Python Environment**

    If you've followed the recommendation above of installing a virtual Python
    environment, you need to enable it for every new terminal session.

    === "Windows"
        ```sh
        .venv/Scripts/activate
        ```
    === "macOS"
        ```sh
        source .venv/bin/activate
        ```
    === "Linux"
        ```sh
        source .venv/bin/activate
        ```

**Update Dependencies:**


=== "Windows"
    ```sh
    python -m pip install -r requirements.txt
    ```
=== "macOS"
    ```sh
    python3 -m pip install -r requirements.txt
    ```
=== "Linux"
    ```sh
    python3 -m pip install -r requirements.txt
    ```

Dependencies should be updated regularly. Especially on dependency related build
errors.

**Build this documentation with live reloading**<br/>
Use the same build commands as explained [above](#build-with-live-reloading), for example:
```
mkdocs serve --dirty
```

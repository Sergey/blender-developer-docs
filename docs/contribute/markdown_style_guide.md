# Markdown Style Guide

Editing the developer documentation should have a low barrier of entry. For that reason, guidelines
are kept minimal.

<div class="annotate" markdown>
- Two space indentation
- Lines should be less than 100 characters long (1)
- Use italics for UI terms (buttons, labels, etc.)
- Use `&lt;br/>` to enforce line breaks as opposed to two trailing spaces
</div>

1. The limit of 100 was chosen because 80 generally ends up in extensive wrapping, whereas 120
makes editing side by side difficult (e.g. markdown editor side by side with a browser showing the
rendered output).

## Editor Configuration

[Some editors/IDEs](https://editorconfig.org/#pre-installed) will use the `.editorconfig` from this
repository for basic formating options.  For other editors, [there are
plugins](https://editorconfig.org/#download) available to enable this.

# Library Dependencies

Blender depends on many libraries. The complete list and the versions in
official releases can be found in the Blender repository:

[build_files/build_environment/cmake/versions.cmake](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/cmake/versions.cmake)

## Precompiled Libraries

Precompiled libraries are available for Windows, macOS and Linux in a GIT-LFS repository.
These are the easiest and most reliable way to get a feature complete Blender build.
Blender builds with these libraries are portable and can be run on other computers.

For Windows and macOS, precompiled libraries are automatically
downloaded when running `make update`. Building library dependencies on
these platforms is normally only done by platform maintainers.

For Linux, precompiled libraries must be downloaded once manually as per
the build instructions, and can then be updated with `make update`.

## make deps

The Blender repository has a CMake based system to build all library
dependencies from source. This is the system used to generate
precompiled libraries for Windows, macOS and Linux.

The `make deps` command performs the cmake setup and build. It is mainly
used by platform maintainers and is not tested to work on many different
operating system installs. A successful build may require some manual
configuration or edits to the code. Minimal instructions on how to use
it can be found in
[build_files/build_environment/CMakeLists.txt](https://projects.blender.org/blender/blender/src/branch/main/build_files/build_environment/CMakeLists.txt).

## System Packages

On Linux, the system package manager can also be used to install many
dependencies. See the Linux build instructions for details.

The resulting builds will not be portable, but libraries can be shared
with other software on the system.

The simplest way to install all needed packages is to run (from the
blender source directory):

` build_files/build_environment/install_linux_packages.py --all`

## Library support
The platform module is responsible for building/maintaining the third party
libraries Blender depends on. Every Blender release will have a `Library update`
issue which will be the central place to communicate with the platform module
pertaining the library work.

### New Dependencies

If a module has the need/desire to take on an additional dependency the module can propose this to the admins.

This proposal should contain the following information:

- Name of the library
- Purpose of the library
- Expected benefits this library would bring to Blender
- Amount of integration work required
- Expected change in Blender on disk footprint and distribution
- License the library is under
- Copyrights of the library

If a library has additional required dependencies or optional dependencies that
the module would like to have enabled the above information should be included for
each of the downstream dependencies.

The platform module will script the new dependency(s) in the dependency builder for
all currently supported platforms only after acceptance by the admins. The
module requesting the dependency is not expected for scripting it but is welcome
to do so if they like, the platform module _must_ be included in code review in those
cases.  

Preliminary libraries to develop against may be requested but this is on a best effort
basis and only after acceptance of the library by Blender admins.

### Version updates of existing libraries

Requests for existing dependencies to be updated to a newer version can be made
in the `Library update` issue for a release, by the following people:

- Any module member from a module that depends on a library
- Any member of the admin/release team
- Any member of the platform module
 
This request should include:

- The new version of the library
- Any License changes for the library since the previous version.
- Any Copyright changes for the library since the previous version.
- Any changes in dependencies (new/removed) of the library

If a library has additional required dependencies or optional dependencies that
require version changes as well, the above information should be included
each of the downstream dependencies.

The platform module will script the update, the module requesting the dependency is not expected for scripting it but is welcome to do so if they like, the platform module _must_ be included in code review in those cases.  
 
Any re-integration work that may need to be done shall be done by the module(s)
depending on the library. Preliminary libraries to develop against may be
requested but this is on a best effort basis.

## Git Hooks

For quite a few of our Git LFS repos we have deployed a sanity check hook that will reject any commits that contains binary files not tracked by Git LFS. The code to the hook is hosted here:
https://projects.blender.org/infrastructure/blender-devops/src/branch/main/gitea/hooks/deny-binary

If your push got rejected, it means that at least one of your commits contains binary files not hosted by Git LFS.

Below are some tips on what to do to resolve this depending on the situation

### The offending files are all new files

If all the files the hook is complaining about are all new files that you added to Git, you simply need to:
1. Add the files (usually as wildcards) to the `.gitattributes` files
This will make it so that Git will hand of the files to Git LFS
2. Recreate the commits and make sure that the new changes to the `.gitattributes` file is in the first commit you want to push.

To check if the files are going to be added to Git LFS, you can run `git lfs status` in the terminal.
The output should have three sections like this:
```
On branch main
Objects to be pushed to origin/main:


Objects to be committed:


Objects not staged for commit:
```

If you have done everything correctly, the files should be listed there in one of the different sections.
Your push should now be accepted.

### The offending files are already tracked by Git

If the files you are trying to push were already tracked by Git previously, you need to follow the same steps as the in the "The offending files are all new files" section but with some minor differences.

Because the hook only works on diffs, any diff containing a binary file will be rejected. So in this case where we change a binary file to be tracked with Git LFS, the diff will contain a binary file that is then changed into a text file with an LFS file hash. So even if you did do everything correctly the push will still be rejected.

To work around this, you need to add the following anywhere in the commit message `override restrictions`. This will bypass the hook and allow you to push.


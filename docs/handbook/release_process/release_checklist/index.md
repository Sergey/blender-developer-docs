# Release Checklist

This is a checklist to go over when making releases, to ensure everything has been updated.

`#blender-release` on chat.blender.org is used for release related communication.

## Alpha
- Check with module teams and developers about large features planned for the new release and add these to the project page.
- Decide on a splash screen and deliver it to the release manager before Beta.

## Beta

Transitioning to the [stabilizing
branch](../release_branch.md).

### Blender

=== "I - Branching"

    - Create `blender-v4.XX-release` branches for the following
      repositories:
      - `blender.git`
      - `tests/data` (`blender-test-data.git`)
      - `release/datafiles/assets` (`blender-assets.git`)
      - `lib/linux_x64` (`lib-linux_x64.git`)
      - `lib/windows_x64` (`lib-windows_x64.git`)
      - `lib/windows_arm64` (`lib-windows_arm64.git`)
      - `lib/macos_x64` (`lib-macos_x64.git`)
      - `lib/macos_arm64` (`lib-macos_arm64.git`)
      - `lib-source.git`
      - `blender-manual.git`
      - `blender-manual-translations.git`

      ```sh
      git checkout -b blender-v4.XX-release
      git push origin blender-v4.XX-release
      ```

=== "II - After Branching"

    - In the `main` branch of the Manual, update the `blender_version`
      number in `manual/conf.py`.
    - In the main branch:
      - `PROJECT_NUMBER` in `doc/doxygen/Doxyfile`
      - `BLENDER_VERSION`, `BLENDER_FILE_SUBVERSION` in
        `source/blender/blenkernel/BKE_blender_version.h`
    - In the release branch:
      - Set `BLENDER_VERSION_CYCLE` to `beta`.
      - Update and uncomment `BLENDER_VERSION` in
        `build_files/build_environment/cmake/download.cmake`.
      - Update `branch = main` in `.gitmodules` of `blender.git` to point to the release branch name.
    - Commit Splash screen to the release branch
      - Don't push this change before talking to the PR team - we announce
        to the world in social media at the same we commit the splash to the
        source code.
    - [Merge the release branch into
      main](../release_branch.md), ensuring
      `BKE_blender_version` and the splash screen remains correct.
    - Pre-create directory on download.blender.org/release/ for new version. Uset setfacl and check permissions (copy from previous releases)

### Buildbot

- Add tracks for new release branch in
  [`buildbot/conf/branches.py`](https://projects.blender.org/infrastructure/blender-devops/src/branch/main/buildbot/conf/branches.py).
- Remove folders for unused tracks on all buildbot machines, as there
  may not be enough disk space for the new track otherwise.
- Push git repo to workers and masters, and reconfigure masters.
- Force build `daily-coordinator`, `doc-api-coordinator` and
  `doc-manual-coordinator` for both `vdev` and the new release
  track.
- Verify that builds succeeded and are available for download.
- If new release is an LTS, create daily-X.X and devtest-X.X branches on
  Steam.

### Translations

- Manual:
  - Add a new category for the release on weblate.
  - Create new components for the manual translations:
    - Manual release: using 'Additional Branch', based on main Manual component.
    - Manual UI release: using 'From Component' and using newly created release manual component.
  - Copy over settings from the main Manual and Manual UI components (mainly the various commit message templates).

### Websites

- Release Notes
  - Adapt from the docs, add artwork
  - Add a new release notes redirect
    (e.g., `blender.org/download/releases/X.X` -\> `developer.blender.org/docs/release_notes/X.X`).
  - Create empty release notes for next release on `developer.blender.org/docs/release_notes/`
    and `blender.org`.
- Update dashboard on projects.blender.org
- Update release phase in chat.blender.org topic


## Release Candidate

### Blender
- Set `BLENDER_VERSION_CYCLE` to `rc` in the release branch.
- Update release list in
  `release/freedesktop/org.blender.Blender.appdata.xml`
- Update manual references by running
  `tools/utils_doc/rna_manual_reference_updater.py`
- Update license.md by running
  [`make license`](third_party_licenses.md)

### Communication
- Write press release and deliver it to the release manager one day before Release.

## Release

Preparation for the final release:

### Communication

- **At least one hour** before going to Release phase, send a message to
  \#blender-coders and check with available core developers online that
  everything looks good for release.
  - *Do not do that outside of reasonable work hours CET (10:00-17:00),
    nor during lunch break (13:00-14:00).*
  - Ensure `i18n (rBT, /release/datafiles/locale)` is updated. Notify
    Bastien Montagne/mont29 so he can update the files prior to final
    release commits
- Move Blender to
  [**Release**](../release_cycle.md) status:
  - Update Release status on [\#blender-coders @
    chat.blender.org](https://chat.blender.org/#/room/#blender-coders:blender.org)
  - Update Release status on front page of
    [projects.blender.org](https://projects.blender.org/)
- Create a *Potential candidates for corrective releases* task tagged
  with the released version. (For LTS releases, create a *Blender LTS:
  Maintenance Task X.X* instead)

### Blender
- Set `BLENDER_VERSION_CYCLE` to `release` in the release branch.

### Tagging

Tag repositories (only after the build is approved):

- Tag and push following repositories:
  - `blender.git`
  - `scripts/addons` (`blender-addons.git`) (only 3.6)
  - `scripts/addons_contrib` (`blender-addons-contrib.git`) (only 3.6)
  - `tests/data` (`blender-test-data.git`)
  - `release/datafiles/assets` (`blender-assets.git`)
  - `lib/linux_x64` (`lib-linux_x64.git`)
  - `lib/windows_x64` (`lib-windows_x64.git`)
  - `lib/windows_arm64` (`lib-windows_arm64.git`) (4.2 and upwards)
  - `lib/macos_x64` (`lib-macos_x64.git`)
  - `lib/macos_arm64` (`lib-macos_arm64.git`)
  - `lib-source.git`

  ```sh
  git tag -a v3.6.0 -m "Tagging Blender 3.6.0 release"
  git push origin v3.6.0
  ```

### Release Builds

!!! Note "**NOTE**"
    Check times when mirrors are updated. Aim to deploy files at 11:30 CET since
    syncing can take a while.

- On the buildbot:
  - Trigger a `vXXX-code-daily-coordinator` build with package
    delivery.
  - Once finished, trigger `vXXX-code-store-coordinator` to upload
    builds to the stores and generate msix package
- Manually tests builds.
- Once ready to release, trigger
  `vXXX-code-artifacts-deploy-coordinator` to deploy the packages to
  download.blender.org and mirrors.
  - Ensure mirrors are synced
- Lock uploaded files on download.blender.org (`schg`). Ask Dan
  McGrath to do this, can't be done from inside jail.

### Stores

- Snap
  - Final builds should be uploaded at this point, but not yet released
    as stable
  - Close beta for this release.
  - Close candidate for previous release, if not an LTS.
  - Promote candidate channel to stable for this release.
  - Update latest/stable to match this release.
- Steam
  - Final builds should be uploaded at this point, but not yet released
    in the stable branches
  - Create vX.X branch for this release and point it to the right build.
  - Point the default branch to the same build as vX.X.
  - Do release announcement and any other store page changes.
- Windows Store
  - Download msix package from download.blender.org and release it in
    the windows store.
  - Update store listings (export CSV, edit, import CSV again) with the
    PR release text

### Websites

#### blender.org
- Add Splash .blend file to [demo-files
  page](https://www.blender.org/download/demo-files/)
- Update
  [Redirection](https://www.blender.org/wp-admin/tools.php?page=redirection.php)
  from `/download/releases/{version}/` to
  `developer.blender.org/docs/release_notes/{version}`
- Download page
  - Paths & Platforms tab
    - Update the Builds Folder and File Path for every build in every
      platform
    - E.g. `blender-3.1.2-windows-x64.msi` →
      `blender-3.2-windows-x64.msi`
  - Splash & Release Notes tab
    - Update Release Features Summary, this text goes under the download
      section.
    - Update Splash Artwork, this shows up next to the Release Features
      Summary, doesn’t necessarily need to be the splash artwork, can be
      a cool screenshot too.
  - Announcement tab
    - This is a piece of HTML that can be displayed under the main
      download button. It could be used for adding a quick link to LTS
      versions.
- Credits: Update page using `source/tools/utils/credits_git_gen.py`
- Front page: Add news item
- Release Notes: Add item to [list of
  releases](https://www.blender.org/download/releases/)
- Thanks page: Update artwork
- Site-wide settings (this needs certain permissions only Pablo,
  Francesco, Dan have)
  - Set Blender Version and Release Date in Site-wide settings
  - Check mirrors have a copy of the file, otherwise disable those
    without. (find the URLs below)

##### Corrective releases

Corrective releases only require:

- Update Download page links
- Update Site-wide settings for:
  - Mirrors that have all the builds
  - Update version if the corrective release is for the latest release
- Update "fancy" release notes page linking to the list of fixes in the
  [release notes](../../../release_notes/index.md).

#### Release Notes

- Finish [Release Notes](../../../release_notes/index.md), update
  release phase and [Compatibility Changes](../../../release_notes/compatibility/index.md)
- Ensure updated screenshots at
  <https://download.blender.org/demo/screenshots/>

#### Docs

- Buildbot:
  - Update [`buildbot/conf/branches.py`](https://projects.blender.org/infrastructure/blender-devops/src/branch/main/buildbot/conf/branches.py) for stable doc
    version and manual version labels (for version switching menu).
  - Push git repo to workers.
- Trigger `vdev-doc-api-coordinator` and
  `vdev-doc-manual-coordinator` on buildbot to update docs, afterwards
  check the links and version switch menu are correct.
  - Python API [current](https://docs.blender.org/api/current/) is the
    new release
  - Manual [latest](https://docs.blender.org/manual/en/latest/) is the
    new release
- Check manual links are valid in Blender (see
  [bl_rna_manual_reference.py](https://projects.blender.org/blender/blender/src/branch/main/tests/python/bl_rna_manual_reference.py))

## LTS

In the case of an LTS release, add a new row in Download → Long-term
Support → Blender X.X LTS and fill with release notes from
`generate-code-patch-notes` buildbot build step (can be run manually
too, see `create_release_notes.py`)

### Backporting bugfixes

- Bugfixes should be in `main` for at least a week before backporting to LTS, to ensure they have been tested.
- Backports should be done one week before the LTS release, to give some time for testing.

### Releases
- LTS releases are done once a month, even if there is just one (high severity) bugfix.
  - Backports are to be done by the second Tuesday of the month.
  - Release is on the third Tuesday of the month.

### Sunsetting LTS

- Update the LTS's page announcing the sunsetting, recommend to switch to the latest LTS.
  [See 2.83 as reference](https://www.blender.org/download/lts/2-83/).
- Move the sunsetted release to `Previous LTS Releases` on
  [blender.org](https://www.blender.org/download/lts/ )

## Post Release

### Stores

- Snap: request tracks for the next release to be created on [snapcraft
  forum](https://forum.snapcraft.io) (with BlenderRelease account), so
  that we will have it by the time the next cycle starts. For LTS
  release, use a lts prefix for the track name.

### Buildbot

- Once the release is out for a while and no bugfix release is planned,
  update buildbot configuration to remove the track corresponding to the
  release.
- In case of an LTS release, keep the track around and remove an old LTS
  track if needed.

### Cycles

- Sync and tag Cycles "stand-alone" repository (rC)

### Translations

- Manual:
  - Remove (delete) categories and components related to releases that are no longer active.

### Benchmark

- Add release to the Blender Benchmark. More info in
  [\#blender-open-data](https://chat.blender.org/#/room/#blender-open-data:blender.org).

### Blender as Python Module

Build and update the python module for PyPI.
Note this is not currently done for bugfix/patch releases, due to limited disk space available.

Step 1: Compile the BPY module:

- Go to the release branch code-daily coordinator, i.e. `v420-code-daily-coordinator`
- Click `Trigger Daily Build`
- Enable:
  - Python module
  - GPU binaries
  - Package delivery
- Wait for the build to finish

Step 2: Deploy BPY module:

- Go to the bpy module deployment coordinator, i.e. `v420-code-bpy-deploy-coordinator`
- Trigger build

### Update LTS milestones

- Update corresponding LTS [milestones](https://projects.blender.org/milestones)
  to have the next scheduled release data

## Mirrors

This is the complete list of mirrors, from which not all may be enabled
for use at the moment. See the [External
Mirrors](https://www.blender.org/about/website/) page in blender.org to
find out which mirrors are currently enabled and ready to be used.

- [CN - Aliyun](https://mirrors.aliyun.com/blender/)
- [DE - RWTH Aachen
  University](https://ftp.halifax.rwth-aachen.de/blender/release/)
- [DK - Dotsrc.org](https://mirrors.dotsrc.org/blender/blender-release/)
- [NL - blender.org](https://download.blender.org/release/)
- [NL - Vereniging
  NLUUG](https://ftp.nluug.nl/pub/graphics/blender/release/)
- [US - Clarkson
  University](https://mirror.clarkson.edu/blender/release/)
- [US - Open Computing Facility at UC
  Berkeley](https://mirrors.ocf.berkeley.edu/blender/)
- [SG - Freedif Singapore](https://mirror.freedif.org/blender/) (syncs
  hourly)

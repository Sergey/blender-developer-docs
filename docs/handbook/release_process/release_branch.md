# Release Branch

## Overview

Towards the end of the [release cycle](release_cycle.md)
a stabilizing branch is created. Below the `blender-v4.4-release`
branch is used as an example.

### Before a release

Bug fixes for the upcoming release must be committed in the stabilizing
branch.

- Commit and push bug fixes to the `blender-v4.XX-release` stabilizing
  branch. This goes for Blender, add-ons, the manual and tests.
- Merge the stabilizing branch into `main`, where the following
  release is being developed.

### After a release

Bug fixes for a corrective release (for example 4.0.1, after 4.0.0 has
been released) must be committed to main and added to a task on
projects.blender.org. Don't commit fixes to the stabilizing branch
yourself!

- Commit and push bug fixes to the `main` branch.
- Check the corresponding
  [milestone](https://projects.blender.org/milestones) for a link to the
  backport list. Put your commit into the *To Be Backported* column. The
  release team will take care of the backport. If you don't have
  permissions to edit the task directly please add a comment instead.
- In special cases when a fix should go directly to the branch (and is
  not suitable for main), open a pull request and put that into the
  backport list instead.

## Details - Before a release

### Committing to the Stabilizing Branch

Develop the commit in a local branch based on the stabilizing branch:

```sh
git checkout -b my-fix blender-v4.4-release
make update
```

`make update` will set all submodules and tests to their corresponding
stabilizing branches. If you have local changes to Blender or
submodules, it will skip updating those repositories. You'll need to
commit (or remove) the local changes first.

Following the [pull request](../contributing/pull_requests.md) workflow,
commit changes to your local branch, push to your fork and create a pull
request. Choose `blender/blender-v4.4-release` as target instead of
`blender/main`. Then merge the pull request as usual.

If you accidentally targeted `main`, you can change it following
[these steps](../contributing/pull_requests.md#rebasing-a-pull-request).


??? Info "Alternative: No pull request"

    If you want to commit directly without a pull request, ensure you are on
    the correct stabilizing branch and update to the latest changes.

    ```sh
    git checkout blender-v4.4-release
    make update
    ```

    Once you have your local `blender-v4.4-release` up-to-date you can
    make your fix with the care you would do in normal `main` development.

    Once done, necessary commits to fix have been made, and you’ve checked
    everything is fine, you can push your changes. As a reminder: always
    commit _only_ that what _you_ have actually changed. If you see
    unrelated changes, just don’t stage nor commit.

### Merge to Main

As soon as you’ve successfully pushed your changes to
`blender-v4.4-release` it is time to also merge that change to
`main`.

```sh
git checkout main
make update
```

You should have latest `main` now. You can now fetch all the latest
change into the release branch, and then merge them into main:

```sh
git fetch origin blender-v4.4-release:blender-v4.4-release
git merge blender-v4.4-release
```

Pay attention to the output. Resolve conflicts if necessary.
Double-check changes with `git log`. If everything looks fine go ahead
and push.

```sh
git push origin main
```

## Submodules

If you need to make changes to tests, libraries or assets, these need
to be merged as well.

### Merging Tests and Libraries

Most of the time tests and libraries are unchanged, but changes in them
need to be merged from the release branch. The steps for this are:

- When working on the git release branch, ensure you have run `make update`
  so the submodules match.
- Commit changes to the test data submodule in the release branch.
- Amend Blender repository commit to include the submodule hash.
- Checkout main and run `make update` again.
- Merge changes to test data submodule from release branch.
- Merge changes to Blender from release branch, and amend to include
  the submodule hash.

### Example Steps

#### Release Branch

Commit Blender repository changes to release branch:
```
git checkout blender-v4.4-release
make update
git commit sourcefile1 soucefile2
```

Then commit test files to the release branch.
```
cd tests/data
git checkout blender-v4.4-release
git commit testfile1 testfile2
```

Amend commit in release branch to use submodule hash with new test files.
```
cd ../..
git commit --amend tests/data
```

Check everything is ok, and push both.
```
cd tests/data
git push origin blender-v4.4-release
cd ../..
git push origin blender-v4.4-release
```

#### Merge to Main

Merge Blender repository changes into main:
```
git checkout main
make update
git merge blender-v4.4-release
```

Merge test data changes into main.
```
cd tests/data
git checkout main
git merge blender-v4.4-release
```

Amend Blender repository commit with updated hash.
```
cd ../..
git commit --amend tests/data
```

Check everything is ok, and push both.
```
cd tests/data
git push origin main
cd ../..
git push origin main
```

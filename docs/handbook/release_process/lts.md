# Blender LTS

With the release of Blender 2.83, Blender Foundation will start a LTS
(Long Term Support) pilot program. The program is aimed at ensuring that
long-lasting projects can be executed using a stable Blender version,
which will provide critical fixes throughout a 2-year time span.

## Criteria to determine what to be ported

Currently, for non-LTS releases, Blender only has a corrective release
if severity 1 issues (high severity bugs) are found. When the corrective
release is agreed on, however, severity 2 (high severity and normal
bugs) fixes are ported along.

For the LTS releases, a more limited policy would apply (only porting
severity 1 issues after the next stable release), on a fixed schedule
(e.g., every 3 months) after the fix was tested in master for some time
(e.g., 1 week).

## Bug-fix flow diagram

Currently, at the beginning of Beta, master is forked into a release
branch. Any bug fix relevant to the upcoming release happens in the
release branch, and is merged to master.

![](../../images/Lts-branch-management-pre.png)

For the LTS versions, their branches work as usual, but once the release
happens they keep receiving critical updates. Such updates can be ported
from master or be performed on the branch itself.

![](../../images/Lts-branch-management-post.png)

## Distribution

The LTS release will be added to the current roster of Blender releases
available during the development cycle.

- [blender.org/download](http://blender.org/download) offers latest
  stable release
- [blender.org/download/lts](http://blender.org/download/lts) offers the
  latest LTS releases (up to 2 in parallel)
- [builder.blender.org](http://builder.blender.org) offers alpha builds
  of the upcoming latest version

## Version Communication

In order to clearly communicate how the growing number of Blender
versions relate to each other, an updated version naming convention will
be adopted:

- Sub-version as corrective releases. Use 2.83.1 instead of 2.83a

Never expose to the users the internal sub-subversion numbering (e.g.,
2.83.9). The complete version and the hash is enough for bug reporting.

- Rename the development builds (currently 2.90 alpha) to allow better
  focus on the upcoming release (2.83). Instead of calling it "2.90
  alpha", we call it *development / master / daily / next / ...* build.
- [builder.blender.org](http://builder.blender.org) should feature exclusively
(or distinctly) the master build

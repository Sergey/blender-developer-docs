# Steam Store

Blender is available on [Steam](https://store.steampowered.com/app/365670/Blender/).

Different versions and daily builds are available by right clicking the software on your Steam Library and choosing `Properties`.
From the properties window select the tab titled `Betas`.

## Publishing Builds

Delivery is automated through the buildbot.

Assigning a build as the stable release is done manually on the
application page. Before doing so, switch to the matching daily build branch and test
it works correctly first.

## Event

We also create an announcement on Steam for major and bugfix releases.
For major releases, use the press release, for bugfix updates the following template can be used:

**Title**: *"Blender [X.X] Corrective Release"*

**Second title**: *<one-line teaser from release notes>*

**Text**:

``` text
It's time to update your Blender!

Blender [X.X] addresses [X] issues present in the initial release.

This is a corrective release, meaning it doesn't contain new features, only fixes. It is highly recommended for everyone currently using Blender [X.X] to upgrade, since it fixes several crashes and improves functionality in many areas.

Read the full list of changes on https://developer.blender.org/docs/release_notes/X.X/corrective_releases/

Follow Blender on X for further updates. https://x.com/Blender

The Blender team,
[Date]
```

**Images**:

We upload two different images for the event, based on the splash screen:

- Cover image: 800px x450px
- Header image: 1920px x 622px

## Update Store Graphics

Update at Store Page Admin \> Graphical Assets. For best results, `.png` files are recommended.

### Store Assets

- Derived from splash screen:
  - Header capsule: 920px x 430px
  - Main capsule: 1232px x 706px
  - Vertical capsule: 748px x 896px

- Blender logo:
  - Small capsule: 462px x 174px

- Page Background (ambient, should not compete with the content and other images)
  - 1438px x 810px

### Library Assets

- Library capsule: 600px x 900px
- Library header: 920px x 430px
- Library hero: 3840px x 1240px
- Library logo: 1280px x 720px

## Known Issues

### Linux
Valve recently changed how applications run on Linux. It now defaults to run applications inside a Steam Linux Runtime.
This has some consequences, known issues are:

* Cycles GPU rendering crashes or cannot find GPUs to render on. [#129895](https://projects.blender.org/blender/blender/issues/129895)
* System Volumes are not visible inside the Blender File Browser [#129902](https://projects.blender.org/blender/blender/issues/129902)

#### Workaround
To workaround these issues, users have to disable the use of the Steam Linux Runtime for the time being.

* To disable it globally for all Steam applications and games, run Steam from the terminal with: `steam -compat-force-slr off`
* Disable it for Blender only: `Properties → Compatibility → Force the use of... → Legacy runtime 1.0`

### Windows
* Error message launching Blender from Steam with Vulkan backend [#131199](https://projects.blender.org/blender/blender/issues/131199)

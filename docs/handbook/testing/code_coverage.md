# Code Coverage

Measuring code coverage is useful to find blindspots of Blender's test suite.
Increasing code coverage is a good way find bugs earlier in the development
process. While covered code is not guaranteed to be bug free, it does make it
much more likely that regressions are caught early when this code is changed.

> NOTE: The built-in coverage report generation currently only works using GCC
> on Linux. Support for more platforms may be added in the future, as well as
> generating coverage reports on the build-bot.

## Usage

1. Create a **debug build** using **GCC** with **WITH_COMPILER_CODE_COVERAGE**
   enabled.
2. Run tests. This automatically generates `.gcda` files in the build directory.
3. Run `make coverage-report` in the build directory (or `ninja coverage-report`
   if using Ninja).

If everything is successful, this will open a browser with the final report
which is stored in `build-dir/coverage/report/`. Running
`make/ninja coverage-reset` deletes all `.gcda` files which resets the line
execution counts.

For a bit more control one can also run `tests/coverage/coverage.py` script
directly.

# Import/Export Tests

3D file import tests compare a short textual dump of imported results
against expected reference textual dump.

## Inspection

Tests result can be inspected visually through a HTML file that shows
both the new result, reference result and a textual diff. This can be found at:

``` text
~/blender-git/build/tests/report.html
```

By default `ctest` output is sparse. More details including the Blender commands can be logged as follows:

```
cd ~/blender-git/build
ctest -VV -R io_obj_import
```

When the import tests fail on the Buildbot, the end of the output log will look like:
```
================================================================================
ERROR Summary:
================================================================================
419/434 Test #422: io_fbx_import ...................................................***Failed    2.15 sec
420/434 Test #423: io_obj_import ...................................................***Failed    1.93 sec
424/434 Test #425: io_stl_import ...................................................***Failed    1.42 sec
```
and the results produced by Buildbot can be downloaded, similar to how Render tests results can: the `deliver-test-results`
Buildbot step will have a ZIP file with all the results. Opening `results.html` from there will show an overview page:<br />
![](images/io_tests_results.png)

And within each failing test category, and overview of failures will look like:<br />
![](images/io_tests_failure.png)

## Adding and Updating Tests

### Updating Reference Results

When adding a new test blend file, or when there are benign test failures due to
intentional changes, the reference output files can be created or updated as
follows:

``` bash
cd ~/blender-git/build
# Update reference for Wavefront OBJ import tests
BLENDER_TEST_UPDATE=1 ctest -R io_obj_import
```
On Windows, setting the environment variable is more involved and would look like `set BLENDER_TEST_UPDATE=1 && ctest -R io_obj_import`, and remember
to unset it afterwards.

After this all the tests should pass, and .blend files and reference
text files can be committed.
How to do so is explained [here](./setup.md#committing-test-data).

### Current import tests

Currently FBX, OBJ, PLY and STL importers are covered by this test suite. Their Python driving files are respectively
`io_fbx_import_test.py`, `io_obj_import_test.py`, `io_ply_import_test.py` and `io_stl_import_test.py` under `tests/python`
source code folder. The data files they use are in the tests data repository, under `io_tests/fbx`, `io_tests/obj`, `io_tests/ply`,
`io_tests/stl`.

Custom import parameters per input file, if needed, can be provided by having a sidecar `.json` file next to imported
file (same file basename, json extension), that has a single json object with custom arguments, e.g.
```
{
  "use_split_objects": false,
  "use_split_groups": true,
  "global_scale": 3.14,
  "forward_axis": "Z"
}
```

# Render Tests

Render, compositing and video sequencer tests compare render results
against reference images.

## Inspection

Tests result can be inspected visually through a HTML file that shows
both the new image, reference image and a diff. This can be found at:

``` text
~/blender-git/build/tests/report.html
```

By default `ctest` output is sparse. More details including the Blender commands can be logged as follows:

```
cd ~/blender-git/build
BLENDER_VERBOSE=1 ctest -VV -R cycles
```

## Adding and Updating Tests

### Updating Reference Images

When adding a new test blend file, or when there are benign test failures due to
intentional changes, the reference images can be created or updated as
follows:

``` bash
cd ~/blender-git/build
# Update reference images for cycles tests
BLENDER_TEST_UPDATE=1 ctest -R cycles
```

After this all the tests should pass, and .blend files and references
images can be committed.
How to do so is explained [here](./setup.md#committing-test-data).

### Categories

Tests are organized in folders per category.

When creating a new category folder, it must be added in `tests/python/CMakeLists.txt`.

### Files

Each blend file generates a test in its category. It always renders frame number 1 to a PNG image.

| Feature | Location |
| - | - |
| Cycles, EEVEE, Workbench, Hydra | `tests/data/render/{category}/*.blend` |
| Compositor | `tests/data/compositing/{category}/*.blend` |
| Video Sequencer | `tests/data/sequence_editing/{category}/*.blend` |
| Mesh Sculpting | `tests/data/sculpting/{category}/*.blend` |

## GPU

By default only CPU tests are run. To enable GPU testing, change the CMake configuration.

| Feature | CMake Variable |
| - | - |
| Cycles |`CYCLES_TEST_DEVICES=CPU;{DEVICE}`<br/>with `CUDA` `OPTIX` `ONEAPI` `HIP` `HIP-RT` `METAL` `METAL-RT` |
| EEVEE, Workbench, Hydra | `WITH_GPU_RENDER_TESTS=ON` |
| Compositor | `WITH_GPU_COMPOSITOR_TESTS=ON` |
| Mesh Sculpting | `WITH_GPU_RENDER_TESTS=ON` |

Result may sometimes be different between CPU and GPU, or between different GPUs. For this reason some tests are blacklisted or use higher diff thresholds.

## Batching

By default multiple tests are run with a single Blender launch, which
speeds up test execution.

For investigating threading or memory corruption issues, disabling this
can help avoid interference between tests. This is done by turning off
`WITH_TESTS_BATCHED`.
# Setup

Tests require Blender to be built, see the [official build
instructions](../building_blender/index.md). This document assumes the
directory layout to be:

``` text
~/blender-git/blender  # Blender source directory
~/blender-git/build    # Blender build directory
```

Running the tests is then done with `make test`:

``` bash
cd ~/blender-git/blender
make test
```

[GTest](gtest.md) tests need Blender to be built
with `WITH_GTESTS` enabled.

## Downloading Test Files

Additional binary test files are needed for running the Blender tests.
These are available through a Git submodule, separate from the Blender
source repository.

`make test` will automatically download these files the first time it
runs.

To download the test files manually:

``` bash
cd ~/blender-git/blender
python3 ./build_files/utils/make_update.py --use-tests
```

## Updating Test Files

Test files change along with the Blender source code. `make update`
will update tests to the latest version if you have downloaded them
once, together with the Blender source code.

``` bash
cd ~/blender-git/blender
make update
```

## Running Tests

`make test` runs all available tests. For fine control, use
[ctest](https://cmake.org/cmake/help/latest/manual/ctest.1.html)
directly.

`ctest` must be run from the build directory.

``` bash
cd ~/blender-git/build
```

Some example commands are:

``` bash
# List available tests
ctest -N
# Run all tests with name matching "alembic"
ctest -R alembic
# Show verbose output for a test, to get more information on why it failed
ctest -VV -R alembic
# Run tests in parallel, and show verbose output for failed tests
ctest -j 4 --output-on-failure
```

When building with Visual Studio or Xcode, the build configuration needs
to be specified for every `ctest` command, for example:

``` bash
ctest -C Release -R alembic
```

Some GTest tests bundle together all tests within a Blender module. It's
possible to run only specific test within a module as follows. Find the
name of the module test, for example by listing all tests with `ctest -N`.

``` text
...
Test  #5: bf_blenkernel_tests
...
```

Then run the test with verbose output:

``` bash
ctest -R bf_blenkernel_tests -V
```

This shows the exact command that is run for each test. You can then
copy-paste that command to run it yourself.

### ASAN builds

Running tests on ASAN builds is tricky, because ASAN will fail most of
them due to false/unrelated memory leak detection. However, completely
'silencing' ASAN reports using the `exitcode=0` `ASAN_OPTIONS` is a
(very) bad idea, as it will also hide many actual issues, including
segfaults!

The first step is to remove `memleaks` errors which happen in third party
libraries, see [the ASAN
page](../tooling/asan.md#quiet-leaksanitizer)
for details.

The `guardedalloc` test intentionally attempts to allocate an invalid
amount of memory and expects to fail and return NULL in that case.
`allocator_may_return_null=true` is required for this test to behave
as expected.

``` bash
ASAN_OPTIONS="allocator_may_return_null=true" LSAN_OPTIONS="print_suppressions=false:suppressions=/path/to/blender/tools/config/analysis/lsan.supp" ctest -j 4 --output-on-failure
```

> NOTE: **No LSAN Suppressions**
> In case LSAN suppressions do not work for some reason, it is also
possible to pass `leak_check_at_exit=false` to ASAN_OPTIONS.
However, doing this will also hide valid `memleaks` reports,
which should be avoided as much as possible.

## Adding Tests

Tests can be written in [Python](python.md) or
[C++](gtest.md). For tests that can easily be
written in Python, this is preferred. Lower-level tests can be written
in C++ files next to the sources under test. See the language-specific
pages for more info.

## Committing Test Data

`tests/data` is a submodule, and making changes there requires some extra steps.
These are instructions for the main branch.
For doing this in a release branch, see [here](../release_process/release_branch.md#merging-tests-and-libraries).

Ensure submodules are up to date.
```
make update
```

Commit test files to the main branch of the test data repository.
``` bash
cd tests/data
git checkout main
.. make your changes ..
git commit testfile1 testfile2
```

Commit changes in the Blender repository, including the updated `tests/data` submodule hash.
``` bash
cd ../..
git commit sourcefile1 sourcefile2 tests/data
```

Check everything is ok, and push to both repositories.
``` bash
cd tests/data
git push origin main
cd ../..
git push origin main
```


> NOTE: **Make sure the tests pass before committing to main**
>
> You should run your tests to make sure they pass before commiting them to main.
This can be done [locally](#running-tests), or using
[Blender Bot](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md)
from Gitea to ensure they pass on all platforms.
The steps for testing on the build bot are as follows:
>
> Commit changes in the `tests/data` repository to a new branch rather than main.
> > WARNING:
> > Be aware that this requires making this branch on the `blender/blender-test-data` repository, and *not* your fork,
> > otherwise the build bot will be unable to access it.
>
``` bash
cd tests/data
git checkout -B add-x-tests
.. make your changes ..
git commit testfile1 testfile2
git push origin add-x-tests
```
>
> Commit the new `tests/data` submodule hash to your fork of the Blender repository.
``` bash
cd ../..
git checkout -B add-x-tests-blender
git commit tests/data
git push me add-x-tests-blender
```
> [Create a pull request](../contributing/pull_requests.md#create-a-pull-request) from the
link printed in the console,
and comment `@blender-bot build` to get the build bot to test the changes. Alternatively,
`@blender-bot build +gpu` can be used to trigger additional GPU tests (these aren't tested regularly,
so some GPU tests you haven't modified may fail).
>
> If the tests pass, then you can push your changes in the `tests/data`
repository to the main branch.
``` bash
cd tests/data
git checkout main
# Use --ff-only to keep the submodule hash unchanged.
git merge --ff-only add-x-tests
git push origin main
```
>
> From the Gitea interface, merge the pull request you just created in the Blender repository.


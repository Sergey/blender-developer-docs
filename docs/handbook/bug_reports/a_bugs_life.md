# The Life of a Bug

There are different stages of the life of a bug. Each of them requires a
different skill set, responsibility and dedicated time:

```mermaid
  flowchart LR
     Triaging --> Investigation -->  Fix
```

- **Triaging**: Is this a valid bug, complying to our guideline, make it
  ready for a developer to fix.
- **Investigation**: Is the severity correct? Should a milestone be assigned to it? When to assign this to a developer, and who they are?
- **Fix**: Where added value happens, where we should spend most of our
  development resources.

## Triaging

*Triaging* should be done by a dedicated team, isolating the rest of the
development team from the inflow of tasks in our bug submission
platform. Any bug fully triaged should be either closed or tagged to a
module (not assigned to a developer). Any confirmed bug should have an
estimated severity.

## Investigation

*Investigation* is a job for the module owners and development
coordinators. The bug severity is confirmed or updated and its quality
is confirmed (or rejected back to triaging). This is also when we
determine if this is a bug or a known issue (or ToDo even).

* For high severity issues this is when developers are assigned and
milestones are encouraged to be set.

* An assigned bug is a clear statement that the issue is on the module's
radar and will be worked on soon.

* If a high severity bug won't be tackled soon, it should set a milestone to a
future release, ideally with some explanation on the bug report.

## Fix

*Fix* includes the development time as well as sending the patch for
review and having it fully merged in the code base.

## A Bug is Born

A user went over the [instructions on how to make a good bug
report](making_good_bug_reports/index.md), and put time and effort in
making the most complete comprehensive report possible. This is only the
start of the conversation though.

In this example we have a pretty good bug report, above the average
even. In fact more incomplete bug reports would be sent back to the
reports asking for more information.

![](../../images/Bug-untriaged-gitea.png)

The team of [Bug triagers](help_triaging_bugs.md) will
now dissect the problem following [a strict guideline on how to handle
the report
gracefully](triaging_playbook.md). After
some investigation and back and forth, the report can be updated so it
is ready to be fixed. It is not so trivial to do a final treatment to
the bug report, so let's continue with this example.

## A Fully Triaged Bug

This right here is a bug report with all the necessary information for
its fix condensed right in the bug description. Let's take a closer look
at what changed from the original report to the final one:

![](../../images/Bug-triaged-Gitea2.png)

1 - Title updated to give a more precise description of the problem.

2 - The bug is confirmed (the `Status > Confirmed` label is added),
for other scenarios please refer to the [bug triaging
playbook](triaging_playbook.md).

3 - The corresponding [module](../organization/modules.md) is tagged (the
corresponding `Module >` label is added). The `Module >` labels
are scoped meaning only one can be chosen (this ensures responsibility
by a single module). If in doubt, choose the module where the issue is
most visible to the user or CC the @blender/Module team which consists
of the module owners. Later in the process, the modules may decide to
hand over responsibility themselves (by changing the `Module >`
label). To help discoverability in the case the issue touches multiple
areas in blender, (multiple) `Interest >` tags can still be added
optionally.

4 - Initial severity is set (for a list of Severity Clarifications see
the [bug triaging
playbook](triaging_playbook.md), also check
there what to do if the issue turns out to be a recent regression).

Note the `Type >` label is untouched here (changing this is part of
Classification)

5 - Unnecessary information removed (the graphic card has nothing to do
with this issue).

6 - Bug tested with older Blenders (e.g., 2.79) to determine if this is
a regression (this influences the initial severity as well, see above).

7 - A simple example file (with the font packed) was created.

8 - No Project is assigned to it. Adding Projects will put the issue on
the modules workboard (and this might not be what the module wants to
use the workboards for -- modules will do this themselves).

9 - No one is assigned to it. It is not a job for the triager to assign
bugs to developers (modules will do this themselves).

10 - Clear reference of the expected behavior.

11 - Added some developer notes that summarize the discussion in the
task.

12 - Removed yet more irrelevant information.

> NOTE: **Description and Title Updates**
> We welcome all the community to help with triaging. However not
everyone has the edit rights to update the bug report. This is ok,
someone with edit access will take care of updating the report before
tagging a module, which is when we consider the triaging complete.

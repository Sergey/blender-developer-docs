# macOS System Information 

## Blender 4.3+ - Semi-Automated Information Collection

In Blender 4.3 we added a script that helps automatically collect system information
in the case that Blender isn't opening. The steps to use it are as follows:

- Navigate to where Blender is installed. This is usually the `Applications` folder.
  - If you installed Blender through Steam, then you can find where Blender is installed
  by right clicking on Blender in Steam selecting `Manage > Browse local files`.
- Right click on `Blender` and select `Show Package Contents`
- Navigate to the `/Contents/Resources/` folder and there you will find the script
`blender-system-info.sh`.
- Open the `Terminal` app and drag and drop the script into the Terminal and press
`Return` to run it.
- The script will collect information about your computer and Blender
and open the bug reporting web page, automatically filling it out with
the information it collected.
- The script is unable to collect information about your graphics card, so you will
need to collect this information manually by following `Graphics Card` section of
the guide below.

![](./images/macOS_script_information_collection.jpg)

---

## Operating System

Select from the top bar of macOS `Apple Icon > About This Mac`. Here you will find
your macOS version beside the field `macOS`.

Share the information on the report form like this: `macOS 14.4.1`

![](./images/macOS_get_os_version.jpg)

## Graphics Card

Open Launchpad from the dock, then search for and open the `System Information`
app. You will be find your graphics card information in the section
`Hardware > Graphics/Display`. The information we're interested in is the
`Chipset Model` and `Metal Support`.

Combine these details together and share them in your bug report form
like this: `Apple M1 Pro - Metal 3`

![](./images/macOS_get_gpu_info.jpg)

## Blender Version

Follow one of the guides below to obtain your Blender version number, then
share it on your bug reporting form.

### Downloaded from Blender Website

There are two main ways we recommend finding the Blender version.
Through the command line, or through a GUI.

#### Command Line

Open the terminal app and run the command `/path/to/blender --version`. In the output
you will find the information we're interested in. They are Blender version,
commit date, hash, and branch *(Blender 4.2+)*. Combine these details together
and put them on your report form like this:
`Blender 4.2.0, branch: main, commit date: 2024-05-16 20:40. hash: 1558bc7cb428`

The path to Blender is typically `/Applications/Blender.app/Contents/MacOS/Blender`

![](./images/macOS_get_blender_version_command_line.jpg)

#### GUI

Navigate to where Blender is installed, right click on it, and select `Get Info`.
You will find the Blender version number in the section `General > Version`. Share
this information on your bug reporting form like this: `Blender 4.1.1 2024-04-16`

![](./images/macOS_get_blender_version_gui.jpg)

### Downloaded from Steam

There are two main ways we recommend finding the Blender version.
Through the command line, or through a GUI.

#### Command Line

Open Steam, right click on Blender, and select the option
`Manage > Browse local files`. Finder will open showing where
Blender is installed. Right click on Blender and select `Show Package Contents`
then navigate to the folder `/Contents/MacOS/`. Here you will find the Blender
executable. Open the terminal app and run the command `/path/to/blender --version`,
using the Blender executable found earlier. In the output you will find the
information we're interested in. They are Blender version, commit date, hash,
and branch *(Blender 4.2+)*. Combine these details together
and put them on your report form like this:
`Blender 4.2.0, branch: main, commit date: 2024-05-16 20:40, hash: 1558bc7cb428`

![](./images/macOS_get_blender_version_steam_command_line.jpg)

#### GUI

Open Steam, right click on Blender and select the option
`Manage > Browse local files`. Finder will open showing where
Blender is installed. Right click on Blender and select `Get Info`.
You will find the Blender version number in the section `General > Version`. Share
this information on your bug reporting form like this: `Blender 4.1.1 2024-04-16`

![](./images/macOS_get_blender_version_steam_gui.jpg)

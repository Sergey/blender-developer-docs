# Manually Collecting System Information 

When reporting a bug you will be asked to provide some system information
and your Blender version number. This information can be automatically
collected and filled out by selecting `Help > Report a bug` from the
top of Blender.

However, sometimes Blender won't open, so you can't use these features.
In which case you will need to collect the information manually.
To do that, check the guide for your operating system below.

- [Windows](windows_information.md)
- [macOS](macos_information.md)
- [Linux](linux_information.md)

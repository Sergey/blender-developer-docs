# This is not a bug

Or *"Why your report was closed"*.

For our issue tracker we have a narrow view of what is considered a bug,
typically this is limited to something that doesn't work as intended,
other issues may be important or limitations we should address however
they often end up being feature-requests, something we can't practically
handle in the issue tracker.

This is not to say the suggestion is unreasonable or should be ignored,
just that it's not the purpose of the issue tracker to handle such
topics.

Take this [paper cuts
thread](https://devtalk.blender.org/t/blender-ui-paper-cuts/2596) with
over 4000 posts and consider the time it would take if developers were
to attempt to address every user suggestion as if it were a bug.

We appreciate the effort that can go into making a bug report and that
it's not always clear when what should be considered a bug or not.

## Other Sites

Here are other sites that might be useful.

- <https://blender.stackexchange.com>  
  If you're not sure about some part of Blender you can ask about it
  here.
- <https://devtalk.blender.org/c/user-feedback>  
  The user feedback section of our developer forum.
- <https://blender.community/c/rightclickselect>  
  A place to make suggestions and comment on other peoples suggestions.

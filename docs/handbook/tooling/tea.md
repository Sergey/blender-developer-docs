# tea: Gitea command line tool

[tea](https://gitea.com/gitea/tea) is a tool to interact with
projects.blender.org.

## Setup

Install `tea`. There are pre-built binaries available
[here](https://gitea.com/gitea/tea#installation). Or you can build it
from source by following the [build instructions](https://gitea.com/gitea/tea#compilation).

Generate an access token:

- Go to user [Setting \> Applications](https://projects.blender.org/user/settings/applications).
- Type in any name (e.g. `tea`) and click `Generate Token`.
- To be written: inform about scopes. Some users might want to check
  `read:public_key` or `notifications` to be able to use certain
  commands)
- A banner will appear with `<your-token>`, copy it for the next step
  (you don't need to save it).

Add a new login:

``` bash
tea login add --name=blender --url=https://projects.blender.org/ --token=<your-token>
```

Finally, you should see your login listed with your username from
projects.blender.org:

``` bash
tea logins
```

Optionally, you can set this login to be the default login with:

``` bash
tea login default blender
```

## Useful Commands

Checkout pull request \#123.

``` bash
tea pull checkout 123
```

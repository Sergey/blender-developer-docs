# clangd

[clangd](https://clangd.llvm.org/) is an implementation of the [language server
protocol](https://langserver.org/) (LSP) for C/C++. It can be integrated into various IDEs
to provide the following functionality:

* Code completion
* Inlay compiler errors and warnings, with auto fix
* Inlay clang-tidy warnings and notes, with auto fix
* Finding unused includes
* Go to definition and references
* Basic refactoring, like rename symbol across files

## Setup

A  `compile_commands.json` needs to be in a place where clangd can find it.
The simplest way is to make a symbolic link in the source folder.

```bash
cd <build_folder>
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DWITH_UNITY_BUILD=OFF .
mkdir ../blender/build
cmake -E create_symlink compile_commands.json ../blender/build/compile_commands.json
```

The next steps depend on your IDE, for example.

* Visual Studio Code has a clangd extension that can be installed.
* Neovim has native lsp support, with plugins that help integrate clangd better.
* CLion has native support.

## Clang-Tidy

Besides compiler errors and warnings, more opinionated warnings and notes
are provided by [clang-tidy](https://clang.llvm.org/extra/clang-tidy/).
These can help improve readability and prevent bugs.

See `.clang-tidy` in the root Blender source directory for the checks that
are enabled.

## Unused Includes

`clangd` also detects unused includes, see its
[documentation](https://clangd.llvm.org/guides/include-cleaner) for details.

Platform differences and build options can lead to false positives. So
be sure to test any changes on the Buildbot.

There are a few ways to resolve such warnings:

* Add the relevant `#ifdef` around the `#include` for platform or build options.
* Replace the `#include` by a more specific one.
* For headers that intentionally export symbols from another header, add
  `// IWYU pragma: export` after the `#include` whose symbols are meant to
  be exported.
* If all else fails, `// IWYU pragma: keep` will suppress the warning.

## Limitations

`clangd` parses the code with full compiler flags, which gives it accurate type information. However it also means that any code not compiled may show errors or get missed by refactoring tools. Enabling full build options gives best results. For platform specific code there is no solution though.

## Demo

<video src="../videos/clangd-lsp-blender.mp4" width="800" controls></video>

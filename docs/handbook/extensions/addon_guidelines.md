<!--
NOTE(@ideasman42): This doesn't cover bundled (core) add-ons since these are not indented
to be the place where new add-ons are submitted.
If necessary we could document conventions for core add-ons separately from extensions.
Arguably `extension.blender.org` should document it's own policy outside of developer docs,
however these conventions are generally good community guidelines which would be good
for other (non Blender) repositories to follow.
-->

# Add-on Guidelines

To prepare your add-on for submission as an extension to Blenders official repository,
add-ons must:

<!--
NOTE(@ideasman42): keep this list short and to the point.
Issues relating to code-quality, formatting, naming etc can be handled elsewhere.
-->

- **Respect Blender's "Allow Online Access":**<br>
  Not making connections to the internet when `bpy.app.online_access` is False.

- **Not Interfere with Other Add-ons:**<br>
  Making changes to other add-ons is forbidden (installing/updating/removing etc).<br>
  If the extension depends on another, an error can be shown when accessing functionality
  that requires the other extension.

- **Be Self Contained:**<br>
  Add-ons must not install Python modules, PIP packages, Python-wheels etc.<br>
  If some additional software required that cannot be bundled, this can be run by the user.

- **Bundle Modules:**<br>
  Add-ons must only load modules into the packages name-space (typically as sub-modules).<br>
  Manipulating Blender's module loading such as changing the module search path or inserting
  modules directly into the global module dictionary is forbidden
  as this makes global changes outside the add-ons name-space.<br>
  Extensions may include 3rd party module as python-wheels.

- **Support "System" (read-only) installation:**<br>
  Add-ons must not write files in their own directories since "System"
  repositories may be used on read-only file-systems.<br>
  Use `bpy.utils.extension_path_user(__package__, create=True)` to create a local directory
  extensions may use to store user local files.

> NOTE: **Hint**
> While these are requirements for `extensions.blender.org`,
> we recommend following them for add-ons hosted elsewhere
> because they help ensure add-ons work in a verity of configurations.

# See also

- [Blender Manual for
  Add-ons](https://docs.blender.org/manual/en/dev/editors/preferences/extensions.html)<br>
  General information about add-on **usage**.

- [Blender Manual for Creating Extensions &
  Add-ons](https://docs.blender.org/manual/en/4.2/advanced/extensions/index.html)<br>
  General information about add-on **usage**.

- [Style Guide for Python Code](http://www.python.org/dev/peps/pep-0008/)<br>
  Recommended coding conventions for submitted Python code

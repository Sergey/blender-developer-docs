# Spanish Team

This page is used as translation reference by members of the Spanish
translation team for the UI and Manual.  
Esta página tiene por cometido servir de referencia para la traducción
de la interfaz y el manual de Blender al español.

## Contacto

El equipo de traducción podrá ser contactado a través [del canal
de traducciones](https://chat.blender.org/#/room/#translations-es:blender.org) de
chat.blender.org.

## Reglas y convenciones

### Neutralidad

En pos de lograr que usuarios de cualquier región, cultura y edad
sientan comodidad al interactuar con el programa y su manual, se buscará
la neutralidad, tanto en el tratamiento, como en los términos a ser usados.

### Tratamiento impersonal

Como forma de evitar tener que decantarse entre un tratamiento de confianza
o uno de respeto, existe la alternativa de la forma de tratamiento impersonal,
que básicamente elude el dilema mediante el uso del siguiente tipo de frase:

    "You can now open the file in your system."  
    "Allows to select the faces you need to remove."

que deberá traducirse como (impersonal):
    
    "Ahora, será posible abrir el archivo en el sistema."  
    "Permite seleccionar las caras que se necesite eliminar."
   
y no como (tú):

    "Ahora, puedes abrir el archivo en tu sistema."  
    "Permite seleccionar las caras que necesites eliminar."

o como (vos):

    "Ahora, podés abrir el archivo en tu sistema."  
    "Permite seleccionar las caras que necesités eliminar."

o como (ud.):

    "Ahora, puede abrir el archivo en su sistema."  
    "Permite seleccionar las caras que necesite eliminar."

### Infinitivo al comienzo de frases

En el entendido anterior, los mensajes de la forma:

    "Use X Coordinate"  
    "Define Color"

deberán ser traducidos como:

    "Usar coordenada X"  
    "Definir color"

y no como:

    "Usa cordenada X"  
    "Define el color"

### Universalidad del léxico

Se preferirá el uso de términos con la más amplia aceptación general posible,
como forma de fomentar la mejor comprensión de los conceptos utilizados por
el programa y el manual, en todas las regiones de habla hispana.

### Mayúsculas en títulos

En inglés, [los títulos](wikipedia:Title_Case) utilizan mayúsculas en la
mayoría de las palabras, lo que se puede apreciar en la mayoría de las etiquetas
presentes en Blender:

    "Copy Location"  
    "Auto Run Python Scripts"

¡Este no es el caso en español! Así, salvo excepciones (nombres propios,
referencias a otros elementos de la interfaz, etc.), los títulos sólo
llevarán mayúscula al inicio de una frase:

    "Copiar posición"  
    "Ejecutar automáticamente scripts Python"

### Sin puntos al final de las cadenas

Las convenciones de Blender dictan que no se deberá escribir un punto final para el final
de las cadenas de la interfaz (generalmente, esto se refiere a descripciones
de herramientas). Dicho punto será agregado automáticamente por Blender.  
Esto no será así en el caso del manual, en donde sí habrá que incluir el punto final en las
cadenas traducidas.

### Verbos factorizados

Algunas oraciones podrán ser traducidas mediante la utilización de dos verbos opuestos.
Por ejemplo:

    Toggle Selection

se podría traducir al español como "Seleccionar o deseleccionar". En estos casos, será posible
utilizar paréntesis para reducir la oración a

    (De)seleccionar

aunque la oración también podría ser traducida como

    Alternar selección

### Separador decimal

Si bien en las distintas regiones de habla hispana se utiliza tanto la
coma como el punto para la separación decimal, al traducir la interfaz y
el manual de Blender se deberá conservar el punto decimal como estándar,
dado que Blender no cuenta con soporte para elegir el separador decimal,
siendo el punto el único válido.

## Consejos y lineamientos de estilo para la traducción

### Comprender antes de traducir

Para traducir correctamente un texto, es necesario comprender en qué
contexto se está utilizando: en qué parte de la interfaz y qué designa.
Ciertas palabras pueden tener varias traducciones muy diferentes y,
entonces, será fundamental saber cuál deberá ser usada para evitar
malas interpretaciones. Muchas palabras en inglés pueden usarse tanto
como verbo o sustantivo. Entonces:

- *Pin Cloth* se traduce como "Fijar ropa".
- *Pin Stiffness* se traduciría como "Rigidez de fijación" (de hecho, "*Pinning stiffness*" sería más exacto aquí...).

### Terminología

Existe un
[glosario](https://translate.blender.org/browse/blender-ui/glossary/es/)
disponible en el sistema de traducción en línea. También existe un
[glosario](https://docs.blender.org/manual/es/latest/glossary/index.html)
en el propio Manual de Blender.  
Salvo excepciones, las palabras y expresiones en inglés deberán ser traducidas
cuando exista un equivalente en español.  
En algunos casos excepcionales, sin embargo, se han conservado los términos en
inglés.

### Terminología usada en el Manual

Los términos utilizados en las traducciones del manual deberán corresponder siempre
a los términos visibles en la interfaz de Blender en español.

### Coherencia

Por regla general, deberá mantenerse una coherencia en la traducción de los términos,
dentro de un mismo contexto. Para esto, resultará útil revisar la traducción del
término previamente usada en otras partes del propio manual o de la interfaz.

### Falta de contextos en el Manual

Actualmente, el sistema de traducción del Manual de Blender no soporta el uso de
diferentes *contextos* para un misma cadena de texto. A causa de esto, será inevitable
que en ciertas partes del manual se muestre una traducción que parezca no adaptarse al
contexto en el que se encuentra. Desafortunadamente, por el momento no existe una
solución a este problema.

### Falta de desambiguación en la Interfaz

Si bien en la traducción de la interfaz sí existe la posibilidad de asignar
diferentes contextos a cadenas idénticas. Igualmente, ciertas veces, los
desarrolladores podrán asignar inadvertidamente para una nueva función, una cadena pre
existente que es usada dentro de otro contexto. Esto generará un conflicto y surgirá la
necesidad de advertir de esta situación para que pueda ser subsanada.

### Términos no traducibles

Existen ciertos términos que deberán preservarse en su forma original.  
Usualmente se trata de nombres de marca o de siglas, como en los ejemplos a continuación:

    Cycles, EEVEE, Workbench, Freestyle, Grease Pencil, JPEG, OpenEXR,  
    Open Shading Language, macOS, etc.

Asimismo, estos términos deberán preservarse exactamente en su formato original, esto es, con su
misma combinación de mayúsculas y minúsculas.

### Recursos útiles durante la traducción

- El [Diccionario de la lengua española](https://dle.rae.es/) editado por la
  Real Academia Española, en colaboración con las veintidós corporaciones
  integradas en la Asociación de Academias de la Lengua Española (ASALE).
- [Wikipedia](https://es.wikipedia.org/): cuando un artículo exista en inglés y español, los enlaces entre
  idiomas a menudo permiten obtener la traducción buscada.
- El [Wikcionario](https://es.wiktionary.org/), ya sea en las traducciones de
  la entrada en inglés o directamente en la propia entrada en español.

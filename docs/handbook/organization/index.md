# Organization

## Modules

Blender development is organized in modules, each covering and area of
the Blender software. Modules consist of owners and members. Read more
about [modules and their organization](modules.md).

For those looking to contribute to Blender, see the
[module list](https://projects.blender.org/blender/blender/wiki/Home)
to find the contact point for each area of Blender.

## Projects

[Projects](projects.md) gather multiple developers and contributors to
work on specific topics. They have a start and end date, while module
work is always ongoing.

## Blender Foundation

The [Blender Foundation](https://www.blender.org/about/foundation/)
is an independent public benefit organization with the purpose to provide
a complete, free and open source 3D creation pipeline, managed by public
projects on blender.org.

**Chairman**: [Ton Roosendaal](https://projects.blender.org/Ton)

## Blender Institute

The [Blender Institute](https://www.blender.org/about/institute/)
is a spin-off corporation of the Blender Foundation, hosting the
foundation's offices and employing developers to work on Blender software.

Two development coordinators are the contact point for developers employed
by the Blender Institute. Thomas Dinges is also the contact point for those
looking to get involved in Blender development.

**Development Coordinators**:
[Fiona Cohen](https://projects.blender.org/FoxGlove),
[Thomas Dinges](https://projects.blender.org/ThomasDinges)

## Admins

The admins are accountable for making sure the blender.org project runs
well as a whole.

This is only the project as a whole, the roadmap and strategy. Module
and project owners are accountable for their own areas, and (can)
consult with admins and go to them for support.

**Admins**:
[Bastien Montagne](https://projects.blender.org/mont29),
[Campbell Barton](https://projects.blender.org/ideasman42),
[Jacques Lucke](https://projects.blender.org/JacquesLucke),
[Philipp Oeser](https://projects.blender.org/lichtwerk),
[Thomas Dinges](https://projects.blender.org/ThomasDinges)

### Responsibilities

- Help to keep modules and projects running well.
- Appoint module owners.
- Propose and approve dev grants and development hires.
- Review and follow-up on projects.
- Make decisions when no consensus can be reached among contributors.

The admins report to the Blender Foundation chairman on these topics.

## Decision Making

Decisions are generally made in consensus between contributors, and
within modules and projects by their owners and members.

However if no consensus can be found the admins can make the decision.

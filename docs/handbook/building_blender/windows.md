# Building Blender on Windows

These are instructions to build Blender for Windows, with Microsoft
Visual Studio.

## Quick Setup

Building Blender is not as hard as most people would think. For
beginners, easiest is to follow these steps carefully. For more details
and alternative ways to set up a build environment, see below.

### Install Development Tools

Git, CMake and Visual Studio **must all be installed**.

- Install [Visual Studio 2019 or 2022 Community Edition](https://visualstudio.microsoft.com)
  (free, be sure to install the 'Desktop Development with C++' workload)
- Install [Git for Windows](https://gitforwindows.org/)
  - In the installer, choose to **add Git to your PATH** to ensure
    `make update` can correctly function.
- Install [CMake](https://cmake.org)
  - In the installer set the system path option to **Add CMake to the
    system PATH for all users**.

### Starting the Command Prompt

Then open the command prompt window by hitting the Windows key+R, and
then typing `cmd.exe`.

> NOTE: It is important that you use `cmd.exe` and not powershell or any
other shell, the instructions below may not work otherwise.

### Download Sources and Libraries

Create a folder to store your copy of the Blender source code. This
guide will assume your chosen folder is `C:\blender-git`.

Download the Blender source code:

```powershell
cd C:\blender-git
git clone https://projects.blender.org/blender/blender.git
```

Download the Blender libraries:

```powershell
cd C:\blender-git\blender
make update # (1)!
```

1. When using Git Bash (or similar) instead of the Windows command prompt,
use `./make.bat` instead of `make`:
```powershell
./make.bat update
```

`make` will automatically detect the libraries you need and offer to
download them for you. Do note that this set of libraries is several
gigabytes large and may take a while to download.

### Compile Blender

```powershell
cd C:\blender-git\blender
make # (1)!
```

1. When using Git Bash (or similar) instead of the Windows command prompt,
use `./make.bat` instead of `make`.

Once the build finishes you'll get one or more message like this,
pointing to the location of your freshly built Blender:
``` text
-- Installing:
C:\blender-git\build_windows_Full_x64_vc14_Release\bin\Release\\somefile\]
```

in that folder you should find your `blender.exe` you can start to run
blender.

To update to the latest changes afterwards, run:

```powershell
cd C:\blender-git\blender
make update
make
```

## Branches

With the quick setup instructions the `main` branch will be built by
default, which contains the latest Blender version under development.

To build another branch or a [pull request](../contributing/pull_requests.md),
first checkout the branch and then run `make update` to update the add-ons,
libraries and tests to match.

```powershell
cd C:\blender-git\blender
git checkout <branch-name>
make update
make
```

## Visual Studio IDE

Blender supports Visual Studio 2019 16.9.16 and VS2022. Older versions will not work.

See instructions to [work in the Visual Studio IDE](../development_environments/visual_studio.md),
instead of building from the command line.

Remember to build the **INSTALL** target, otherwise Blender will not launch due
to missing dlls.

## Source Control

### Git

Git is required for accessing the Blender source code. [Git for
Windows](https://gitforwindows.org/) command line utilities are
sufficient, but more user friendly user interfaces exist. For example
[TortoiseGit](http://tortoisegit.org/) integrates with Windows explorer.

## Build Options

By default Blender builds will be similar to official releases. Many
[Build Options and Targets](options.md) are available for debugging,
faster builds, and to enable or disable various features.

For a full list of the optional targets, run:

    make help

## Manual Setup

The quick setup instructions use the convenience batch file to set up
a build directory, run CMake configuration and build all with one
`make` command.

For more control, you can manually do these steps.

### CMake

Install and run the CMake application.

* Set the Blender source directory, e.g. `C:\blender-git\blender`
* Choose a Build Path located outside the source directory, e.g. `C:\blender-git\build`
* Click Configure, and change options as desired
* Click Generate

Any changes to the build-system will re-generate project files automatically
from within [Visual Studio](../development_environments/visual_studio.md) when building.

### Compile from the Command Line

From the command prompt, navigate to the chosen build path folder.

Then in the following command just substitute `[CONFIG]` with one of the
following options: `Release`, `Debug`, `RelWithDebInfo`.

```powershell
cmake --build . --target INSTALL --config [CONFIG]
```

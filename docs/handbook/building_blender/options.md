# Build Options

By default Blender builds will be similar to official releases. Many
build options are available for debugging, faster builds, and to enable
or disable various features.

## Build Targets

There are a few build targets to choose the overall type of build.

- <b>`make`</b> - build with all features enabled, except CUDA and Optix
- <b>`make lite`</b> - build with the absolute minimum amount of
  features enabled, either for faster builds or to avoid installing many
  dependencies
- <b>`make release`</b> - complete build with all options enabled
  including [CUDA and Optix](cycles_gpu_binaries.md), matching the
  releases on blender.org
- <b>`make headless`</b> - build designed to run from the command line
  without an interface (for renderfarm or server automation)
- <b>`make bpy`</b> - build [as a python module](python_module.md) which can
  be loaded from python directly

For a full list of targets, run <b>`make help`</b>.

## Setup For Developers

### Debug Builds

On Windows in Visual Studio or Xcode on macOS, a single build folder can
contain both release and debug builds, and you can switch between them
in the IDE.

For other platforms, the easiest way to set up a debug build is to build
the debug target. This will create a separate build in
`../build_<platform>_debug`.

``` bash
make debug
```

The build type of an existing build can also be changed by setting
`CMAKE_BUILD_TYPE` in the CMake configuration to either `Debug` or
`RelWithDebInfo`.

### Developer Options

We recommend developers to configure CMake to enable [address
sanitizer](../tooling/asan.md), automated
tests and options for faster builds. More details about [tools for
Blender development are here](../tooling/index.md).

The most common options can be enabled by using the `developer` target,
which can be combined with other targets. For example:

``` bash
make developer
```

Or to combine it with a debug build as you usually would:

``` bash
make debug developer
```

Note that, due to enabled asserts even in Release configs, and address
sanitizer overhead, the `developer` target might not be suitable for
performance measurements.

### Ninja

For faster builds, the [Ninja build system](https://ninja-build.org/)
can be used. If ninja is installed and available in the path, it can be
added to the `make` command when setting up the build folder. If there
already exists a build folder with a different build system, the folder
must be removed first. Example command to set up ninja build:

``` bash
make debug developer ninja
```

### Caching

Caching helps make rebuilds faster, especially when switching between
git revisions and branches.

#### Linux and macOS

If [ccache](https://ccache.dev/) is installed, it can be used as
follows:

``` bash
make ccache
```

for makefiles or

    make ccache ninja

for Ninja or

``` bash
make debug developer ccache ninja
```

for Ninja, Debug build type, and developer options enabled.

The equivalent of the above for Xcode generator would be:

``` bash
cmake -S . -B ../build_xcode -C build_files/cmake/config/blender_developer.cmake -DWITH_COMPILER_CCACHE=ON -G Xcode
```

#### Windows

If [sccache](https://github.com/mozilla/sccache) is installed and
available in the path it can be used, however it will only function with
the ninja generator.

``` bash
make developer ninja sccache 
```

## Editing CMake Options

By default, the `CMakeCache.txt` configuration file will be in
`../build_<platform>`. There are a multiple ways to edit it.

- Editing `CMakeCache.txt` in a text editor.
- Opening `CMakeCache.txt` with the CMake GUI, to easily change options
  and re-configure. For example on Linux:

  
``` bash
cmake-gui ../build_<platform>
```

- Using **ccmake**, for a command line text interface to easily change
  options and re-configure.

  
``` bash
ccmake ../build_<platform>
```

- cmake parameters can also be set on the command line, for example:

``` bash
cmake ../build_<platform> \
    -DCMAKE_INSTALL_PREFIX=/opt/blender \
    -DWITH_INSTALL_PORTABLE=OFF \
    -DWITH_BUILDINFO=OFF
```

  
These commands are exactly those found in `CMakeCache.txt` so you can
copy commands from there and use them in the command line without
running ccmake.

| CMake GUI                       | ccmake text interface        |
| ------------------------------- | ---------------------------- |
| ![](../../images/Cmake-gui.jpg) | ![](../../images/Ccmake.jpg) |

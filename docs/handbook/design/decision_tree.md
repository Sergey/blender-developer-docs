# Decision Tree

What to consider when proposing (or analyzing) a design?

Example of questions:

* Which problem is this trying to solve?
* Who is this solving for?
  (what is the target audience: beginners? Experienced users? Animators? Riggers?)
* Within the context of Blender as a multi-purpose DCC, how does this feature fit on it?
* Which areas of Blender are affected by this design?
* What is the impact on existing workflows?
* How does it impact the other user-groups and workflows in Blender?
* Which new problems do this introduce?

## Bottom-Up

Sometimes the decision tree is a bottom-up process,
where the different questions can cascade up to different levels.

Take this example: “I want to add a new Bake button on the viewport header”.

On a surface level you need to see how this affects the real-state of the header,
which workflow you are prioritizing with this change, and what may need to be reshuffled.


On a second level you can look at whether Blender has other similar buttons.
In this case you can quickly see that there are no tools (operators)
called directly from the header.

On a third intermediate level you could see how this affects other editors.
If we have Baking on the viewport, should we have this also for the animation editor?
The Image Editor?

On a higher level you need to understand what will this new button bring to the workflow,
what is the problem leading to this solution, and whether there are other solutions that
may be more suitable.

Similar examples: “Should we have buttons in the Properties Editor?”,
“Should we have buttons on nodes?”

## Top-Down

The opposite can also happen and the initial concept is a high-level concept.
It is also important to know how it impacts the other areas of Blender,
how the concept cascades down to the workflows, editors and ultimately the UI/UX.


# Design Proposals

"Design isn't a product that designers produce, design is a process that designers facilitate."
--Leah Buley

This section is intended for everyone involved in the Blender development process,
regardless of whether they are contributors, modules or staff members.

This aims at helping the modules to handle better the contribution from the community
and present and forward with its own design proposals.

Topics:

* [Decision Tree](decision_tree.md)
* [Idea, Concept, Design](ideas_concept.md)
* [Communication and Presentation](communication_presentation.md)
* [Examples](examples/index.md)

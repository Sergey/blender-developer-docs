# Visual Studio

Visual Studio is the official Windows development environment from Microsoft.

The Community editions are free for open source development and have all the
features needed for building and developing Blender. Professional editions
work fine as well.

## Generating the Project

A Visual Studio project is automatically created as part of the process of
[building Blender on Windows](../building_blender/windows.md).

Those instructions will generate both the project and compile Blender in
one command. To generate just the project, the following command can be
used instead:

    cd C:\blender-git\blender
    make full nobuild

If you want to customize your build (i.e. select a Visual Studio
version, platform architecture, of Blender feature set), you can get a
list of customizable options by typing:

    make help

Once the batch file finishes it should tell you where the project files
have been written, for example:

    -- Build files have been written to: c:/blender-git/build_windows_Full_x64_vc14_Release

## Building

In the build folder, find a `Blender.sln` find which you can open with
the Visual Studio IDE.

Then in the Solution Explorer, find the **INSTALL** target, right click
and select "Build". This will both compile Blender, and install all the
required files to the Blender output folder.

Once the install target has been built once, you can now work with the code
as usual in Visual Studio. Build > Build Solution can be used to rebuild
after making changes.

> WARNING: **Important**
> If you skip the INSTALL step, Blender will fail to start.
> You don't have to run this for every build, but it should be run
> after big changes like `make update`, switching branches or changing
> from release to debug configuration.

## ClangFormat

Visual Studio has built-in support for formatting and automatically uses
the `.clang-format` file.

However, the version of clang-format shipped
with Visual Studio might not understand all the directives inside
`.clang-format` file. You might want to point VS to Blender's clang-format.
Under `Tools -> Options -> Text Editor -> C/C++ -> Code Style -> Formatting -> General`,
check "Use custom path to clang-format.exe",
and point it to `lib\windows_x64\llvm\bin\clang-format.exe`

It is recommended to format on save, to avoid forgetting.

- [C/C++ Formatting Documentation](https://docs.microsoft.com/en-us/visualstudio/ide/reference/options-text-editor-c-cpp-formatting)
- [Extension to format on file save](https://marketplace.visualstudio.com/items?itemName=mynkow.FormatdocumentonSave)

# Google Summer of Code - 2022 Projects

**Progress updates for 2022**

## Current State

The information in this page still to be populated through the duration
of the Google Summer of Code.

**1<sup>st</sup> evaluation -** 25 - 29 July
**Final submission -** 5 - 12 September

## Communication

### Development Forum

All Blender and code related topics will go to the devtalk forum. Here
students will also post their weekly reports, and can ask for feedback
on topics or share intermediate results with everyone.

<https://devtalk.blender.org/c/blender/summer-of-code>

### chat.blender.org

For real-time discussions between students and mentors. Weekly meetings
will be scheduled here as well.

<https://chat.blender.org/#/room/#gsoc-2022:blender.org>

## Projects

Google has granted 4 projects for the Blender Foundation.

------------------------------------------------------------------------

### 3D Text Usability Improvements

by **Yash Dabhade**
*Mentors:* Campbell Barton
*Branch:* soc-2022-text-usability

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:YashDabhade/GSoC2022/Proposal.html)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2022-3d-text-usability-improvements-weekly-reports/24746)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:YashDabhade/GSoC2022/FinalReport.html)

### Integrating Many Lights Sampling into Cycles

by **Jeffrey Liu**
*Mentors:* Brecht Van Lommel
*Branch:* soc-2022-many-lights-sampling

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:JeffreyLiu/GSoC2022/Proposal.html)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2022-many-lights-sampling-in-cycles-x-weekly-report/24732)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:JeffreyLiu/GSoC2022/FinalReport.html)

### Soft Bodies Simulation using Extended Position-Based Dynamics

by **Aarnav Dhanuka**
*Mentors:* Sebastian Parborg
*Branch:* soc-2022-soft-bodies

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:AarnavDhanuka/GSoC2022/Proposal.html)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2022-soft-body-simulations-using-xpbd-weekly-reports/24740)
- [Final Report](https://archive.blender.org/wiki/2024/wiki/User:AarnavDhanuka/GSoC2022/FinalReport.html)

### Waveform Drawing Improvements

by **Ming Xu**
*Mentors:* Richard Antalík
*Branch:* soc-2022-waveform-drawing

- [Proposal](https://archive.blender.org/wiki/2024/wiki/User:MingXu/GSoC2022/Proposal.html)
- [Weekly
  Report](https://devtalk.blender.org/t/gsoc-2022-waveform-drawing-improvements-weekly-report/24722)
- Final Report

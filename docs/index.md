---
hide:
  - navigation
  - toc
  # Hide Preview/Next footer.
  - footer
---

# Blender Developer Documentation

<div class="grid cards" markdown>

- **Blender Developer Handbook**

  ---
  Build blender yourself, get started and read up all general information needed
  to develop Blender. <br/>
  For new and experienced developers alike.

  [:octicons-arrow-right-24: Check out the handbook](handbook/index.md)

- **Features**

  ---
  Dive into the many (sub-)modules and features of Blender.

  [:octicons-arrow-right-24: See the features](features/index.md)

</div>

<div class="grid cards" markdown>

- **Summer of Code**

  ---
  A program to bring new contributors into open source development.

  [:octicons-arrow-right-24: Learn about Summer of Code](programs/gsoc/index.md)

- **Release Notes**

  ---
  Every release of Blender is packed with features! Browse through the history,
  or check out the latest and greatest.

  [:octicons-arrow-right-24: Release Notes](release_notes/index.md)
  [:octicons-arrow-right-24: Compatibility Changes](release_notes/compatibility/index.md)
</div>

---

### Contribute Documentation

Help build and improve this documentation platform.

[:octicons-arrow-right-24: Read more](contribute/index.md)

[:octicons-arrow-right-24: Build instructions](contribute/build_instructions.md)

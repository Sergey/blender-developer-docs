# Virtual Reality

## Object Extras / Object Type Visibility

Object extras such as image-empties can now be shown in the VR
viewport/headset display. Being able to see reference images in VR can
be useful for architectural walkthroughs and 3D modeling applications.

Since users may not want to see all object extras (lights, cameras,
etc.), per-object-type visibility settings are also newly available as
session options
(blender/blender@5c92c04518b5).

![](../../images/Xr_object_type_visibility.png "../../images/Xr_object_type_visibility.png"){style="width:300px;"}

## Other Changes & Additions

- New `Camera Landmark from Session` operator for VR landmarks that
  creates a new camera and `Custom Object`-type landmark from the
  current VR headset pose
  (blender/blender-addons@5aa25f6a3f0a).
- Support for HTC Vive Focus 3 controllers
  (blender/blender@964d3a38fac8).

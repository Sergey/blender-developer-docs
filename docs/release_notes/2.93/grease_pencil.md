# Blender 2.93: Grease Pencil

## LineArt Modifier

New Line Art modifier is now included to provide functionality of
generating stylized lines from geometries in the scene.
(blender/blender@3e87d8a4315d)

![Line art demonstration render of a 727 cut-away drawing.](../../images/Cutaway_0401_bw_3k.png){style="width:800px;"}

- Produce accurate occlusion result from any geometry.
- Intersection lines can be generated and selected separately for
  styling.
- Chaining allows strokes to be connected thus allow smooth display of
  grease pencil styles and textures.
- Vertex weight in the mesh can be transferred into grease pencil to
  allow further style control of generated strokes.
- Strokes can be baked to allow further edits in the viewport.

## Import/Export

- New operators to Import SVG to grease pencil objects and export grease
  pencil objects in SVG and PDF format.
  (blender/blender@a8a92cd15a52)
- SVG Importer use vertex color for colors.

[Demo Video](https://www.youtube.com/watch?v=BMm0KeMJsI4)

## User Interface

- **IMPORTANT CHANGE:** Now `Autokey` button must be enabled to
  generate new keyframes in Draw, Edit or Sculpt mode. If the Autokey
  button is disabled, the last available frame is used. This is more
  aligned with how Blender works and with other 2D softwares. Also, 2D
  template has now Autokey button enabled by default.
  (blender/blender@6a662ffda836)
  - Note: Old files are not changed. Enable `Autokey` button manually
    to get the old behavior.

<!-- -->

- Now the multiframe is disabled when play animation. Before, it was
  impossible to see the animation because all frames were visible.
  (blender/blender@e7c36ce43cf6)
- Swap positions of Pie menu options for Vertex Paint and Weight Paint.
  (blender/blender@20d0bb43afc8)
- New multiframe display mode. Keyframes before or after of the active
  frame are displayed using onion colors. This can be disable using
  Onion overlay options.
- Dopesheet now display a bar between keyframes to indicate that
  keyframe is still used.
  (blender/blender@e44b2ada3e4a)
  (blender/blender@296984b40e03)
- Dopesheet context menu has been reorganized and some options added.
  (blender/blender@15a4ed3aff40)
- Materials `Options` panel renamed to `Settings` to keep
  consinstency.
  (blender/blender@51c7ff9222c3)
- Annotations now has a `Opacity` parameter (this was removed during
  the refactor and never was added again).
  (blender/blender@c62e60f01f2a)

## Operators

- `Resolution` parameter for Fill Tool has been renamed to
  `Precision` and allows values below 1. This is very handy for
  storyboarding.
  (blender/blender@6704372f047c)
- New operator to Duplicate Layers but with empty Keyframes. This
  operator is very useful to duplicate the keyframes but don't copy the
  strokes to be used in cleanup or painting works.
  (blender/blender@cfd54ad7d78e)
- New option to Trace images starting in the current frame and not
  always 1.
  (blender/blender@e747c07156cc)

## Tools

- New Layer transform parameters.
  (blender/blender@e02d84eb3bea)
- New Multiframe support in Draw mode (included in fill tool refactor
  commit).

### Fill Tool

A refactor of the fill tool has been done inorder to solve several
problems we had since the first version of the tool.
(blender/blender@1352d81b1747)

Changes:

- The filling speed has been improved for each step of the process with
  the optimization of each algorithm/function.
- New `AutoFit` option to fill areas outside of the viewport. When
  enable, the total size of the frame is calculated to fit the filling
  area.
- New support multiframe filling. Now it is possible to fill multiple
  similar frames in one go.
- New `Stroke Extension` option to create temporary closing strokes.
  These strokes can be displayed and adjusted dynamically using wheel
  mouse or PageUp/Down keys. Use `Wheel + Shift` key for precise
  adjustments.
- Parameter `Resolution` now is named `Precision` and has been moved
  to topbar.
- `Resolution` now has decimals and can be lower than 1 to allow quick
  filling in storyboarding workflows. Maximum value has been set as 5.
- Parameter `Simplify` has been moved to Advanced panel.
- Improved fill outline detection. In some cases, the outline penetrated
  the area to be filled with unexpected results.
- Fill is canceled if the filled area is not closed. This avoids ugly
  full screen fill.
  (blender/blender@c489bb7c016f)
- Fixes some corner case bugs with infinite loops.

[Demo Video](https://www.youtube.com/watch?v=Jtp0vF3PlbE)

### Interpolate Tools

Following with the changes included to interpolate strokes of different
number of points, a full review has been done in the interpolation
tools.
(blender/blender@6bba83058986)

- Interpolate now is a tool and not an operator. It was not logic to
  have this tool as a button.
- Interpolate tool parameters have been moved to topbar.
- Interpolate popover has been removed from topbar and interpolate
  `Sequence` operator has been moved to grease pencil menu.
- Interpolate Sequence now include a Redo panel.
- Interpolate tool now allows to select the strokes by pairs. This
  allows to interpolate any stroke with any stroke and not as before
  that it was only possible by drawing order. If no stroke is selected,
  the interpolation is done as before.
- Now is possible interpolate again if a previous keyframe exist.
  Before, it was impossible to interpolate two times in same frame and
  this made impossible to do the interpolation by groups of frames.
- New automatic option to `Flip strokes` if the initial stroke and end
  stroke are not in the right position because direction is different.
  Also the flip can be set manually for corner cases.
- Cleanup of menus related to interpolate.
- Fixed some bugs and removed parameters from scene because now all are
  tool or operator contained.
- Some code cleanup and function renames.

[Demo Video](https://www.youtube.com/watch?v=88FsF6ontPk)

This commit also includes the some codebase to future implementation of
the concept `Vertex Active` that now does not exist in grease pencil.

## Modifiers and VFX

- `Seed` parameter moved outside Randomize panel in Noise Modifier.
  (blender/blender@3191b76eb534)
- New Noise Offset parameter in Noise Modifier.
  (blender/blender@7a4bdc3a4feb)
- Remove limitation to use only one Lattice modifier.
  (blender/blender@670c1fdf64d1)

## Python

- New API to force the stroke recalculation. In some scripts, the
  coordinates of the points are manipulated and there was no way to
  update the stroke.
  (blender/blender@faf45091b454)
- New API to manipulate weight points. The old RNA prop was wrong and
  never was usable.
  (blender/blender@71e63153ebf3)

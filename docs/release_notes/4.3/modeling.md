# Blender 4.3: Modeling & UV

## New Unwrapping Method "Minimum Stretch"

A new iterative unwrapping method has been added which can deliver results with
less distortion by iteratively refining the result.

This can be used from the Unwrap menu as well as "Live Unwrap".

This uses the SLIM algorithm (Scalable Local Injective Mappings) internally,
see the [research publication](https://igl.ethz.ch/projects/slim/).
(blender/blender@788bc5158ed2b9ee3194bbeeed340d9e3e003781)

<figure markdown="span">
<div class="grid" markdown>
<figure markdown="span">
  ![](images/uv_slim.webp)
<figcaption>Minimum Stretch (SLIM)</figcaption>
</figure>
<figure markdown="span">
  ![](images/uv_conformal.webp)
<figcaption>Conformal</figcaption>
</figure>
<figure markdown="span">
  ![](images/uv_angle_based.webp)
<figcaption>Angle Based (default)</figcaption>
</figure>
</div>
</figure>

## Bevel Modifier

The bevel modifier now has options to use custom attributes as bevel weights
(blender/blender@cc5ed48cd34d89522e885bec0adb91623053e5e8).

## Copy Material to Selected

`Copy Material to Selected` now sets the `Link` type of the selected objects
to match those of the active object.
Without this, it would change the underlying material slots on the mesh data block,
which is probably not what the user intends. With the new behavior,
it now changes the selected objects to use object-linked materials
(blender/blender@2b9c7b3e8f).


# Blender 4.3 Release Notes

Blender 4.3 was released on November 19, 2024.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/4-3/).

* [Animation & Rigging](animation_rigging.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Geometry Nodes](geometry_nodes.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [User Interface](user_interface.md)
* [Video Sequencer](sequencer.md)

## Compatibility
### Windows on Arm (experimental)

This is a new step towards bringing Blender to new architectures. Test Blender on **Windows on Arm**
by [downloading the arm64 build](https://www.blender.org/download/).

### Cycles

* On macOS, support for AMD and Intel GPUs through the Metal backend of Cycles has been removed
  (blender/blender@c8340cf7541515a17995c30b4a236ac2a326f670)
* The minimum driver version requirement for NVIDIA GPUs has been increased to 495.89
  as a result of updating to OptiX 7.4
  (blender/blender@4f5b6b114d7aaf6b7638d6e05e82b1faf47413f0)
* Support for Vega in Cycles AMD HIP backend has been removed
  (blender/blender@c2f93e0f68aec9fa48ba311401ef2545c53b24ba)

[See all compatibility changes](../../release_notes/compatibility/index.md)

## Bugfixes
* [Fixes for issues present in previous versions](bugfixes.md)

## [Corrective Releases](corrective_releases.md)

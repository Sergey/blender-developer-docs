# Blender 4.3: Bugfixes

There are a total of 460 commits in 4.3 that fixed issues that existed in Blender 4.2 or older.

## Animation & Rigging
 * Rigify UI bug on selected bone in pose mode (Rigify type submenu) [[dda364637c1](https://projects.blender.org/blender/blender/commit/dda364637c13f76a2b017a2e44778b1f8bdee045)] - Backported to 4.2.4
 * Keyframes get hidden in the timeline when panned in the up direction [[71692abd591](https://projects.blender.org/blender/blender/commit/71692abd5918925149e59d258bb12124346b41bb)]
 * Armature "Assign to Collection" crashes when called with default arguments [[e071bf46908](https://projects.blender.org/blender/blender/commit/e071bf46908ed689fd107f82a2a3c5cf5eec2421)] - Backported to 4.2.4
 * Baking Action 'Clear Constraints' Incorrect Description [[be54e363330](https://projects.blender.org/blender/blender/commit/be54e363330b84756edf4b9d4a7b4e48c2008f8c)]
 * Other keyframes not deselected when inserting new keys in older files [[d31ec42d24d](https://projects.blender.org/blender/blender/commit/d31ec42d24d529d0ab618bd7e4050ff01174ae00)]
 * Armature bones with Selectability disabled prevent click selection through them [[328ec2b172e](https://projects.blender.org/blender/blender/commit/328ec2b172e8954307f5f7d92b9880c033ec8cdf)] - Backported to 4.2.4
 * Regression Pasting pose on liboverride armature produces warning about 'removing all override data' [[9405bd25d6d](https://projects.blender.org/blender/blender/commit/9405bd25d6df272cb639473e0c3b621a6d8eb23a)]
 * IK Panel: Limit Z min/max properties don't Symmetrize correctly for the Bone [[0753f7f78ca](https://projects.blender.org/blender/blender/commit/0753f7f78ca589141acb9c97f96bafca42d17b1d)]
 * Removing f-curve modifier does not purge modifier motion [[0211a68fb4e](https://projects.blender.org/blender/blender/commit/0211a68fb4e6d159df14735d5f2413ea0eeafc3f)] - Backported to 4.2.4 & 3.6.18
 * Bone Duplicate/Symmetrize doesn't affect pose bone colors [[41a3e6c3bd3](https://projects.blender.org/blender/blender/commit/41a3e6c3bd3a4f94a4fbbbb0e2d54c4abc8af76f)] - Backported to 4.2.4
 * Crash on setting preview range to selected with no keyframes selected [[1246baa81bd](https://projects.blender.org/blender/blender/commit/1246baa81bde0c203a0de932911e6b9df760e167)]
 * Actions created via Python cannot be assigned to action constraints [[0c9ef75df85](https://projects.blender.org/blender/blender/commit/0c9ef75df852d5959237eac5e5043cb467fad258)]
 * Keys deleting when moving key(s) in Graph Editor with shared animation across multiple objects [[6ea0b8cdb20](https://projects.blender.org/blender/blender/commit/6ea0b8cdb2089fc6675ad36c695794bb572bf3b0)] - Backported to 4.2.3
 * Particle system binds keyframes even though they are new particle settings [[44a845d650b](https://projects.blender.org/blender/blender/commit/44a845d650ba020b64a58ee899c73aab1708b5df)]
 * Armature Unselectable EditBones are not selectable from Outliner, assert in debug builds [[1f49236a542](https://projects.blender.org/blender/blender/commit/1f49236a542558aa90d9ddb5df7bf969bf8ef535)] - Backported to 4.2.4
 * NLA Editor's Channel list does not update when object's viewport visibility changes [[f883cb77ba6](https://projects.blender.org/blender/blender/commit/f883cb77ba686c7616ccb7744e7ff25700ef1b65)] - Backported to 4.2.4 & 3.6.18
 * Memory Leak when creating Pose Asset [[d738d76c127](https://projects.blender.org/blender/blender/commit/d738d76c127a8c55893a711af99cbe4976d98671)]
 * "Sync Visible Range" option not working in "Movie Clip Editor Panel" > Tracking Mode > View - Graph [[6036163e143](https://projects.blender.org/blender/blender/commit/6036163e14306f9e86bf826589a16a794095630c)] - Backported to 4.2.4 & 3.6.18
 * Regression Graph Editor: broken f-curve line [[1b23a8983df](https://projects.blender.org/blender/blender/commit/1b23a8983df3a5f409d8d22004bbcdca64071e30)] - Backported to 4.2.2
 * Renaming a custom property removes its animation keys [[4f4add54061](https://projects.blender.org/blender/blender/commit/4f4add54061d21ee86774d1e728e27bd221c8bd2)]
 * Insert keyfame uses wrong values [[2641216344f](https://projects.blender.org/blender/blender/commit/2641216344fa14f726b7dcfad3b2f98ac0b04587)] - Backported to 4.2.1
 * Long Key Line and Interpolation Line turn invisible when only single keyframe visible in viewing range. [[07f88ddd67c](https://projects.blender.org/blender/blender/commit/07f88ddd67c6fc0e921ee753423292ded9e5c6dd)] - Backported to 4.2.1
 * Regression Stick bone tail selection is not drawn [[06fab9f8f1f](https://projects.blender.org/blender/blender/commit/06fab9f8f1f2c23d7371c162875b1f374636ebf6)] - Backported to 4.2.1
 * Can't move channels in Graph Editor except for Object Transforms [[a2b53f47ddb](https://projects.blender.org/blender/blender/commit/a2b53f47ddb474edd8206d0e7ab41bd7a1964895)] - Backported to 4.2.1
 * Anim Rigify `limbs.spline_tentacle` doesn't generate correctly [[ba1f7caffac](https://projects.blender.org/blender/blender/commit/ba1f7caffaca808d047899694bbc602e2c6bd4de)] - Backported to 4.2.1
 * Only insert needed generates empty action [[28dd78457a8](https://projects.blender.org/blender/blender/commit/28dd78457a8b5054aeaf6021f8a57c1c5e05688f)] - Backported to 4.2.1
 * Anim layered Action data is still loaded in 4.2-release [[9427ec3f400](https://projects.blender.org/blender/blender/commit/9427ec3f400375fd72b0b3fea9ea38ac2a91edb9)] - Backported to 4.2.1
 * Graph Editor: Cursor Value to Selection can output position outside normalized range [[163baad17eb](https://projects.blender.org/blender/blender/commit/163baad17ebcf656be68cbc0fe7997c297ee799a)] - Backported to 4.2.1
 * (Unneccessarily) restricted execution in scripted expression drivers (using e.g. "self", "bpy.data") [[1a8053939bd](https://projects.blender.org/blender/blender/commit/1a8053939bd79e0bd7a896ae0f3b1b9195ecb142)] - Backported to 4.2.1
 * Insert keyfame uses wrong values [[780dc67f6d8](https://projects.blender.org/blender/blender/commit/780dc67f6d827213d84514398504f0e3ad705c77)]
 * OBJECT_OT_shape_key_clear ignores slider_min and slider_max [[e45ec6b5910](https://projects.blender.org/blender/blender/commit/e45ec6b591071b92d93d7cb1181a77d86384eb00)]

## Core
 * Blendfile corruption when loading a local Image which is already loaded through linked data [[b863369ae98](https://projects.blender.org/blender/blender/commit/b863369ae984423c6a900a43605052d57e3a294e)]
 * Bpy data `remove` performance issues [[f23478439cd](https://projects.blender.org/blender/blender/commit/f23478439cd5b310925834cb4f934eed421bca31)] - Backported to 4.2.4
 * Library Override retains linked version of the ID [[a1538098291](https://projects.blender.org/blender/blender/commit/a1538098291fe1cca9a06485b685f891032e1981)]
 * Crash on startup on macOS 15 [[50f2857b1a8](https://projects.blender.org/blender/blender/commit/50f2857b1a844b3173c4ea6ad3b294c51c6781b6)]
 * Modifiers can be added to linked (non-overridden) objects using Copy to Selected (and can't be undone) [[eb44c398241](https://projects.blender.org/blender/blender/commit/eb44c39824108679cadfc3de7cb2a82155d54edb)]
 * Blender 4.2 - Compress Saving option gets removed when recovering from AutoSave [[4c2718e318a](https://projects.blender.org/blender/blender/commit/4c2718e318a9db47eb42d6dcc656c92b2cc47c2d)] - Backported to 4.2.2
 * 'Instance to Scene' not handled correctly for child collections in a liboverride hierarchy [[df996ad837a](https://projects.blender.org/blender/blender/commit/df996ad837a0cc78a4e544d30497f52a9830d82b)]
 * Default Value for Viewport Display Color's Alpha Wrong. [[7ff1d5d9e4c](https://projects.blender.org/blender/blender/commit/7ff1d5d9e4cf1f87a617f25ab748d90a7a34282b)]
 * Custom properties not saved into .blend file [[de47fee2e1a](https://projects.blender.org/blender/blender/commit/de47fee2e1a34f91a3b76b1aefaff53701df2c2d)]
 * Crash on undo: Unlinking Scene Collection from Object Properties panel [[6e143cbf753](https://projects.blender.org/blender/blender/commit/6e143cbf753cf0fd054da61606169ca7fd63bbea)] - Backported to 4.2.2
 * Crash when EXR image is loaded in image list [[6d93bf6b44a](https://projects.blender.org/blender/blender/commit/6d93bf6b44a3d4272c8640c50a4703a75acc2ed6)]
 * Blender 4.2.1 and 4.3.0 bad crash guide while file open. [[7597f494b96](https://projects.blender.org/blender/blender/commit/7597f494b96a185113edd874bc294a474e7ce4a0)]
 * Crash when EXR image is loaded in image list [[1fc6a5b9bd5](https://projects.blender.org/blender/blender/commit/1fc6a5b9bd5f7d2763ba4a83f52bbbbcf242170b)] - Backported to 4.2.1
 * Regression Setting parent to triangle changes object location [[29edcef5c9a](https://projects.blender.org/blender/blender/commit/29edcef5c9af1a6c9d64948cfd30b7feb6f6a8e1)] - Backported to 4.2.3
 * Blend File Format: Fix using `0` as `SDNAnr` for non-SDNA data ('raw' arrays of bytes) [[9106383a52f](https://projects.blender.org/blender/blender/commit/9106383a52f08b26d28013291d76dec15dbf1f5c)]
 * Most RNA paths for UI Editors properties are unresolved/invalid [[e7979317ec2](https://projects.blender.org/blender/blender/commit/e7979317ec2514bee3968451a968ddc063c95664)]
 * Active camera context issue [[fc8341538a9](https://projects.blender.org/blender/blender/commit/fc8341538a9cd5e0e4c049497277507b82237c9a)] - Backported to 4.2.1
 * Local view / undo stack issue [[03652c851c0](https://projects.blender.org/blender/blender/commit/03652c851c06371c5ea34dd08db096901711aa03)]
 * Segfault when opening .blend file with invalid shapekey [[ac284c1694d](https://projects.blender.org/blender/blender/commit/ac284c1694d483311854edf0eea56fef01839d95)]
 * Segfault when opening .blend file with invalid shapekey [[9b8001f3709](https://projects.blender.org/blender/blender/commit/9b8001f370936fd02baad8ac1a6b6f79fb7c74f0)]
 * Library overrides on custom property named the same as a "regular" property on an ID not created/preserved on reload [[365a3da148b](https://projects.blender.org/blender/blender/commit/365a3da148be12c8884e61c6991e0ce0765f2bc6)]
 * Crash on Undo after editing an Image from a Linked Library [[a18ab987856](https://projects.blender.org/blender/blender/commit/a18ab98785681af3c8015d2705ad433370ee5965)]

## Grease Pencil
 * Grease Pencil: SVG export creates invalid xml [[9d6794dd5d4](https://projects.blender.org/blender/blender/commit/9d6794dd5d4cbbe0be57680796a49e60a3b1a6fd)]
 * 3D View with grease pencil object, no name for the overlay grease pencil options panel in header [[c07b89b3297](https://projects.blender.org/blender/blender/commit/c07b89b3297f3d2dc02e846d94fbe1a0820efd89)]
 * Assert in keyframe drawing code with GPv2 demo file [[13ceb73941d](https://projects.blender.org/blender/blender/commit/13ceb73941d8439965036e317695c6fe257a0a94)] - Backported to 4.2.1
 * Memory leak when using GPencil Line Art modifier on certain frames [[6041bdd93a9](https://projects.blender.org/blender/blender/commit/6041bdd93a996b40a1a7cce91712a5071e198ee8)] - Backported to 4.2.1
 * Palette editing not working in Color Attribute Paint Mode in Draw Mode [[71e1d795b35](https://projects.blender.org/blender/blender/commit/71e1d795b35458eb4aada9992c21b7d08fa2a22f)] - Backported to 4.2.1

## Modeling
 * Copy to selected modifier crash using Quick Favorites [[a9209f10a66](https://projects.blender.org/blender/blender/commit/a9209f10a66c72dc677ea1e2d6ece584565ff29d)]
 * Add cube tool grid snapping will not work on top of other objects [[b4154c6a0e4](https://projects.blender.org/blender/blender/commit/b4154c6a0e49fa16c08bfe9acd1e540690edf42d)] - Backported to 4.2.4
 * Convert a text object sharing data with another one into mesh give empty mesh [[fee02cc224d](https://projects.blender.org/blender/blender/commit/fee02cc224dcef907043bcc1f0614851a83f3c2a)]
 * Blender 4.2.3 crashes during BMesh garbage collection [[3f6707efd0e](https://projects.blender.org/blender/blender/commit/3f6707efd0e837fc85ef57a4bcfb081a8dc64f70)] - Backported to 4.2.4
 * Unwrap with subsurf fails with triangles at level 1 [[f3773333261](https://projects.blender.org/blender/blender/commit/f37733332614e9ba4d2978998eb153fbefcfdba2)] - Backported to 4.2.4
 * Crash when add a driver to Axis property in SimpleDeform Modifier [[f69fbb41c19](https://projects.blender.org/blender/blender/commit/f69fbb41c19af9bc32934b1a2fd4c3c1e3e4188d)]
 * Changing curve mean weight/radius/tilt changes control point type (if ore than one point is pick selected) [[7e720a8dd67](https://projects.blender.org/blender/blender/commit/7e720a8dd67a5170d2cfa5d629f7532151e7253b)]
 * Crash when using Subdivision Modifier and Armature Modifier on a skinned mesh(v4.2.3LTS) [[ccb92947c53](https://projects.blender.org/blender/blender/commit/ccb92947c534bfe1907c298545d84beca41d8bab)] - Backported to 4.2.4
 * Linked material's node tree can be modified by Copy/Paste Material [[0319b7b50a4](https://projects.blender.org/blender/blender/commit/0319b7b50a4fac1f01df454de7cfb71721ccaaa1)]
 * When using the biset tool, and using cntl to snap to angle intervals, it seems it doesn't snap to exactly 45 degrees. [[dc45169acde](https://projects.blender.org/blender/blender/commit/dc45169acde700470b7780327ae718b459558cab)]
 * New Snap To Grid snaps to "floor"/"visible" 2-dimensional grid only (4.1 was a 3-dimensional grid) [[75ffda39b23](https://projects.blender.org/blender/blender/commit/75ffda39b23455dc7dbe15c452790d05a06d33f5)] - Backported to 4.2.4
 * Precision mode does not work with grid snap [[56c98046a5d](https://projects.blender.org/blender/blender/commit/56c98046a5d2177fc8d215668a3dd7d140d3ce61)] - Backported to 4.2.4
 * Edge Slide freezes blender and mouse cursor movement if geometry is too broken [[580e0af309f](https://projects.blender.org/blender/blender/commit/580e0af309f2f53b162312ebedb0897b2d744c43)] - Backported to 4.2.4
 * Memory leak selecting meshes in multi-object edit-mode [[c852f7402ed](https://projects.blender.org/blender/blender/commit/c852f7402ed52a3b7ac91b7551f08ef15d52e469)] - Backported to 4.2.3
 * Crash after editing multi-user mesh data with two scenes open [[d832bf3ae48](https://projects.blender.org/blender/blender/commit/d832bf3ae483b285f7490c948d0b4fdeb7054064)]
 * Snaping in solid mode (Xray 1.0) doesnt snap other geometry [[b0d37b42673](https://projects.blender.org/blender/blender/commit/b0d37b42673bbd45727ef332782ef07d80e55afe)] - Backported to 4.2.3
 * Normal rotates in wrong direction [[e71c8443549](https://projects.blender.org/blender/blender/commit/e71c844354981f1bd464a0b267baf395111d8517)]
 * Snapping related performance regression in 4.2 [[a13513ab25c](https://projects.blender.org/blender/blender/commit/a13513ab25cc9e83a07fdef0a986d3a21aeb7804)] - Backported to 4.2.3
 * Regression Multires Unsubdivide destroys the vertex groups [[cb4a2036773](https://projects.blender.org/blender/blender/commit/cb4a2036773f5e51e86ca116c34eb5f604c3d740)] - Backported to 4.2.3
 * Crash when Bevel + Mark Seam or Sharp [[8549bc94dea](https://projects.blender.org/blender/blender/commit/8549bc94deaa91577ff5a87520c88141c6b8b6c5)]
 * Crash on clicking 'Lock Camera to View' widget after running 'Reload Scripts' operator. [[0f3793205d2](https://projects.blender.org/blender/blender/commit/0f3793205d2b554442378d1135885714fdf99c26)] - Backported to 4.2.2
 * Multiresolution modifier: Using 'Unsubdivide' while the mesh has faces hidden in edit mode results in Blender freezing completely [[2cc395be064](https://projects.blender.org/blender/blender/commit/2cc395be064b7663d30aff668f1f2dde894968a3)] - Backported to 4.2.2
 * Pin to last issue on Library override object [[1dbb88070b2](https://projects.blender.org/blender/blender/commit/1dbb88070b25779821ac00cbadeff9506f4c4365)] - Backported to 4.2.2
 * Crash while bridging faces [[a51f6b69235](https://projects.blender.org/blender/blender/commit/a51f6b692351012ee70192258ba677c5554e38df)] - Backported to 4.2.2
 * New_from_object crash with mesh DepsgraphObjectInstance.object created from non-mesh instance_object [[4b71496f567](https://projects.blender.org/blender/blender/commit/4b71496f567520679b23b7c4a64e5729cb9cf803)]
 * Unexpected behavior when clearing parent then switching to 'Clear and Keep Transformation' using operator panel [[a4d94ee5d5c](https://projects.blender.org/blender/blender/commit/a4d94ee5d5cdfead60834deb50d8abf34d461715)]
 * Joining some meshes shows warning "Call save() to ensure ..." in console [[fec8591464f](https://projects.blender.org/blender/blender/commit/fec8591464fc965753d287f1767858ab1f90ef3b)] - Backported to 4.2.1
 * Add Brush: Added spline always misses one segment equivalent of a length in Blender 4.2 [[2449b12e672](https://projects.blender.org/blender/blender/commit/2449b12e672f59ce0fff1e691353ad6cf1f6d557)] - Backported to 4.2.1
 * Select in Edit Mode from Material slots with same Material broken [[ca199ba13f7](https://projects.blender.org/blender/blender/commit/ca199ba13f7f400fb42705fe01f2bdb803d33ceb)] - Backported to 4.2.1
 * Triangulate modifier (also Triangulate Faces operator) changed behavior with custom normals [[8b3beeee82f](https://projects.blender.org/blender/blender/commit/8b3beeee82f3ec99045b8ea14e368a7124279ba3)] - Backported to 4.2.1
 * Multiple select snap mode not work in UV editer [[0dad3bdfa7b](https://projects.blender.org/blender/blender/commit/0dad3bdfa7b3f42390dab56dd99ae5f4f98382d2)] - Backported to 4.2.1
 * Easing on Bendy Bones Resets Inconsistently [[d4aecd93e78](https://projects.blender.org/blender/blender/commit/d4aecd93e789012f9cb1374461f2b2c3e8b694f4)] - Backported to 4.2.1
 * Incorrect snap symbol [[e7fa847bba0](https://projects.blender.org/blender/blender/commit/e7fa847bba020d0dc06d50d8e84a9235e2de9396)] - Backported to 4.2.1
 * Depsgraph Missing dependency between Cloth Sim -> Pose with Armature modifier [[d51cab60df6](https://projects.blender.org/blender/blender/commit/d51cab60df6e36972dc14055a3d2f854a7e2d3c4)]
 * Getting console spam from the Triangulate modifier [[1e237da1db0](https://projects.blender.org/blender/blender/commit/1e237da1db0131fdad64c03a741f433c2bcf2886)]
 * Blender crashes when I add an array modifier to a curved circle [[7e75ef20ca9](https://projects.blender.org/blender/blender/commit/7e75ef20ca9217bcdb7229a151365ef69198efd4)]

## Nodes & Physics
 * Geometry Nodes: material lost after first frame in uncached simulation zone [[5cdaa7b0f07](https://projects.blender.org/blender/blender/commit/5cdaa7b0f075c58bcf2e9ecd16018eebf5d97e0f)]
 * Control click socket rename doesn't work for vectors [[59bc3f0959e](https://projects.blender.org/blender/blender/commit/59bc3f0959e96586e720b2e4aeb8e954fbc1c7d0)]
 * Geometry Nodes: Bad alignment of float4x4 in Resample Curve node cause the crash [[af58c223a48](https://projects.blender.org/blender/blender/commit/af58c223a48ffe9818897f7297e51ae1c4712cab)]
 * Crash when Rename Input Node with empty Sting "" [[c38484107f5](https://projects.blender.org/blender/blender/commit/c38484107f53235ed8f21a7174c536ab2d6b2428)]
 * Crash on duplicate particle system [[e65f48e8e29](https://projects.blender.org/blender/blender/commit/e65f48e8e2925626023ffce093f7e03addc5e292)]
 * Crash on switching from Particle mode to Object mode [[443cb9ff9ef](https://projects.blender.org/blender/blender/commit/443cb9ff9efff2fe8d783cd2863635b48288e99e)]
 * An Empty Panel is Shown in The Modifier while not in Group Node. [[7c7b8e1f28f](https://projects.blender.org/blender/blender/commit/7c7b8e1f28fed11a4a9abe17ca28baeb15035ab4)]
 * Geometry Nodes: Crash when switching a modifier that's actively using a viewer node to a tool [[84ffe3720d5](https://projects.blender.org/blender/blender/commit/84ffe3720d5b253e557b2727a8458ef9f808ddb6)]
 * Index of nearest node evaultes GroupID input incorrectly [[116a55e46e4](https://projects.blender.org/blender/blender/commit/116a55e46e4a8f2c08a5119c4ce1cd91578d1877)] - Backported to 4.2.4
 * Crash with this particular geometry nodes setup [[5ad56c52ff3](https://projects.blender.org/blender/blender/commit/5ad56c52ff3473756e687bbd052c5db73a4945c6)]
 * Spreadsheet not updated when converting color attribute from domain corner to vertex [[7777fca066d](https://projects.blender.org/blender/blender/commit/7777fca066dc1679467b0d64f0d5083bf7f0cdc1)]
 * Regression Geometry nodes realize works 10 times slower [[99d73565d61](https://projects.blender.org/blender/blender/commit/99d73565d61d6a449b4331507428085a3a36da8f)]
 * Unhandled OpenVDB exception when using Volume to Mesh node [[5fa8b5b6016](https://projects.blender.org/blender/blender/commit/5fa8b5b6016929930ca37c06577b29dacf578738)]
 * Crashes after changing bl_socket_idname for interface socket [[74f266c1198](https://projects.blender.org/blender/blender/commit/74f266c1198af87842e88fc61923c13d056f0cc5)]
 * Regression Custom nodes icon missing [[6d8d21812b5](https://projects.blender.org/blender/blender/commit/6d8d21812b59db6084ca2fbb1835713141c5d530)] - Backported to 4.2.4
 * Regression Copy operation fails in nodes editor when mouse is over header [[2ca5a65add7](https://projects.blender.org/blender/blender/commit/2ca5a65add765c68ed729c66ff1b2b3360c2c574)] - Backported to 4.2.4
 * Regression Increased instability in fire simulation due to changes of default values [[1a52f2bbabd](https://projects.blender.org/blender/blender/commit/1a52f2bbabd8d0bc7109065c6b7bff891337497d)]
 * Geometry Nodes: Crash while dragging value (Resample Curve node?) [[8182f684bec](https://projects.blender.org/blender/blender/commit/8182f684bec02eca40d941bde4b3dfdbd5ce03a0)]
 * Geometry Nodes Crashes Blender - Access Violation in mesh_sample.cc - Windows 11 [[0358a3d11f8](https://projects.blender.org/blender/blender/commit/0358a3d11f881afd8b38ce20ff7e73d5e5fe67ae)] - Backported to 4.2.2
 * Regression Connecting node lines disappear [[75d5e69d44e](https://projects.blender.org/blender/blender/commit/75d5e69d44ebc539aa84e1e55463f1f45ddab7bb)] - Backported to 4.2.2
 * Regression Geometry nodes - instances and boolean operation bug [[40ae2d7df6e](https://projects.blender.org/blender/blender/commit/40ae2d7df6e2b6d132a58f8402de647535b97bf3)]
 * 3D viewport background color affects attribute text color. [[174387e4d90](https://projects.blender.org/blender/blender/commit/174387e4d905379bbdcb7b6a2513ed249c11e927)] - Backported to 4.2.2
 * Crash with "Extrude Mesh" geometry node on from_pydata edge only mesh [[324e1441a22](https://projects.blender.org/blender/blender/commit/324e1441a22e841d6bd674b27e0efd9bd2aeceee)] - Backported to 4.2.2
 * Regression Simulation/repeat zone overlay is opaque for frame titles [[491df9df6fa](https://projects.blender.org/blender/blender/commit/491df9df6fa840a70f9f435ca8ffb8d8212fef81)]
 * Wrong number of node group users when duplicating a shader [[bb43726ddd8](https://projects.blender.org/blender/blender/commit/bb43726ddd83092b8119f40dce7633d0b14547ed)]
 * Group input socket missing subtype when created from Gamma shader node Gamma socket [[d2c38d3fdc1](https://projects.blender.org/blender/blender/commit/d2c38d3fdc1d5a4f0cc9b3157ab50ab85bf0cf3f)] - Backported to 4.2.2
 * Regression Group ID in Fill Curve Evaluate inconsistantly [[93546768bab](https://projects.blender.org/blender/blender/commit/93546768bab46c81590df8a51d9023b0f7791de8)] - Backported to 4.2.2
 * Geometry Nodes: Crash in Sample UV Surface node [[7dbff95070f](https://projects.blender.org/blender/blender/commit/7dbff95070f3bf8e3168ff6334ccb6c86a2fdd11)] - Backported to 4.2.2
 * Regression Playback of a VDB sequence uses a very large amount of memory [[354a097ce06](https://projects.blender.org/blender/blender/commit/354a097ce0610f6adb8372008b70db0a8bf3e9ed)]
 * Blender crashes after playing animation button for particles system (Boids) [[0d4e2ea40df](https://projects.blender.org/blender/blender/commit/0d4e2ea40dfd049e9a99d7f911e673a890beb720)] - Backported to 4.2.2 & 3.6.18
 * Right click+alt lazy connect shortcut doesn't work with capture attribute. [[d81ba55ce43](https://projects.blender.org/blender/blender/commit/d81ba55ce435affb701f0e511f3b19c4681b81ef)]
 * Blender Crash while loading baked data containing instances that hold empty collections [[05925b404d0](https://projects.blender.org/blender/blender/commit/05925b404d06e0730f00bb6a36c4965b2a4dcb8b)]
 * Frequent crashes when performing minor adjustments with rigid body constraints [[5ee7d028400](https://projects.blender.org/blender/blender/commit/5ee7d028400bcbd630a27e44ae11dd0e96e2311b)]
 * Blender 4.2 RC - Pasting a material onto itself consistently crashes the program. [[c90f13f2b34](https://projects.blender.org/blender/blender/commit/c90f13f2b34bb3c078f6ba0c37295e6777e66cc6)] - Backported to 4.2.1
 * Geometry Nodes: Applying a bake node after a "deform curves on surface" node crashes blender [[6b9b958668b](https://projects.blender.org/blender/blender/commit/6b9b958668b720549922ae1b708182970a87534d)]
 * Geometry Nodes: Blender crash in create string socket in Capture Attribute node [[1d37294d73c](https://projects.blender.org/blender/blender/commit/1d37294d73c346d407eba742589d7f82f5562570)] - Backported to 4.2.1
 * Geometry Nodes: Duplicate warnings accumulate in repeat zone [[0811cbbbc62](https://projects.blender.org/blender/blender/commit/0811cbbbc624be0ec72f375a095e514d6d7e0edc)]
 * Crash Converting FLOAT4X4 attribute from vertex to Face Corner [[37fca25820f](https://projects.blender.org/blender/blender/commit/37fca25820f35308e5e25120df21884498abdf75)] - Backported to 4.2.1
 * Regression Some nodes has wider spacing between sockets. [[71e1305b063](https://projects.blender.org/blender/blender/commit/71e1305b0634a812f4de4e450a32574fc999d496)] - Backported to 4.2.1
 * Geometry Nodes: cache related nodes are not present in quick plug search [[649fa05bcb8](https://projects.blender.org/blender/blender/commit/649fa05bcb8d32a8586e0df04a01ab1f4367e516)] - Backported to 4.2.1
 * Geometry Nodes: Crashing when I input the value node into the length on the resample curve node. [[96cc9109fdb](https://projects.blender.org/blender/blender/commit/96cc9109fdb321307443bcfc8d5dd13a47f38e6d)] - Backported to 4.2.1
 * Geometry Nodes: `Store Named Grid` experimental volume node shows up in link-drag search for `Texture` sockets [[6f7b2291ce6](https://projects.blender.org/blender/blender/commit/6f7b2291ce6968549d4450af3e4f1e4b65be11d5)]
 * Custom panel name can't auto translation in Geometry nodes modifier [[52121edd96f](https://projects.blender.org/blender/blender/commit/52121edd96f2c4aced2e6ecdf3f3594a8131c30c)]
 * In Particle Edit mode, Emitter particles are not visible in the viewport [[e175259f44c](https://projects.blender.org/blender/blender/commit/e175259f44c8aab022ff4d9b1e93fab208eca629)]
 * Crash when entering PS in Capture attribute node [[ddd079b54f0](https://projects.blender.org/blender/blender/commit/ddd079b54f0cdebf49e00f73ca6c773115b96b19)]

## Pipeline, Assets & IO
 * USD Import Fails To Import Normals And Hard Splits Faces [[f0e2d7e5cac](https://projects.blender.org/blender/blender/commit/f0e2d7e5cac9d8e6b50fab1b2729311a4c0618eb)]
 * Blender 4.2 and newer crashes on open with malformed JSON in asset-library-indices [[3a7b6c4f79f](https://projects.blender.org/blender/blender/commit/3a7b6c4f79f1d1ab48d28dabc2c54f2f0eb5f976)]
 * Image datablock replace function doesn't trigger geometry node update as it does in the other editors. [[38ac0037035](https://projects.blender.org/blender/blender/commit/38ac0037035dd85a732ea1ac593c4013d1a7b637)]
 * Assets asset library not loaded properly after creating it with python [[a859ed11301](https://projects.blender.org/blender/blender/commit/a859ed113012ed24763d03de6a8400f06c451106)]
 * Exporting glb file with full backing, and a step different than 1, crashes [[912c85bb9d9](https://projects.blender.org/blender/blender/commit/912c85bb9d9131d34941688c08d84804a0e47c31)] - Backported to 4.2.4
 * Blender spams verbose info on stderr [[1d108b0d77d](https://projects.blender.org/blender/blender/commit/1d108b0d77dfff984770cb82b2d5859d44100d84)] - Backported to 4.2.4
 * GlTF `KHR_materials_variants` extension should not be exported as a required extension [[0b3010d84cc](https://projects.blender.org/blender/blender/commit/0b3010d84cc3e3e02517b79abe973596b94a804e)]
 * GLTF ignores Shapekeys even if only Armature Modifier exists [[080877be6bc](https://projects.blender.org/blender/blender/commit/080877be6bc44e882ce0ddef336b3824cfdf854f)] - Backported to 4.2.4
 * MeshSequenceCache modifier not available in dropdown for Curves (Hair) object [[3b9a07d49ad](https://projects.blender.org/blender/blender/commit/3b9a07d49ade89937cf39e4572d64cec0ead26c9)]
 * USD ALab demo scene crashes when attempting to load just the "Proxy" display purpose [[1676c06386e](https://projects.blender.org/blender/blender/commit/1676c06386e84bebf69a73e9ededd50a00ee87f9)]
 * Gltf export colored vertices: color become inactive and vertices loose color [[98a7aa2c41c](https://projects.blender.org/blender/blender/commit/98a7aa2c41c03ebf7f29915a98c3bf9161a5fc47)]
 * Liboverride objects can be renamed using batch rename [[8c3a3bb9a3d](https://projects.blender.org/blender/blender/commit/8c3a3bb9a3d0ff3c57b2a8e7e81103b6aef56b78)]
 * OBJ material groups are not written when Materials is disabled [[5a1cf200aa4](https://projects.blender.org/blender/blender/commit/5a1cf200aa457da9e90fc008d4c4c6a8031d5481)]
 * Regression STL importing files in with smooth shaded faces [[958433194eb](https://projects.blender.org/blender/blender/commit/958433194eb71e48a91621f1f86a131b04de3e8a)] - Backported to 4.2.3
 * USD preserve bone lengths on skeleton IO [[832d2432498](https://projects.blender.org/blender/blender/commit/832d24324981ce70ee415ba5c72b62f1b3e08fc3)]
 * Blender adds Grease Pencil Objects to Scene when appending IDs with annotations (e.g. in their shader editor) [[3915a84fc6d](https://projects.blender.org/blender/blender/commit/3915a84fc6d46464e3f015612bd531bba8948a21)] - Backported to 4.2.2 & 3.6.16
 * PLY export does not clamp vertex colors when they exceed 1.0 (instead they are wrapped) [[c4a67ec3c72](https://projects.blender.org/blender/blender/commit/c4a67ec3c72b3790b7dbc3824059164c326ad4d3)] - Backported to 4.2.2
 * USDA import crashes Blender during material assignment to geometry subsets [[3a81bde8968](https://projects.blender.org/blender/blender/commit/3a81bde89686ee6fbde4fecfa49fbe4e31a90686)] - Backported to 4.2.2
 * USD drag-drop import crash after undo [[58bd3d40ee2](https://projects.blender.org/blender/blender/commit/58bd3d40ee20d105bbcbe09f8d1ebf3d4075b1dc)] - Backported to 4.2.2
 * Alembic Issues importing Hair in 4.2 but not in 3.6 [[ce82d4434f4](https://projects.blender.org/blender/blender/commit/ce82d4434f4ad9c7f4cb2a008c0c7edc75fafae5)] - Backported to 4.2.2
 * Crash/assert when using "Render Active Object" on an asset with a non-rendering type of object (cameras, lights, etc) active [[36c5606fc3a](https://projects.blender.org/blender/blender/commit/36c5606fc3ac6a98a1515b35949abeaeef335423)]
 * Regression STL importer "Unit Scale" bug. [[7207015a58a](https://projects.blender.org/blender/blender/commit/7207015a58a350187966efd8efa6f7a328cfedef)] - Backported to 4.2.1
 * Blender Crashes When Exporting USD with Allow Unicode Enabled and Using the Character "∧" [[ef2e1f84233](https://projects.blender.org/blender/blender/commit/ef2e1f84233d248b59e07e55efb34af8b461265c)] - Backported to 4.2.1
 * OBJ Importer: Vertex Colors Lose Alignment In At Least 2 Situations [[86e7668b110](https://projects.blender.org/blender/blender/commit/86e7668b11043955bb104e026335bca962dc29f9)]
 * Hydra Storm viewport rendering with the USD export option will crash if the scene contains sub-d meshes [[2f7b66f6343](https://projects.blender.org/blender/blender/commit/2f7b66f634328405555475b8db6ebec7a5c3954d)]
 * Crash switching Alembic file to use Render Engine Procedural [[4035f2fe9c0](https://projects.blender.org/blender/blender/commit/4035f2fe9c0c2e16f1ebb3aa2b320f5b482b9e7c)] - Backported to 4.2.1
 * Alembic importer imports everything twice [[0cf688401b7](https://projects.blender.org/blender/blender/commit/0cf688401b7f0d125c1c55ea896e0569ef208c27)] - Backported to 4.2.1
 * Export All Collections operator returns incorrect info message after running [[c46679c108a](https://projects.blender.org/blender/blender/commit/c46679c108a22792377b07afcb41c71d7106e5e9)] - Backported to 4.2.1
 * Batch export of STL-files incorrect handling of .stl-suffix [[fe1bf4897da](https://projects.blender.org/blender/blender/commit/fe1bf4897dacd9b74f0e380a2258ddab6de1a82d)] - Backported to 4.2.1
 * Crash droping images in view3d ui regions [[604dc2cc33d](https://projects.blender.org/blender/blender/commit/604dc2cc33d00ef4ed8425260d97d1b08f7c8362)] - Backported to 4.2.1
 * Blender new OBJ import is broken (Merge By Distance > Sharp Edges does nothing), legacy one is working [[6f0620161dc](https://projects.blender.org/blender/blender/commit/6f0620161dce0dcfc3286ece6bdf181ae852c2f6)]

## Platforms, Builds & Tests
 * Broken `WITH_UI_TESTS` in Ghost Wayland builds (Wayland/Weston issues?) [[fd63b3d725e](https://projects.blender.org/blender/blender/commit/fd63b3d725e184284fc505ed3ec05df8e4444895)] - Backported to 4.2.4
 * Crash when dragging a file into Blender with Wayland [[20756a07cdc](https://projects.blender.org/blender/blender/commit/20756a07cdcebb08049412902ea6f535cc9cd626)] - Backported to 4.2.4
 * Gltf2 files have a too long filepath and cannot be unzipped (by default) on Windows [[4e5093130cb](https://projects.blender.org/blender/blender/commit/4e5093130cb2596c1b37aa2b11f612127400c3a7)]
 * Missing vcruntime140.dll when running blender-launcher.exe on Windows [[82a42915968](https://projects.blender.org/blender/blender/commit/82a42915968f514faf77edb76c7a2ed656a0fa44)] - Backported to 4.2.2
 * Steam launcher script refuses to launch on drive mount points that contain spaces [[d63db15fe9e](https://projects.blender.org/blender/blender/commit/d63db15fe9e7a5ea184de231b33c418dcd02d650)] - Backported to 4.2.2
 * Ffmpeg for Windows is built with no CPU SIMD optimizations since 4.0 [[246a0ec46a9](https://projects.blender.org/blender/blender/commit/246a0ec46a9ecd97e22e585fd044ab28ef4eae27)]
 * Changes from 'source/blender/blenlib/intern/filereader_zstd.c' led to failure on s390x architecture [[1c69154aaf0](https://projects.blender.org/blender/blender/commit/1c69154aaf033730a9e3882574d56ca6597b84ae)] - Backported to 4.2.1
 * Manpage fails to generate [[d40c2705a17](https://projects.blender.org/blender/blender/commit/d40c2705a17847f65aad21cf85f3b30751ac34a1)] - Backported to 4.2.1
 * Blender on arm64 Linux complains about missing SSE 4.1 [[45447e8b2f8](https://projects.blender.org/blender/blender/commit/45447e8b2f8852629a74b79d52e915ed62e501d0)]

## Python API
 * Calling addon_utils.enable(..) fails on extensions with wheels [[cd07e729a21](https://projects.blender.org/blender/blender/commit/cd07e729a2161de769c76f2ad230f2d6fd7e6071)] - Backported to 4.2.4
 * Cannot build an extension with non-empty paths [[67865cb73ac](https://projects.blender.org/blender/blender/commit/67865cb73accf7838b802348eb544872be8bc4cb)] - Backported to 4.2.4
 * Cannot build an extension with non-empty paths [[ae5f5c5bcac](https://projects.blender.org/blender/blender/commit/ae5f5c5bcac3e2c2c957ce30b3396d1fcfbe0545)] - Backported to 4.2.4
 * Python Module can't find Draco library [[b25da97fefd](https://projects.blender.org/blender/blender/commit/b25da97fefdf8854ffda3884dce16a2c92f9b69e)]
 * Disabling / uninstalling extensions doesn't always remove the extension's dependencies [[e3e6dd8fdb1](https://projects.blender.org/blender/blender/commit/e3e6dd8fdb1f6da2e2ce54fde20229c55502ffab)] - Backported to 4.2.4
 * Blender unstable/crashing if command line "--python" script contains "import multiprocessing" [[9a252c2e73b](https://projects.blender.org/blender/blender/commit/9a252c2e73b9303d4c2c493c7a80a75a4b1629bc)] - Backported to 4.2.4
 * PyAPI docs: `use_draw_scale` wrong default value [[26d34d3bfc1](https://projects.blender.org/blender/blender/commit/26d34d3bfc15cf6d132c471d06fd513ea23d5851)]
 * Blender can deadlock if `Operator.poll_message_set` was called with a string [[1d286a1b93d](https://projects.blender.org/blender/blender/commit/1d286a1b93d34c758565d3c292ec9e4aee05f89f)] - Backported to 4.2.4
 * Bpy.props.EnumProperty dynamic enum items tooltip breaks though Python is keeping a reference to the last items [[c836c0a20e5](https://projects.blender.org/blender/blender/commit/c836c0a20e5a6870cd7a6107a75e56c097846ce3)]
 * Python ParticleSettings.material_slot valid only if object active, otherwise WARN is printed. [[140c0f1db9f](https://projects.blender.org/blender/blender/commit/140c0f1db9f25cfcad1cf8e7b42090555feaa017)]
 * Skipping wheel for other system (macosx_10_9_universal2 != macosx_11.2_arm64) [[87f227ef7a5](https://projects.blender.org/blender/blender/commit/87f227ef7a590672521fba77c0697edb0e2638d4)]
 * Extensions build: no error handling when "type" is missing [[b0fb6a1b2c5](https://projects.blender.org/blender/blender/commit/b0fb6a1b2c56588589b3b69ba6d0dfa3c2e5ba5c)] - Backported to 4.2.4
 * WIN32 Extension update failed and removed the addon instead [[1563ba7e6cd](https://projects.blender.org/blender/blender/commit/1563ba7e6cd838ab5495e60623326f7e7aee4ac3)] - Backported to 4.2.3
 * Crash on load after id_properties_ui type change [[57eae31ce9c](https://projects.blender.org/blender/blender/commit/57eae31ce9ca7b8b18cbceefa6020cec9939b257)] - Backported to 4.2.3
 * Inconsistent GPU module doc (active_framebuffer_get v framebuffer_active_get) [[0311d8082b7](https://projects.blender.org/blender/blender/commit/0311d8082b72932ba18d20e0143a165a67c921f9)] - Backported to 4.2.2
 * Extensions - fail to install extension from cli [[6822e61c6f3](https://projects.blender.org/blender/blender/commit/6822e61c6f386ba94b55bbdecdd84f89bbbc6d61)] - Backported to 4.2.2
 * Console Keyboard Interrupt does not work on Blender 4.2.1 [[54153629f4c](https://projects.blender.org/blender/blender/commit/54153629f4c7c9017588f30946f2b806dd448a66)] - Backported to 4.2.2
 * `bpy.types.Library`==ID if access through subclasses. Bug? [[43f7b50df31](https://projects.blender.org/blender/blender/commit/43f7b50df3168008a82b7c7a0afaaa71adc3419d)]
 * Regression Crash if `cls.bl_rna` after unregister_class(). [[1235404cfe9](https://projects.blender.org/blender/blender/commit/1235404cfe9cd4c7397ae80ac9614dfd31bcb6b0)] - Backported to 4.2.2
 * `copy_from_face_interp` needs tuple input, loses reference, returns nothing [[e9333c82aba](https://projects.blender.org/blender/blender/commit/e9333c82aba32e8f49bc893459cb3e69b9fb4ba6)] - Backported to 4.2.2
 * Scrollbars of UI lists in custom nodes are synced [[d7a6ab6f04e](https://projects.blender.org/blender/blender/commit/d7a6ab6f04ebf2c4c98f670cc3475549007370b7)]
 * UI Button Placeholder not behaving as expected [[da57f2f3d52](https://projects.blender.org/blender/blender/commit/da57f2f3d52e03fa7606d825bfc75563af6338a2)]
 * Disabling / uninstalling extensions doesn't always remove the extension's dependencies [[58c7237bbfd](https://projects.blender.org/blender/blender/commit/58c7237bbfd3c47ea745fee8ed57556790620261)] - Backported to 4.2.1
 * Extensions Errors on the console about missing directories [[f8e06a5f5fa](https://projects.blender.org/blender/blender/commit/f8e06a5f5fa07961091158c7da24fbd9c7eac519)] - Backported to 4.2.1
 * RNA paths from python tooltip/operator for viewport settings are wrong [[f53b6b89f84](https://projects.blender.org/blender/blender/commit/f53b6b89f84d6c3a7e9799832e93f479b94e45b8)]
 * Removing Addon with compiled modules(e.g. cython) on Windows will cause PermissionError: [WinError 5] Access is denied [[fc6a99e0d27](https://projects.blender.org/blender/blender/commit/fc6a99e0d27171f4e1c1a0fddb7e2a3ab722315c)] - Backported to 4.2.1
 * Extension cannot be uninstalled if symlinked [[805f66c5c48](https://projects.blender.org/blender/blender/commit/805f66c5c4821ab045067a93043be571ea5fe0d5)] - Backported to 4.2.1
 * Failure to connect to online repository on WIN32: [ASN1] nested asn1 error (_ssl.c:4035) [[3a88af54029](https://projects.blender.org/blender/blender/commit/3a88af540291f81aa2da2f9e30101fa04b26901d)] - Backported to 4.2.1
 * Regression ACCESS VIOLATION for operators using class properties [[7fcd4e2429b](https://projects.blender.org/blender/blender/commit/7fcd4e2429b829b4ea85f3f763face4163236d8e)] - Backported to 4.2.1
 * Core extensions report bugs to add-on repo [[13f27ea0ae9](https://projects.blender.org/blender/blender/commit/13f27ea0ae9e153e0470b1b4042a1ba2484aa035)] - Backported to 4.2.1
 * Extensions Unable to parse manifest if version format "X.X" instead of "X.X.X" [[f3a0ecb4e25](https://projects.blender.org/blender/blender/commit/f3a0ecb4e25675113d90426dd10373b771c84bf4)] - Backported to 4.2.1
 * Legacy Addons don't install in 4.2 if they're missing a LF or comment at the top [[68c2c9cb26a](https://projects.blender.org/blender/blender/commit/68c2c9cb26ad0bbb516f17e9d477ae645279d671)] - Backported to 4.2.1
 * Extensions double-warning upon launching Blender [[9a568f22279](https://projects.blender.org/blender/blender/commit/9a568f22279d149178b24a6825140664e1710277)] - Backported to 4.2.1
 * Blender 4.2: Can't install add-ons if a specific order of operations is followed on first open [[c7a26cf5c27](https://projects.blender.org/blender/blender/commit/c7a26cf5c27e8d54b5fa23c6ce14f296ffccdafc)] - Backported to 4.2.1
 * Dependency graph: Simple exporter - issue running code snippet [[a04d0b932f3](https://projects.blender.org/blender/blender/commit/a04d0b932f36df690cc0cf28c8d214ad1a0cd896)]
 * Typos in quickstart [[1395a958d79](https://projects.blender.org/blender/blender/commit/1395a958d79c7cfba42a52ddceda80f50c307bfb)]

## Render & Cycles
 * Viewport Shading, Render mode, render crashes [[2f786d998de](https://projects.blender.org/blender/blender/commit/2f786d998def9b1db5ab78a58c0924a2a8440d13)] - Backported to 4.2.4 & 3.6.18
 * Changing a curve's material, using 3D curves, on OptiX, in viewport rendering, results in error [[5804a1cc2ce](https://projects.blender.org/blender/blender/commit/5804a1cc2ce59c5e47949ada8d42d177aa3a3221)] - Backported to 4.2.4
 * Dual GPU - Bake to Color Attribute Crashes Blender [[e5a4beb518b](https://projects.blender.org/blender/blender/commit/e5a4beb518b65296321d8994ef1b36841fc787ea)]
 * Cycles Render Fails with error message: OPTIX_ERROR_INVALID_VALUE in optixPipelineSetStackSize when using OSL with wavelength node [[3a36d638a50](https://projects.blender.org/blender/blender/commit/3a36d638a5099d906968673fe96a971e7c1da2a6)]
 * Cycles Light Tree: Artifact on the principled volume shader when sun angle is "0" [[219e6551196](https://projects.blender.org/blender/blender/commit/219e655119673492318e2c1b915f5d2b79e7d67a)] - Backported to 4.2.4
 * Vector Pass output in Cycles is wrong when camera focal length is animated [[dfc58d282b6](https://projects.blender.org/blender/blender/commit/dfc58d282b62819e8b60e26bd1ff80102bf172ad)]
 * Opening Blender 3 document in Blender 4 breaks Alpha transparency in Cycles [[88289e8baca](https://projects.blender.org/blender/blender/commit/88289e8baca67d00fe57a746890b6e65113a0216)] - Backported to 4.2.4
 * Crash to desktop when baking texture from selected mesh with no faces to active [[5f8721d92ac](https://projects.blender.org/blender/blender/commit/5f8721d92ac7a8f398067979d64e2f6ffbf04910)] - Backported to 4.2.3
 * 4.1 splash screen won't render in Blender 4.2.1 with MetalRT [[b96a7b72045](https://projects.blender.org/blender/blender/commit/b96a7b72045b9b41f41b33f76f23d2936d945844)] - Backported to 4.2.3
 * UDIMs + Generated Test Grid + Cycles (only!) = FF00FF [[6385296e568](https://projects.blender.org/blender/blender/commit/6385296e5684a826953d9a320a90336789b6c1ed)]
 * OneAPI Embree GPU, transparent shadows will render opaque if transparent bounces is modulo 256 [[d300098ee5b](https://projects.blender.org/blender/blender/commit/d300098ee5b85e1dbd2cd26d6a90c58850b706be)] - Backported to 4.2.2
 * Blender 4.2 CUDA Device Type, OPTIX denoising, kernel reloads message printed multiple times per frame [[2d611f7b451](https://projects.blender.org/blender/blender/commit/2d611f7b4519f5a6558d778f19f2305e548babbe)]
 * HIP-RT Memory leak in Cycles viewport [[4bfee1936c8](https://projects.blender.org/blender/blender/commit/4bfee1936c8ab9b83d07b848051f5151c31f49ce)]
 * 4.2 oneAPI "Embree on GPU" (HWRT) no hair strands in viewport [[94c9898f417](https://projects.blender.org/blender/blender/commit/94c9898f417f43f86349c35ed8d36e38d8ae7fe8)] - Backported to 4.2.2
 * Atan2(0,0) evaluates to NaN on Metal but 0 on all other platforms [[77035192c98](https://projects.blender.org/blender/blender/commit/77035192c98c709f98eacc915ad48189200309d3)] - Backported to 4.2.2 & 3.6.16
 * Cycles light tree misses some light samples [[84bab7f300b](https://projects.blender.org/blender/blender/commit/84bab7f300b6f4e4f6d02540c1119a146b83c033)]
 * Too dark objects inside a volume object with shadows disabled (cycles) [[be0d2e19b54](https://projects.blender.org/blender/blender/commit/be0d2e19b54f122571847eae2f3fbc3324ea9dfd)]
 * Certain OSL scripts have artifacts in the world shader on Windows [[c5008d2de9f](https://projects.blender.org/blender/blender/commit/c5008d2de9f4c8988420b381f0af6957b4a2e4b8)]
 * Cycles viewport BVH2 misses some curve interections [[58aa349f689](https://projects.blender.org/blender/blender/commit/58aa349f689c1bf15118603e51a2c7b49869d88a)]
 * Frames are layered on top of each other separated by a scanline when exporting video with PNG video codec [[df00c30fcd1](https://projects.blender.org/blender/blender/commit/df00c30fcd1f841ad86b9bd13a86f53b9bb2ead3)] - Backported to 4.2.1
 * Cycles 3D Curves missing some intersections using Metal [[a3b0c68ca92](https://projects.blender.org/blender/blender/commit/a3b0c68ca92c99ee3b6a2eb293f30b82125d6a72)]
 * X86 Blender on Apple Silicon doesn't show Cycles Metal GPU render. [[ce6454d02ff](https://projects.blender.org/blender/blender/commit/ce6454d02ff856655fd29d4f47632c59096e945b)] - Backported to 4.2.1
 * Crash baking textures with Cycles Metal [[7e9776a1672](https://projects.blender.org/blender/blender/commit/7e9776a16720af0186ae05432fc3f03a6127735a)]
 * Point Density node works on surfaces in OSL [[90bcc3b5dc4](https://projects.blender.org/blender/blender/commit/90bcc3b5dc4c576752adb587c4424e4948927b6b)]
 * Regression Precision artifacts on refractive materials with Cycles MetalRT [[6b522c5e306](https://projects.blender.org/blender/blender/commit/6b522c5e3067e512bb00ffa7c12b1b523e2c3204)] - Backported to 4.2.1
 * Principled shader clearcoat roughness inconsistent change from 0.0265 to 0.0266 (Cycles only) [[df4df3cd52a](https://projects.blender.org/blender/blender/commit/df4df3cd52ab7ef5733d7a76d06829645cf73108)]
 * Cycles Not handling phase functions of overlapping volumes properly [[7e40d567d47](https://projects.blender.org/blender/blender/commit/7e40d567d4738a09d445b6f477aeff5282fce716)]
 * Cycles NaN pixels in Glossy and Transmissive Passes with roughness values near 0.0266 [[5b61a01c19c](https://projects.blender.org/blender/blender/commit/5b61a01c19cbc0c0a96f2bd1368a9ab920990f4a)] - Backported to 4.2.1
 * OSL generates UV coordinates for objects that don't have UV maps [[e7ce8d33e30](https://projects.blender.org/blender/blender/commit/e7ce8d33e30203c61c4cf296f6278af17158f052)]
 * Incorrect sampling with 180° Spot and Light tree activated [[90e83175ebb](https://projects.blender.org/blender/blender/commit/90e83175ebbc317b40cad8ff2405fe4037152a4a)] - Backported to 4.2.1
 * No transfer of the "Cast Shadow" attribute for light [[7438bf8e0fb](https://projects.blender.org/blender/blender/commit/7438bf8e0fb6a15ac3594695da5be799d2e0ca1d)] - Backported to 4.2.1
 * Voronoi Texture Smoothness Socket Input also is used as Lacunarity in Smooth F1 [[138e9143461](https://projects.blender.org/blender/blender/commit/138e914346166e4a3c1d0c3e6007dbd6e6fe2d81)] - Backported to 4.2.1
 * OptiX OSL uses incorrect SSS render method [[cf96136ec6c](https://projects.blender.org/blender/blender/commit/cf96136ec6cf9ec9753a2ea568d3f233b5152e4d)] - Backported to 4.2.1

## Sculpt, Paint & Texture
 * Sculpt Regression: Object mode doesn't reflect mesh after undos in sculpt mode [[d3644412201](https://projects.blender.org/blender/blender/commit/d364441220119abc184df60cd4086dec0d06dd4f)]
 * Regression Fill mask in sculpt mode on partially hidden mesh causing instant crash [[b1bcdf8d53e](https://projects.blender.org/blender/blender/commit/b1bcdf8d53ef49ca0e3e6087c15b8ad88ad8883d)]
 * Calculate brush spacing relative to the scene makes blender stop working using drawing tablet with size pressure. [[bccae832ae3](https://projects.blender.org/blender/blender/commit/bccae832ae3fd7c36331868320cbc9bcd796964a)]
 * Blender 4.2.3 Sculpt Mode | "Error : EXCEPTION_ACCESS_VIOLATION" | Voxel Remeshing Causes Crash Randomly But Often [[426ed8c61ed](https://projects.blender.org/blender/blender/commit/426ed8c61edc351e94342d5c148f4135ac056f0a)] - Backported to 4.2.4
 * Blender crashes every time i try to use "Smooth" set on negative direction with stylus [[304800db737](https://projects.blender.org/blender/blender/commit/304800db73706e7cfa753fd07e73f6550b14c57d)]
 * Undo either stops working or causes ghastly bugs when it goes in/out of Sculpt Mode [[c98da9d971c](https://projects.blender.org/blender/blender/commit/c98da9d971c391d035fccaefb64adbef52e672a8)]
 * Face Set From Masked Not Working in Blender 4.2.2 [[ffa4f6eaba9](https://projects.blender.org/blender/blender/commit/ffa4f6eaba94b02f65cae5d8644ef31e47547227)] - Backported to 4.2.4
 * Crash to desktop when in sculpt mode user moves an object [[bcf6524ca1e](https://projects.blender.org/blender/blender/commit/bcf6524ca1eef214c36775a9599673d3333f6d32)] - Backported to 3.6.18
 * Crash when using "Set Vertex Colors" without a brush. [[8d11882d079](https://projects.blender.org/blender/blender/commit/8d11882d079e50b69a579361a3bb7ffa847d9265)]
 * Sculpt Mode: Confirming gesture tool creates additional gesture point [[fbc8ef56ef9](https://projects.blender.org/blender/blender/commit/fbc8ef56ef9cf57014e5d76416f4b78d2a3ce19c)] - Backported to 4.2.4
 * Missing brush visualization when using Shift to smooth [[6f781481c1d](https://projects.blender.org/blender/blender/commit/6f781481c1d510a4614528961e0b0a4ecd7c54e6)] - Backported to 4.2.4
 * Sculpt Editing voxel size unpredictable with large/small meshes (disapears, cannot be edited in a meaningful way) [[4221f827cf1](https://projects.blender.org/blender/blender/commit/4221f827cf1b7c25c0cb2b47cca754ec11540fa9)]
 * Multires Unsubdivide freezes Blender with hidden geoemtry [[ef174d1e81a](https://projects.blender.org/blender/blender/commit/ef174d1e81a0917a1d49d8ae96a92ff841d19fb1)]
 * Regression Box Hide occasionally causes artifacts [[77be435eee7](https://projects.blender.org/blender/blender/commit/77be435eee7ced157971049b01cff3a7380fea18)]
 * Edit Face Set on default mesh changes mesh color [[ed274e7a76c](https://projects.blender.org/blender/blender/commit/ed274e7a76ce715556077feecb48cc54295f21d4)] - Backported to 4.2.1
 * Blender crashes when undoing during sculpting on duplicate object [[8a812e334d0](https://projects.blender.org/blender/blender/commit/8a812e334d07fa8be186bc66f5a14a9ba86e702d)] - Backported to 4.2.1
 * When using "bpy.ops.paint.image_paint" to draw textures, modifying the Mask Mapping of the Texture Mask is invalid. [[09cb2be1590](https://projects.blender.org/blender/blender/commit/09cb2be1590d103f4a3d825a61b57cf84c4c4fec)]
 * Box mask crashes Blender in sculpt mode [[338140f1e68](https://projects.blender.org/blender/blender/commit/338140f1e68612ba6756a016296ddc93ffc973dd)] - Backported to 4.2.1
 * Color sampling clamped to 1 for specific tool [[584834e5a85](https://projects.blender.org/blender/blender/commit/584834e5a85be840be3551571bbfed05f7c5cad3)]
 * Clay strips brush leaves cracks on mesh [[e1ca54c52a8](https://projects.blender.org/blender/blender/commit/e1ca54c52a8b1a9e34afb870c4825e83164a8d9b)]
 * Regression Sculpt mode Annotation tool cursor conflict [[03d3f2fa738](https://projects.blender.org/blender/blender/commit/03d3f2fa738ab0980a7ec106653b2fa20af16ece)]

## User Interface
 * Missing brush thumbnails in sculpt asset shelf in large asset library [[671f08df966](https://projects.blender.org/blender/blender/commit/671f08df966dc080d513527176bec6f4997d61e9)]
 * Missing brush thumbnails in sculpt asset shelf in large asset library [[0f8e4b35165](https://projects.blender.org/blender/blender/commit/0f8e4b351654c33064e8fd1a790afb1bf757e8eb)]
 * Resizing areas can be cancelled by pressing esc, but not right-click [[68ddaa2f0a0](https://projects.blender.org/blender/blender/commit/68ddaa2f0a007ffe488bd8c58107163ba8ad5dbc)]
 * Assert when saving a file with deleted workspaces [[320c77b9635](https://projects.blender.org/blender/blender/commit/320c77b9635fbb2ced803759dd34b4f224d90e17)]
 * Python template_list cursor is offset by 1 character [[551e87c4fd2](https://projects.blender.org/blender/blender/commit/551e87c4fd2f6a62f8e7a69378b207fef820003b)] - Backported to 4.2.4
 * Blender crashes when using context.temp_override with bpy.ops.object.mode_set in some cases [[007fd95f178](https://projects.blender.org/blender/blender/commit/007fd95f1780f657bf84b66c30dbbf56c1ae2988)] - Backported to 4.2.4
 * Volume shadows tooltip incorrect [[d92ee66889d](https://projects.blender.org/blender/blender/commit/d92ee66889dc43b538ac1f0a5e92cb3fa071a3a1)]
 * Grless key not supported on WAYLAND UI [[cf3a6697d2e](https://projects.blender.org/blender/blender/commit/cf3a6697d2eb5ce19556cce1a1cd306a063e2109)]
 * Regression outliner delay refreshing makes search results fail to display [[7ae75928996](https://projects.blender.org/blender/blender/commit/7ae759289960748ee91cc5257d3ab53016841534)]
 * Floating text that appears when dragging outliner elements is blurrier than other interface text [[00dca5acec9](https://projects.blender.org/blender/blender/commit/00dca5acec90ae4946a0b4daaa2450c4481c71c3)]
 * Changing a shorcut property through python does not save when preferences are saved [[772bb795d26](https://projects.blender.org/blender/blender/commit/772bb795d267dc6d0d66cc95a8b54128c86c9bb3)]
 * Regression file selector automatically creates paths & warns [[ea2d27cf84d](https://projects.blender.org/blender/blender/commit/ea2d27cf84d4f6ca0b61f174cac9cfc00656a50f)] - Backported to 4.2.3
 * Tooltips unnecessary large padding [[826ba8e27a3](https://projects.blender.org/blender/blender/commit/826ba8e27a33534d2818d21bc88da226676dd444)] - Backported to 4.2.3
 * Ctrl-clicking in Outliner to toggle selection has an unresponsive delay/speed limit [[e9262f48aad](https://projects.blender.org/blender/blender/commit/e9262f48aad2c3fc13c03fefb949f5078518c8a7)]
 * Recent files list and off-line storage behaviour [[5bb7f06409d](https://projects.blender.org/blender/blender/commit/5bb7f06409d67709a584ecc6cf87de27d7f6ddf3)]
 * Status bar doesn't show modifier keys for Bisect operator unless one of them is pressed [[120d2b867b5](https://projects.blender.org/blender/blender/commit/120d2b867b597e9b9ccea53af9d23bf410a0a12f)]
 * Uncheck "Load UI" causes failure in direct manipulation of nodes with certain files [[22397ad819a](https://projects.blender.org/blender/blender/commit/22397ad819a207f044e9144aab9529b667cdc160)] - Backported to 4.2.3
 * UI Few operators appears in context menus where it doesn't make sense [[ccb18659253](https://projects.blender.org/blender/blender/commit/ccb18659253774f873d4aefde7d6ef2fc5018bc4)]
 * View Transform of default startup file is AGX, but new scenes are created with Filmic [[e98e6eda5db](https://projects.blender.org/blender/blender/commit/e98e6eda5dbf395eb742e3f3d52ece8280cf78b9)]
 * 'Disable in Viewports/Renders' toggles on linked objects/collections can be set by shift-clicking parent toggles [[e494a440240](https://projects.blender.org/blender/blender/commit/e494a4402403b0e3ab443eff4bebd1ba049815f6)]
 * Regression Toggle Maximize Area slower with geometry nodes [[60013228e1d](https://projects.blender.org/blender/blender/commit/60013228e1de0444756767081e34567fcca2646f)] - Backported to 4.2.3
 * `angle_limit` in Bevel Modifier does not show enough precision [[800d543fa57](https://projects.blender.org/blender/blender/commit/800d543fa57b6ee97fe7a22ed4043ca7f5343064)]
 * Materials can be unlinked from liboverride objects through outliner (and can't be undone) [[ba6126dbbd8](https://projects.blender.org/blender/blender/commit/ba6126dbbd887c2aceac46b56632b62d778a0363)]
 * Changing enum props in redo menu doesn't update the menu drawing [[622affe853c](https://projects.blender.org/blender/blender/commit/622affe853c4146c8057d0d3dea16325712d5b06)] - Backported to 4.2.3
 * Instanced Panel crashes Blender [[42b390fe3d3](https://projects.blender.org/blender/blender/commit/42b390fe3d366635881729461941aff173ccfaf5)]
 * "Auto-Save Preferences" does not work in all preference menus [[2ba69519a8b](https://projects.blender.org/blender/blender/commit/2ba69519a8b5b607b73fe759efef322f15476a4b)] - Backported to 4.2.3
 * Layout panel missaligned in `UI_DIR_UP` popups [[b721374e9b1](https://projects.blender.org/blender/blender/commit/b721374e9b1abc72754874dae5ded1f1d1c0aafa)]
 * Can't change viewport text overlay color [[c41bb200087](https://projects.blender.org/blender/blender/commit/c41bb20008747eccebca1113bb5967e4889e797c)] - Backported to 4.2.2
 * Crash on duplicating child collection of liboverride hierarchy [[a9208e9f8e1](https://projects.blender.org/blender/blender/commit/a9208e9f8e1980b6284af914ca372068526b12e2)]
 * Crash after unlink -> undo -> select (linked copied scene) [[2954323c2d6](https://projects.blender.org/blender/blender/commit/2954323c2d6664db114a208ecb1d32d29b061813)]
 * Crash after unlink -> undo -> select (linked copied scene) [[f5b16e474fd](https://projects.blender.org/blender/blender/commit/f5b16e474fd221357e8b97898672322987df048c)] - Backported to 4.2.2
 * 4.2 Regression: UI popover can not be scrolled upwards [[7bb8ab81946](https://projects.blender.org/blender/blender/commit/7bb8ab81946ef033f9315db32e6c6f5138abdb64)] - Backported to 4.2.2
 * Theme File Output node not using the same theme color as the other "Output" nodes [[05f2e6f5c22](https://projects.blender.org/blender/blender/commit/05f2e6f5c2283175f9b27eb8f8fbadd6804462b2)]
 * Animation doesn't play/start when cursor is over side panel of NLA editor [[743e24a86b4](https://projects.blender.org/blender/blender/commit/743e24a86b489408385a555ad8b16d5e8eabcdf6)] - Backported to 4.2.4 & 3.6.18
 * Outliner Delete skips selected child collections [[28ed2251003](https://projects.blender.org/blender/blender/commit/28ed22510030614c882941aeaf469cc2df33f114)]
 * Segmentation Fault Upon Blender Startup [[d9cfb9fea25](https://projects.blender.org/blender/blender/commit/d9cfb9fea25af226d2b545f3a5325289dbd65388)] - Backported to 4.2.2
 * Area light resize gizmo (amongst other gizmos) missing undo step? [[46cd88f229c](https://projects.blender.org/blender/blender/commit/46cd88f229c90d87d7528ad3e0169ec5229c6dfb)] - Backported to 4.2.2
 * Camera placement is undone when changing Pose Asset properties from redo panel [[ce95155b308](https://projects.blender.org/blender/blender/commit/ce95155b308da327c7f48f01ec7e98395ebabbd1)]
 * Gizmo flickering when a Blender window is resized [[83fe5712ab6](https://projects.blender.org/blender/blender/commit/83fe5712ab6170d0177616f29708120f6cdff037)]
 * Outliner lacks a refresh when making a node group single-use [[9b1d653554f](https://projects.blender.org/blender/blender/commit/9b1d653554f975ea007ffd4073ad53e971413873)]
 * Wrong cursor icon after saving with multiple windows open [[bf529e248a2](https://projects.blender.org/blender/blender/commit/bf529e248a229ab99bbac75d92034fa747d3e005)] - Backported to 4.2.2
 * Hex values in color popup are written with 1 or 2 digits mixed, instead of always using 2 digits [[d6ea01bd384](https://projects.blender.org/blender/blender/commit/d6ea01bd384cdc9dd49b63ca2ade4a34e663a157)] - Backported to 4.2.2
 * Wayland tiny mouse cursor with 125% fractional scaling [[92f633e7cbd](https://projects.blender.org/blender/blender/commit/92f633e7cbd67cdd202139849e2666b0012a618c)] - Backported to 4.2.2
 * Lack of "Unsupported GPU" GUI message when Blender crashes on startup on linux. [[04c5a9f053c](https://projects.blender.org/blender/blender/commit/04c5a9f053c7c5261d2f3ca3086ae4535d6234bb)]
 * Insert text unicode not working in 4.2 [[5a9fe638ded](https://projects.blender.org/blender/blender/commit/5a9fe638dedb179050c4929ea8fcdec80d221af2)] - Backported to 4.2.1
 * Transfer Mode (Alt+Q) doesn't work for pose mode switching to a linked armature [[9e0b6734679](https://projects.blender.org/blender/blender/commit/9e0b6734679ca35bf1c35d11ef57cc8db908a954)]
 * Crash when viewing properties space data in Data API [[8e43a9a04ee](https://projects.blender.org/blender/blender/commit/8e43a9a04ee56953daae1387a7ef38617631b2d6)]
 * Asset Browser: Asset type icon misaligned [[b64cf71c1d2](https://projects.blender.org/blender/blender/commit/b64cf71c1d23bb971b992acdea408b18b64ff884)]
 * "Pin to Last" Fails to Maintain Modifier Order [[a26819ad88c](https://projects.blender.org/blender/blender/commit/a26819ad88c60d7508b16b690d931b5efd53e6ae)] - Backported to 4.2.1
 * Regression Tooltip missing from open URL preset operator [[0baeca8f9e3](https://projects.blender.org/blender/blender/commit/0baeca8f9e34b36320d3163c3f83fb587f4e186f)] - Backported to 4.2.1
 * Extensions becomes empty when filtering addons in the workspace [[8da807684ca](https://projects.blender.org/blender/blender/commit/8da807684ca49ecd03a69755794d63aa73ab7cde)] - Backported to 4.2.1
 * Regression UV editor doesn't import images by drag and drop anymore [[6e152df547c](https://projects.blender.org/blender/blender/commit/6e152df547cd1f323ffce729fe17572dc16b773b)] - Backported to 4.2.1
 * Crash when switching an area to asset browser and back and creating a new file [[a5970b8248b](https://projects.blender.org/blender/blender/commit/a5970b8248b0d7d10a356687127a1126a6f12f2f)] - Backported to 4.2.2
 * Purge Unused crashes if mouse not moved away from popup before confirm delete with enter key [[d5d64f4a9f7](https://projects.blender.org/blender/blender/commit/d5d64f4a9f7d80223417905bd243c28c425e545d)] - Backported to 4.2.1
 * Crash when using certain tools in viewport on Wayland [[cb4aa93480f](https://projects.blender.org/blender/blender/commit/cb4aa93480f7c88b3923809ce3fbee8f55a2c363)] - Backported to 4.2.1
 * File Browser: Back button doesn't go back to first folder (in the new Video Editor > Video Editor workspace) [[0b70a9edc56](https://projects.blender.org/blender/blender/commit/0b70a9edc56b9d501ab34a5e8f8193e89b55125d)]
 * File Browser: Back button doesn't go back to first folder (in the new Video Editor > Video Editor workspace) [[a63a15527db](https://projects.blender.org/blender/blender/commit/a63a15527db9e58f43d3b39f48c77deeb84d8b40)]
 * Linux Wayland: Repeatedly opening and closing a Blender window freezes Blender [[80f0c613c07](https://projects.blender.org/blender/blender/commit/80f0c613c0797f2cd8dd7d7663de2da93f50a3a0)] - Backported to 4.2.1
 * DOF eyedropper button not showing in Cycles and inconsistent text labels [[5f54c463c0e](https://projects.blender.org/blender/blender/commit/5f54c463c0e57982412dd2aa8929f3cc4853606e)] - Backported to 4.2.1
 * UI is not refreshed after copying modifier using Outliner [[7ee2938bc19](https://projects.blender.org/blender/blender/commit/7ee2938bc194b4c37612373a7883d3a745b06167)]
 * Asset dragging don`t work with custom asset shelf [[d6e9438e608](https://projects.blender.org/blender/blender/commit/d6e9438e608406d578751cebc065fff215b91ce9)]
 * Preferences Text Rendering [[19922968281](https://projects.blender.org/blender/blender/commit/19922968281ae9d7c6066c74a9439a9c87c08ab5)]
 * Confirm On Release in Radial Control doesn't work with Right Mouse Button [[439c3180982](https://projects.blender.org/blender/blender/commit/439c318098289e6983f58af09d4134a11a730783)]
 * Can't use Swedish keybinds on Linux Mint (Unsupported keys: Unknown) [[ef1d5c629b4](https://projects.blender.org/blender/blender/commit/ef1d5c629b42fad737f17e5c9eaff43dcfaf9234)]

## VFX & Video
 * Video Sequencer - No Audio Output from Scene Strips [[ebe209b8ecf](https://projects.blender.org/blender/blender/commit/ebe209b8ecf11128a562ff595fa09425cf27eadc)]
 * Crash Compositing with Double Edge Mask [[a0cfb0b1429](https://projects.blender.org/blender/blender/commit/a0cfb0b14293f9b91de541ff9df27fc9537352ec)] - Backported to 4.2.4
 * VSE Frame Number retiming not working when applied to separate 'Scene' [[6ee1eb49bee](https://projects.blender.org/blender/blender/commit/6ee1eb49beee640c7918fa358f2b851ca6bb331e)]
 * VSE Doing "copy to selected" with a Scene strip's input property (Camera/Sequencer) will crash Blender. [[8c3b7345605](https://projects.blender.org/blender/blender/commit/8c3b7345605de3b214a98734f0963329cf0e207d)]
 * Shader Points of curve float node are remapped incorrectly when set with custom max x,y range [[a1b489ec711](https://projects.blender.org/blender/blender/commit/a1b489ec71155e56ea73bbb8d447db7db4acb67a)]
 * VSE Retiming key interaction issues [[9ef61368652](https://projects.blender.org/blender/blender/commit/9ef61368652ddbbaad5e9eba39b2c61a90619ec8)]
 * VSE retiming doesn't work good during speed-up transition [[ba59908dc8a](https://projects.blender.org/blender/blender/commit/ba59908dc8aa121f6029b7befc9f4402a8b41285)]
 * Do not display keys on zoomed-out strips [[d50d1e757b4](https://projects.blender.org/blender/blender/commit/d50d1e757b4ca71ed07f73f0bf120dff170988f1)]
 * Cryptomattes From Compositor Break In GPU Compositor Mode [[cce896fe11f](https://projects.blender.org/blender/blender/commit/cce896fe11f858c70d3511c17b2dd25dfca8bece)]
 * Selecting Cryptomate while rendering causes crash [[bce20b935c5](https://projects.blender.org/blender/blender/commit/bce20b935c5e7a7bbee30d04873962b1a0a56748)] - Backported to 4.2.4
 * Sequencer Editor: Strips Handlers Do Not Return Correctly When Moving Cursor Left and Cancelling Move Modal Event [[72be365c6ba](https://projects.blender.org/blender/blender/commit/72be365c6ba699e84d628b910f1e00eae1e69a85)]
 * Render view with Cropped render region result in line offset / distortion with GPU compositor set to GPU [[99674e36f14](https://projects.blender.org/blender/blender/commit/99674e36f147abcef69556f9615b89615ea2a860)] - Backported to 4.2.3
 * Regression Corrupted output when changing render regions in script with cycles without denoising [[6ae3232eda1](https://projects.blender.org/blender/blender/commit/6ae3232eda16632edd253bee04144ae2d82d51b6)] - Backported to 4.2.3
 * Blank GPU compositor result with Legacy Cryptomatte node unless compositor is updated [[ccab8005f6c](https://projects.blender.org/blender/blender/commit/ccab8005f6ce8d98e7acc5eab3c79e2a678bfcef)]
 * Motion Tracking: Bug with Aply Scale [[9664fb0e699](https://projects.blender.org/blender/blender/commit/9664fb0e699b9383d3b6b1a8fedb3b4db38877e8)]
 * Black rectangle artifact in mask feather [[318125c82b3](https://projects.blender.org/blender/blender/commit/318125c82b328921e408cf4a643d1805fd3476b4)] - Backported to 4.2.3
 * GPU compositor glare node does not work properly on high resolution images [[3a070122ea0](https://projects.blender.org/blender/blender/commit/3a070122ea07ec470d3ceb913f50338858603837)] - Backported to 4.2.3
 * OpenEXR MultiLayer output in 4.2 does not save render layers where "Use for Rendering" is turned off [[db490e90fee](https://projects.blender.org/blender/blender/commit/db490e90fee955f8fd55879b849d3235c27d8a82)]
 * Scale node result is 1 pixel too small in both dimensions, only in cpu mode. [[60bb3b66779](https://projects.blender.org/blender/blender/commit/60bb3b66779aed97150247c42c66e02def018b91)]
 * Compositor NaN in render highlights when adding kuwahara node in anisotropic mode CPU/GPU [[1b392ef0167](https://projects.blender.org/blender/blender/commit/1b392ef01671e11cef080ff7e0c13f13d61c82ed)]
 * Alpha Over resulting scale different in CPU and GPU compositor [[161b1107469](https://projects.blender.org/blender/blender/commit/161b1107469b2c98d8399cb91fec1bf1394162f4)]
 * Regression 'Sequence Render Animation' not working for scene strips in render shading [[580b4a1a2d3](https://projects.blender.org/blender/blender/commit/580b4a1a2d309c7ecface1d562997b7f50d943e9)] - Backported to 4.2.4
 * Compositor crop gizmo can produce invalid crop configurations [[a5376593413](https://projects.blender.org/blender/blender/commit/a53765934135bbbca93b69d58a1d3bff79c1aaa0)]
 * Video Error in video decoding on either Linux or AVX512 CPU (ffmpeg alignment issue?) [[596067ea357](https://projects.blender.org/blender/blender/commit/596067ea357288c1219022c81a6e308e9d236f9e)] - Backported to 4.2.3
 * Compositing Using Multiple Utility Levels Node Freeze Blender [[aac0c2cc5f0](https://projects.blender.org/blender/blender/commit/aac0c2cc5f0eedb05d48d0e60715516baa49eeba)] - Backported to 4.2.3
 * Regression Deinterlace checkbox makes video invisible [[bab4f7a0cdd](https://projects.blender.org/blender/blender/commit/bab4f7a0cdd6a90ad72ad6b7d2e5b5378435294b)] - Backported to 4.2.2
 * VSE Handle tweaking does not work with Select Box tool [[d2091b4b15a](https://projects.blender.org/blender/blender/commit/d2091b4b15a46977369e26c09d330f7854258d66)]
 * VSE Tonemap modifier produces wrong result when strip is not fullscreen [[78972f85592](https://projects.blender.org/blender/blender/commit/78972f85592eaeece90798d7702b879b7b379331)]
 * Float data images display trash in image editor [[a904db3ee7a](https://projects.blender.org/blender/blender/commit/a904db3ee7acf6cc315d9fc9adc92eca94828714)]
 * Compositor Difference between CPU and GPU mode with `Lens Distortion` node [[bb876905498](https://projects.blender.org/blender/blender/commit/bb876905498e0034df3f7e38122310d18e2da8b4)]
 * Compositor has no independent Texture access [[d1f0084a336](https://projects.blender.org/blender/blender/commit/d1f0084a336332c7258ae91d76bdc0ea2129fc56)] - Backported to 4.2.2
 * Cryptomatte doesn't work when its source is EXR file packed into blend file [[758401c6720](https://projects.blender.org/blender/blender/commit/758401c6720d64a7fe5cb616ee44ae32107915f7)] - Backported to 4.2.2
 * Crash when unlinking scene from render layers node in compositor when using GPU device [[844d6d2df08](https://projects.blender.org/blender/blender/commit/844d6d2df08bcca0a2b5b7c90fc75bedbed88a18)] - Backported to 4.2.2
 * VSE prefetch frames crashes with some scene strips [[717b8c08f31](https://projects.blender.org/blender/blender/commit/717b8c08f313750ae052597fc577ba5abe3bb0d2)] - Backported to 4.2.1
 * Adding a compositor node while looking through the viewport realtime compositor leads to crashing [[149825bc2af](https://projects.blender.org/blender/blender/commit/149825bc2af297738eaa6c348fbca914e2ecb501)] - Backported to 4.2.1
 * Adding a Scene to VSE always rendered as sold view (or what ever the "scene strip display" is) [[5ecb70964ed](https://projects.blender.org/blender/blender/commit/5ecb70964ed7006ba236875a4feb1bfa773ea9d4)]
 * Scrubbing on Video Sequencer does not produce sound if scrbbing spans only a few frames [[e6178be3b85](https://projects.blender.org/blender/blender/commit/e6178be3b855d45498e6e6f8685971c70eb5574f)]
 * Compositor File Output node destroys non-color data when saving into non-EXR formats [[9c443492042](https://projects.blender.org/blender/blender/commit/9c4434920424b9305aa5629e9446ff7614cca29f)]
 * CPU compositor Translate sets left and bottom edge to blank pixels [[ef1d22aea57](https://projects.blender.org/blender/blender/commit/ef1d22aea57ff2bd9f8d3dab922d7d9727d9a03b)]
 * VSE UI: Order of VSE Modifier options [[b00a16b4e10](https://projects.blender.org/blender/blender/commit/b00a16b4e10e359ed906077c229542da4efaec75)]
 * Bpy MovieSequence.frame_duration returns wrong value when Adjust Playback Rate selected [[42234e5a8ed](https://projects.blender.org/blender/blender/commit/42234e5a8ede222ebc28971a6aef8cbc2cd227dc)]
 * Compositor Convert Colorspace node crashing Blender 4.2 when viewing realtime compositor in rendered 3D viewport [[69154a5e3b8](https://projects.blender.org/blender/blender/commit/69154a5e3b8509a2d73802e890ff67b95a968284)] - Backported to 4.2.1
 * Video Error in video decoding on either Linux or AVX512 CPU (ffmpeg alignment issue?) [[b17734598df](https://projects.blender.org/blender/blender/commit/b17734598dfc530db6b0d78f15b9173889cdc165)] - Backported to 4.2.1
 * Viewport Compositor: Noticeable performance impact when enabling viewport compositor [[406a2d3ff03](https://projects.blender.org/blender/blender/commit/406a2d3ff0379667ff776f8090323830208387ff)]
 * VSE : shadow of text strip don't shadow the outlines of text + naming issues [[160f27330f2](https://projects.blender.org/blender/blender/commit/160f27330f23d62d3b2dbd58d19934a7b9ad8cf7)] - Backported to 4.2.1
 * Compositor transform/realize can shift image by 1/2 an input pixel [[b3e62d3052e](https://projects.blender.org/blender/blender/commit/b3e62d3052ee78d4ae69869ade43612451afdedd)]
 * VSE Text strip outline wrongly goes "into" the text area (visible with semitransparent font color) [[6432d54ef72](https://projects.blender.org/blender/blender/commit/6432d54ef725d7722c5b175ab40d024a7a4a242b)] - Backported to 4.2.1
 * VSE text outline is not fully rendered for some (mostly "script") fonts [[d80f0aa0680](https://projects.blender.org/blender/blender/commit/d80f0aa068038c3a90166a2847cd0b969a3c7573)] - Backported to 4.2.1
 * Compositor transform/realize can shift image by 1/2 an input pixel [[ab51d879c31](https://projects.blender.org/blender/blender/commit/ab51d879c319dfc7d1de30cd5e405fa0f435b116)]
 * Unreadable OpenEXR output file [[40be124184b](https://projects.blender.org/blender/blender/commit/40be124184bb571669b7346a36003c33c412cb20)]
 * Startup default not saving all render passes [[cf032d68932](https://projects.blender.org/blender/blender/commit/cf032d6893211d194cc05ae7d787e0230e26732e)]
 * VSE Line between two strips draws as multiple pixels randomly although there is no gap [[527e55239bf](https://projects.blender.org/blender/blender/commit/527e55239bf967bb75dbf53611efa2f586e35ccf)] - Backported to 4.2.1
 * VSE misses strip auto-deselect with right click selection (Blender 2.7 x keymap) [[63db751cde9](https://projects.blender.org/blender/blender/commit/63db751cde93c45d64c5e678cfaf6d88e3b7d15d)] - Backported to 4.2.1
 * VSE Meta/scene strips are drawn with a gap to strip start [[7a260b761dd](https://projects.blender.org/blender/blender/commit/7a260b761dd3a5415c344a794ede7b9206317d78)] - Backported to 4.2.1
 * VSE cache not updated for moved strips [[6a39d79967b](https://projects.blender.org/blender/blender/commit/6a39d79967b3c02e0e4d0a8cc3bf6cc7f274d737)] - Backported to 4.2.1
 * VSE - Frame Selected ( Numpad . ) doesnt work anymore [[aac0189e2c4](https://projects.blender.org/blender/blender/commit/aac0189e2c40abd65a56a22277310d8ef23d5898)] - Backported to 4.2.1
 * Compositor does not recognize EXR Multilayer passes immediately if imported by drag and drop [[3ca6aa19c9a](https://projects.blender.org/blender/blender/commit/3ca6aa19c9ad2ab4b04b7af1163781357a7eaa2d)]
 * Compositor Cryptomatte node lagging UI in certain cases [[f0ec207e9c7](https://projects.blender.org/blender/blender/commit/f0ec207e9c7de5762ff23ea1a6897f90829fb478)]
 * Vector pass not being saved correctly when using the File Output node [[308b53746b1](https://projects.blender.org/blender/blender/commit/308b53746b159b9e1365304a9c9a387899a6208a)]
 * VSE Crash when rendering Scene strip [[ea56b408ef2](https://projects.blender.org/blender/blender/commit/ea56b408ef241c43521bf2dca29d104083fb44cd)]
 * Blender 4.2 beta Compositor sun beams do not have falloff and light is stronger comparing to Blender 3.6 Compositor [[acd4a85d6b7](https://projects.blender.org/blender/blender/commit/acd4a85d6b7fed3694dd3132c1cc0fca05412760)]
 * Vector pass not being saved correctly when using the File Output node [[57a6832b178](https://projects.blender.org/blender/blender/commit/57a6832b1787b03670c3277d7b8c934f122199b0)]
 * Cryptomatte layers saved incorrectly with EXR DWA compression when using File Output nodes [[d14ad050787](https://projects.blender.org/blender/blender/commit/d14ad050787010b5758c9a4102f6882b47fa3537)]
 * Compositor GPU File Output doesn't write meta data [[f954a6b5fbd](https://projects.blender.org/blender/blender/commit/f954a6b5fbddf5fe1fc1f08d6df8436812e374d1)]
 * Sequencer Tweak Tool Warnings in Console [[f205079f1e8](https://projects.blender.org/blender/blender/commit/f205079f1e80057596539e1972c064ea94dc52a4)]
 * Switch (and move) camera leads to incorrect 7.1 surround audio mixing [[bf74978260a](https://projects.blender.org/blender/blender/commit/bf74978260acf2385b5bf8264867c3c1d0c3fe1a)]

## Viewport & EEVEE
 * Rebaking an eevee volume probe produces significant lighting changes [[e3a960ef2e4](https://projects.blender.org/blender/blender/commit/e3a960ef2e440e86c8fe60cf8079cd1b33c21a50)]
 * Crash when switching from EEVEE to Cycles while rendering animation [[dafa3fb88fb](https://projects.blender.org/blender/blender/commit/dafa3fb88fb2aad6f1e690e0a8857f17d0fa4ebb)] - Backported to 4.2.4
 * MacOS Crash when dragging window from one screen to another [[03d31a3717c](https://projects.blender.org/blender/blender/commit/03d31a3717c661952ed0add9446f3826e6d646a9)] - Backported to 4.2.4
 * Blender 4.2 MacOS Eevee use absurd amount of memory when render the same file compare to 4.1 [[658700ddffa](https://projects.blender.org/blender/blender/commit/658700ddffae2c4e576cd519e2bc4878106a8dcd)] - Backported to 4.2.4
 * EEVEE Light Probe RAM Pool Crash on MacOS [[750a9af5182](https://projects.blender.org/blender/blender/commit/750a9af5182e272b443f405e8fb2ac979d5ed94d)]
 * EEVEE next halo on motion blurred out of focus objects [[e47e720f061](https://projects.blender.org/blender/blender/commit/e47e720f0612bdcce10813b08a82648c5fabb5b0)]
 * EEVEE Volume probe create bad shading if resolution is divisible by 3 [[b1185a47365](https://projects.blender.org/blender/blender/commit/b1185a47365cd71b98f5d39fa781fde0b7546f77)] - Backported to 4.2.4
 * Overlay Apply scale on lights is showing wrong size of light [[81f439ff376](https://projects.blender.org/blender/blender/commit/81f439ff376e5c191acec4e4843f463c13d856c3)] - Backported to 4.2.4
 * Shadow breaks with linked duplicate [[ac1069805ce](https://projects.blender.org/blender/blender/commit/ac1069805ce33b0aa0a40cfbc80e4118d1424ee1)] - Backported to 4.2.4
 * Spotlights with Shadows Cast Strange Artifacts in Eevee Next (4.2 RC) [[3764f53b1bd](https://projects.blender.org/blender/blender/commit/3764f53b1bd9c664d8c0e71b2176efadd21efe3d)] - Backported to 4.2.4
 * Blender crashes with a large Light Probe Volume Pool on Intel Arc [[483f96ce733](https://projects.blender.org/blender/blender/commit/483f96ce7333a5eb62ddda172f5aba966289232e)]
 * Regression Eevee Not rendering textures when displacement is used [[9bcba25bc2a](https://projects.blender.org/blender/blender/commit/9bcba25bc2aa5ad96391423daa6d06339c54083b)] - Backported to 4.2.4
 * Blender crashes with a large Light Probe Volume Pool on Intel Arc [[7d5712be9b3](https://projects.blender.org/blender/blender/commit/7d5712be9b3f4fbe833d1c175c0f522e03cf62ec)]
 * EEVEE Next light bug: At high power values, light intensity decreases. [[52f2bb53b99](https://projects.blender.org/blender/blender/commit/52f2bb53b99e85d41c0ecaec44cf33a1d02246e9)]
 * Using gizmo to adjust image empties does not immediately update the corresponding values in its properties [[3dd20a64f03](https://projects.blender.org/blender/blender/commit/3dd20a64f03caa6d884a860d9b2bd7b8bc25754b)]
 * Large VDB files crashing Blender 4.2 RC - macOS Metal [[52dfb4aa8fe](https://projects.blender.org/blender/blender/commit/52dfb4aa8fe1b337354641d2907925aac890e501)] - Backported to 4.2.3
 * EEVEE Position render pass missing in viewport shading panel [[fcc263162de](https://projects.blender.org/blender/blender/commit/fcc263162dea191938f531d0ebb9c86d876de403)]
 * Incorrectly displays texture transparency when face orientation is enabled [[6c52e4752f1](https://projects.blender.org/blender/blender/commit/6c52e4752f19ab1029bc3899f7d51e312dd6e65c)] - Backported to 4.2.3
 * Cryptomatte segmentation error [[ac24912736b](https://projects.blender.org/blender/blender/commit/ac24912736be02cde32b1b8c1799c94e12ce709c)] - Backported to 4.2.3
 * UV islands display issue - macOS Metal [[57f7d6380cb](https://projects.blender.org/blender/blender/commit/57f7d6380cb4b2a19091d4889d68ee195544fa4a)] - Backported to 4.2.3
 * EEVEE Dithered shadows get darker with higher sample counts [[3d74be9c5b0](https://projects.blender.org/blender/blender/commit/3d74be9c5b0537a121b7412b9707b76b2da89193)]
 * EEVEE-NEXT Inconsistent DOF Preview [[88c84e6a37f](https://projects.blender.org/blender/blender/commit/88c84e6a37f03731a4b0cfd2d01a7d3e919ca2f0)] - Backported to 4.2.3
 * Combining two Shader to RGB nodes destroys the Normals AOV [[c92514f1e75](https://projects.blender.org/blender/blender/commit/c92514f1e75737c34a2c75aa9e3a37a434e7a9c1)] - Backported to 4.2.3
 * Eevee Next: Mesh Plane Disappears on Some Angles [[02cf5f5f0f4](https://projects.blender.org/blender/blender/commit/02cf5f5f0f412a4d793093e5bd12d2c259b08cf5)] - Backported to 4.2.3
 * EEVEE Next - Refractive materials do not refract emissive materials [[318eab55841](https://projects.blender.org/blender/blender/commit/318eab55841fd32eabfe28deaf4bcbcd2c31392c)] - Backported to 4.2.3
 * HDRI Screen tearing [[9c54ddff690](https://projects.blender.org/blender/blender/commit/9c54ddff6907f0c194fcd63b0c46f45667b87b80)] - Backported to 4.2.3
 * Meshes or Curves with just "wire" edges disappear in the viewport if SubD is used while in Viewport "Wireframe" mode [[0d9ab189d1e](https://projects.blender.org/blender/blender/commit/0d9ab189d1e016b06c6369bd7655e4674ae7d868)] - Backported to 4.2.2
 * Assert (null VBO size) in draw code when opening a Gold blendfile [[d3b655f76d2](https://projects.blender.org/blender/blender/commit/d3b655f76d2deea794a62572fe962345e2254803)]
 * EEVEE Can not use Curves Info->Length [[0db13bef196](https://projects.blender.org/blender/blender/commit/0db13bef196c3fa7476b1282d1c271dc72ca44da)] - Backported to 4.2.2
 * GP Objects Flicker in Video Sequencer when Material Preview mode. [[d3909d42c63](https://projects.blender.org/blender/blender/commit/d3909d42c633cd43b35d679dd696c0020758bdb7)] - Backported to 4.2.2
 * Default Cube has `Transparent Shadows` off [[2afb9e3d95c](https://projects.blender.org/blender/blender/commit/2afb9e3d95cc62a005e532bb4eefbb7476a7f9f5)] - Backported to 4.2.2
 * EEVEE Command Line Rendering on the Mac freezes, memory usage constantly increases up until freeze [[dc8f5d5b898](https://projects.blender.org/blender/blender/commit/dc8f5d5b898f0f9dfe57f8149e7fc575d3dff12f)] - Backported to 4.2.2
 * EEVEE-NEXT - Shadows Break with Subdivision Surface (RDNA2 only?) [[78af1722cfc](https://projects.blender.org/blender/blender/commit/78af1722cfc198258e49b82439ce2243a2c469e0)] - Backported to 4.2.2
 * Backface culling keeps "face orientation" overlay [[e5ba4cc7b00](https://projects.blender.org/blender/blender/commit/e5ba4cc7b0077aa449685ecff9eb7b4181b559df)]
 * Backface culling keeps "face orientation" overlay [[0bad30317cd](https://projects.blender.org/blender/blender/commit/0bad30317cdf7033d11b52bb6c8b25cd30d6a179)] - Backported to 4.2.2
 * Black transparent BSDF is invisible [[0d4c76c9862](https://projects.blender.org/blender/blender/commit/0d4c76c9862508e092b007ea6be5be129d20107a)] - Backported to 4.2.2
 * Data passes have wrong alpha with macOS GPU compositor [[529c720457b](https://projects.blender.org/blender/blender/commit/529c720457b447d802a495ab8e8e8b8bd971a3db)] - Backported to 4.2.2
 * 4.2.1 EEVEE Cryptomatte pass doesn't match Cycles [[7391810b53e](https://projects.blender.org/blender/blender/commit/7391810b53e8034ad7ed299886778a5339d98a2b)] - Backported to 4.2.2
 * Regression Environment pass is not rendered (or rendered black) with 4.2 EEVEE [[00be586a820](https://projects.blender.org/blender/blender/commit/00be586a8200c5cf01c8e5a709849cad7ce9ac60)] - Backported to 4.2.1
 * 4.2 EEVEE Cryptomatte pass no longer matches Cycles [[925753c83bf](https://projects.blender.org/blender/blender/commit/925753c83bfcdc5d9f6899e882c5705d049d7eaa)] - Backported to 4.2.1
 * 4.2 EEVEE Cryptomatte pass no longer matches Cycles [[c4e90eddcb3](https://projects.blender.org/blender/blender/commit/c4e90eddcb3c4240e2589596cc2180496691381c)] - Backported to 4.2.1
 * Setting Vertices Y to 0 and sliding in grid node causes blender to crash [[4349d6b766c](https://projects.blender.org/blender/blender/commit/4349d6b766c3c73a75029e25b76bab8e180cebb0)] - Backported to 4.2.1
 * MacOS Armature text line rendering is broken [[a4519710261](https://projects.blender.org/blender/blender/commit/a45197102615033e5819483c7831824ecf551e76)] - Backported to 4.2.1
 * Crash Display wireframe in object mode on subdivided mesh with a lone edge [[a9d318d2f3e](https://projects.blender.org/blender/blender/commit/a9d318d2f3eb75c6760977029c8ad9bc4819611d)] - Backported to 4.2.1
 * EEVEE-Next Volume does not immediately work after opening Blender [[09390858abb](https://projects.blender.org/blender/blender/commit/09390858abb9f58667cd4bf8d62125571d921a54)] - Backported to 4.2.1
 * Weight Paint Not Appearing in Material Preview viewport shading [[644e7bf3094](https://projects.blender.org/blender/blender/commit/644e7bf30943bc0ec6848afc6fab375fa594e942)] - Backported to 4.2.1
 * Blender crashes when adding a working Geometry Node modifier to a specific object in Edit Mode [[0b97228cbcb](https://projects.blender.org/blender/blender/commit/0b97228cbcbda57db0543e92e27fdf7330e6bf9b)] - Backported to 4.2.1
 * Eevee 4.2 no longer exports Cryptomatte metadatas [[4d85c03815c](https://projects.blender.org/blender/blender/commit/4d85c03815c62f706a7211da18f5a09cdcf31fcb)] - Backported to 4.2.1
 * Alpha Clip still visible in material settings when using Cycles render engine [[0c429ee652e](https://projects.blender.org/blender/blender/commit/0c429ee652e9c6787dd816ac833a8a8783969917)] - Backported to 4.2.1
 * Random segfaults when working with GP objects [[56bd0c7e4ad](https://projects.blender.org/blender/blender/commit/56bd0c7e4ad6e440298e97c33de1a8f9a54c2527)]
 * Regression The Mix RGB node produces unexpected results in the World Shader [[6d302ca7ca0](https://projects.blender.org/blender/blender/commit/6d302ca7ca013bdd74e5d8bc89f83b87855bffc0)] - Backported to 4.2.1
 * Eevee Next missing render frame progress in stdout [[9d797b834e8](https://projects.blender.org/blender/blender/commit/9d797b834e86e3ab1957a9bfbf31dceae9d091c4)] - Backported to 4.2.1
 * Noise Artifacts caused by shader mixing [[3fcd80db682](https://projects.blender.org/blender/blender/commit/3fcd80db682588d1796f5354e08889cba16b3bb2)]
 * Transparent 3D viewport issue [[c929e251db1](https://projects.blender.org/blender/blender/commit/c929e251db14d0455b82d43a041774d1241162f8)]
 * UI | HDRI preview [[71de5456973](https://projects.blender.org/blender/blender/commit/71de54569733411448f1a87bfba3bdcdac463c70)]
 * Face orientation option affects wired objects [[41da774dd05](https://projects.blender.org/blender/blender/commit/41da774dd050eb58463160c16acf36c9f89e67ef)] - Backported to 4.2.2
 * Regression Missing textures not shown in Texture Paint mode with magenta [[d8e710885cb](https://projects.blender.org/blender/blender/commit/d8e710885cb1084f69b5f9cfbf7480ed1fbdc35d)]
 * Blender doesn't run with --gpu-backend vulkan flag on linux (ubuntu) [[faf0171c36a](https://projects.blender.org/blender/blender/commit/faf0171c36ae3c9aa0d7b913138087a2ba9d68ce)]
 * EEVEE Next: Holdout doesn't work at object level [[b5a9a67dcf5](https://projects.blender.org/blender/blender/commit/b5a9a67dcf5ca8c805b4c2f058345f5c6776c6ff)]


## UNKNOWN
 * Fix #129167: Operator search can hang for some operator names [[1d6add574d7](https://projects.blender.org/blender/blender/commit/1d6add574d7547cc76217040b49075bad8c7f9e7)] - Backported to 4.2.4
 * Fix #125588: Revert: Fix #120592: File output node not showing up in search [[7903aa6faa3](https://projects.blender.org/blender/blender/commit/7903aa6faa3399f7beac1e89144aa731f76e8954)] - Backported to 4.2.1
# Blender 4.3: Cycles

## HIP-RT support on Linux

Support for hardware accelerated ray-tracing has been added for Linux platform
(blender/blender!121050)

From the implementation details this is done by switching Blender to use open-source HIP-RT
libraries for both Linux and Windows.
These libraries are pre-compiled and stored in the typical pre-compiled platform repository.
  
![](images/cycles_hiprt.png)

There is some known issue with performance in Spring file,
which is not specific to the work done for Blender 4.3, and will be worked on further.

## Volumes

The Volume Scattering node now supports more phase functions in addition to Henyey-Greenstein (blender/blender!123532)

* Rayleigh for atmospheric scattering
* Fournier-Forand for underwater scattering
* Draine for interstellar scattering
* Mie for clouds and fog


<figure markdown="span">
<div class="grid" markdown>
<figure markdown="span">
  ![](images/volume_arctic_hg.jpg)
<figcaption>Arctic scene with Henyey-Greenstein.</figcaption>
</figure>
<figure markdown="span">
  ![](images/volume_arctic_mie.jpg)
<figcaption>Arctic scene with Mie.</figcaption>
</figure>
<figure markdown="span">
  ![](images/volume_water_hg.jpg)
<figcaption>Underwater scene with Henyey-Greenstein.</figcaption>
</figure>
<figure markdown="span">
  ![](images/volume_water_ff.jpg)
<figcaption>Underwater scene with Fournier-Forand.</figcaption>
</figure>
</div>
<figcaption>Sample scenes provided by Christopher Tyler.</figcaption>
</figure>

## Other

* The Oren-Nayar BSDF (used for Diffuse BSDF with non-zero roughness)
  now is energy-preserving and accounts for multiscattering.
  (blender/blender!123345)
  * This results in slightly brighter and more saturated results for existing materials.
* The Principled BSDF now has a Diffuse Roughness input,
  which acts like the Roughness input on the Diffuse BSDF
  (blender/blender!123616)
* AMD and Intel GPU support from Metal backend has been removed (blender/blender!123551)
* The panoramic camera now supports central cylindrical projection as a mapping option
  (blender/blender!123046)
* Use more compact representation for B-Splines for OptiX
  (blender/blender!125899, blender/blender!127861)
  NOTE: This increases the minimum driver version to 495.89.
* Support for Vega in Cycles AMD HIP backend has been removed (blender/blender@c2f93e0f68aec9fa48ba311401ef2545c53b24ba)

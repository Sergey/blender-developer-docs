# Blender 4.3: Animation & Rigging

## Properties Editor

![Action selectors in the Material properties](images/action_selector_in_properties_editor.png)

Action selectors are now shown for data-blocks in the Properties editor
((blender/blender@f917b60036976a2fe2e7f990e171f20cb502a261),
(blender/blender@383c3c825363be90c0d8a53d7915ba3be749f1ed)). This means that it is now possible to
select the Action for Mesh (and its Shape Keys), Material (and its Shader Node Tree), World
(and its Shader Node Tree), Scene, Camera, Light, and many other data-block types.

## Motion Paths
Added a theme entry to drive the color of the motion path line before
and after the current frame. The setting can be found under "3D Viewport"
and is called "Before Current Frame" and "After Current Frame".
(blender/blender@5427775fef, [Manual](https://docs.blender.org/manual/en/4.3/animation/motion_paths.html))

## Keyframing
When inserting keys, all other keys get deselected leaving only the newly created keys selected.
(blender/blender@6ef77a0d22)
This does not affect the Python API where the selection is unchanged after inserting keys.
In order to easily achieve the same result with Python there is a new function on the Action
called `deselect_keys`.

## Bone Eyedropper
Properties where a bone can be chosen now have an eyedropper button that
allows to pick bones from the 3D Viewport or the Outliner.
In the 3D viewport, bones can only be picked while the armature is in Pose Mode or Edit Mode.
While in Object Mode the picking will fail and throw a warning.
(blender/blender@fae19d7c92, [Manual](https://docs.blender.org/manual/en/4.3/interface/controls/buttons/eyedropper.html))

<figure>
<video src="../videos/bone_eyedropper.mp4"
       title="Bone Eyedropper demo"
       width="720"
       controls=""></video>
<figcaption>Bone properties now show an eyedropper button to pick bones</figcaption>
</figure>

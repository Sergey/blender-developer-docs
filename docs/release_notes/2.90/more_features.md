# Blender 2.90: More Features

## Data Duplication

Data-blocks duplication (or deep copy, full copy) has been improved.

- Doing a full copy of a scene will now use the same preference settings
  as when duplicating a collection or object, to determine which
  sub-data-blocks should also be copied
  ([\#77255](http://developer.blender.org/T77255),
  blender/blender@eee35ebdfbed).
- Doing a full copy of a scene or collection now duplicates actions only
  if relevant option is set in the user preferences.
- All usages of duplicated sub-data-blocks should now be properly
  remapped to their copies.
- Doing a full copy of a local data-block will now never duplicate the
  linked sub-data it may use (this was already the case for scenes, but
  not for objects or collections before).
  - However, doing a full copy of a linked data-block will also
    duplicate all of its linked sub-data, if allowed (since user then
    wants to get a locale duplicate of its linked data).

## Baking

- New Ray Length setting, which limits how far rays can travel before
  they are excluded from the baking result. Useful for baking meshes
  with holes in them.
  (blender/blender@27cac4a102b9)

## Motion Tracking

- Added new lens distortion model which allows to solve motion in
  Blender and do compositing in Natron/Nuke. It is available in the Lens
  settings of movie clip and is called "Nuke" (which is the default
  model in Natron as well).

## Sequencer

- Add frame interpolation option to speed effect.
  (blender/blender@99cb6dbe6525)

## Experimental Features

- The `User Preferences → Developers Extra → Experimental` options are
  now organized in different categories
  (blender/blender@dd9c0dbf5ed5).
- They are also only available on daily builds of master. Beta and Final
  releases don't have their panels.

## Linux

- Linux now has initial Wayland support, when building the
  `WITH_GHOST_WAYLAND` option. Some features are still missing in this
  implementation and X11 remains used by default for now.

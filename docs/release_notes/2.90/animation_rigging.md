# Blender 2.90: Animation & Rigging

## Library Overrides

- Many more properties and data are now overridable (all constraints and
  modifiers, camera objectdata, etc.).
- Overrides are now duplicated as well when copying an override
  data-block (e.g. if you select a whole object hierarchy and duplicate
  it in the 3DView, you will get a new override of the same linked
  data).
- Python-defined properties and custom properties can also be overridden
  (as long as they are defined as overridable, and belong to some data
  that is also overridable).
- Making override of an object or collection from the 3DView now does a
  complete check of dependencies and overrides everything needed (Note
  that this will override a lot of data-blocks in typical complex
  production characters e.g.).
- From the outliner, you can override the hierarchy of linked
  data-blocks leading to the one you selected. It will also behave
  similar to the 3DView operation when applied on an object or
  collection.
- 3DView 'make override' operator has been upgraded:
  - No more need to select a 'main' object when overriding an instanced
    collection (this was not used anymore anyway).
  - One can now override a linked, but not object-instanced, collection
    fro; the 3DView, by selecting one of its objects and calling the
    operator.
- Overrides can now be reset from the outliner (i.e. all defined
  override properties are removed, except for those ensuring
  relationships between IDs, and override data-blocks get reloaded),
  either individually or as a whole hierarchy.
- Overrides will now be reloaded when their linked library reference are
  reloaded.

## Constraints

- Matrices with negative scale can now be properly interpolated. This
  means that it is now possible to use, for example, a Copy Transform
  constraint and target an object with Scale X=-1, without glitches when
  you dial down the influence slider.
  (blender/blender@a5e176a8ed8a
  and demo videos in [D8048](https://developer.blender.org/D8048)).
- Track to constraint uses -Z as track axis and Y as up-axis by default
  now
  (blender/blender@be009020827c).
  That way defaults are appropriate for the main use case of camera
  tracking.

## Graph Editor

- Using the scrubbing region to change the current frame only changes
  the frame, not the cursor
  (blender/blender@fc59febb1b30).

## Drivers

- New `lerp`, `clamp` and `smoothstep` functions, inspired by GLSL
  `mix`, `clamp` and `smoothstep`.
  (blender/blender@f8cc01595d)
- The simple expressions subset now includes `lerp`, `clamp`,
  `smoothstep`, `round` and `log(val,base)`.
  (blender/blender@f8cc01595d)

### Dependency Graph passed to Drivers

Drivers can now get to the current dependency graph (for example to get
to the current view layer), via the new `depsgraph` variable that is
available to them. This can then be used in the driver expression, and
passed to custom driver functions (as registered in
`bpy.app.driver_namespace`) if necessary.

Here is an example from [T75553](https://developer.blender.org/T75553),
where a driver function is used to determine whether the current view
layer is named `View Layer 1`:

![Example of a driver using `depsgraph` in its expression](../../images/Driver_Depsgraph_2.90.png){style="width:222px;"}

``` Python
import bpy

def is_view_layer_1(depsgraph: bpy.types.Depsgraph) -> bool:
    print(f"depsgraph={depsgraph.mode} VL={depsgraph.view_layer.name}")
    return "View Layer 1" in depsgraph.view_layer.name

bpy.app.driver_namespace['is_view_layer_1'] = is_view_layer_1
```

The driver can now use `is_view_layer_1(depsgraph)` as its expression.

**Note:** As a rule of thumb, never use `bpy.context` in drivers.
Drivers can be evaluated in multiple contexts at the same time, for
example when Blender is rendering in one window and showing the 3D
viewport in another. In these cases `bpy.context` will return the
wrong data half the time. The `depsgraph` passed to the driver will
always be correct, though.

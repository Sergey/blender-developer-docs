# Blender 4.0: Compositor & Sequencer

## Compositor

### Viewport Nodes

The following nodes are now supported in the [Viewport
Compositor](https://docs.blender.org/manual/en/4.0/compositing/realtime_compositor.html):

- [Movie
  Distortion](https://docs.blender.org/manual/en/4.0/compositing/types/transform/movie_distortion.html)
- [Sun
  Beams](https://docs.blender.org/manual/en/4.0/compositing/types/filter/sun_beams.html)
- [Keying](https://docs.blender.org/manual/en/4.0/compositing/types/keying/keying.html)
- [Kuwahara](https://docs.blender.org/manual/en/4.0/compositing/types/filter/kuwahara.html)
  (classic and anisotropic)
- [Inpaint](https://docs.blender.org/manual/en/4.0/compositing/types/filter/inpaint.html)
- [Double Edge
  Mask](https://docs.blender.org/manual/en/4.0/compositing/types/mask/double_edge_mask.html)

### Kuwahara Filter

Kuwahara filter node for the compositor, to give images a painterly
look.
(blender/blender@f3cb157452b8af6782bac88d34946e929a4fa6f8)

![Multiple iterations of Anisotropic Kuwahara filter](../../images/Anisotropic_Kuwahara_Filter_Example.png){style="width:800px;"}

### User Interface

- The compositor nodes add menu has been re-organized for consistency
  with other editors.
  (blender/blender@75919610b40e6f6c6eea8246185fc565adbc950a)
- Node preview images have been moved to an overlay displayed above the
  node. The overlays menu has an option to show and hide all previews
  (blender/blender!108001).

![](../../images/Blender4.0_node_previews.png){style="width:600px;"}

## Sequencer

### Interactive Retiming

A new system for interactively retiming strips was added in the
sequencer.
(blender/blender@e1f6587f12f127b9a59fa35d6e5977fda2449a2b,
blender/blender@4dc026ec8e91c21d9b53c7d2f8bc0f3c56cc586f,
blender/blender@6d215b87ce5d1c524c17a06535b6c324a9366945,
blender/blender@f4d6edd4764a780cf673a2007b45a7de871fc6f1,
blender/blender@86a0d0015ac7bb114cbc44a03dd6b422a5ac709f)

Adjust the speed of strips by dragging retiming keys. Additional keys
can be added by pressing the I key. The keys can be edited by pressing
the Ctrl + R shortcut.

Retiming also supports smoothly transitioning from one speed to another.
Transitions can be created by right clicking on a key, or from retiming
menu and choosing Add Speed Transition. Freeze frames can be added
similar to transition keys.

The speed of whole strips or a retimed segment can be set by using Set
Speed operator from retiming menu.

Retiming keys can be hidden by turning off the retiming overlay.

### Sound Strips

- Sound strip waveforms are now processed much faster.
  (blender/blender@b11540a65f)
- New equalizer modifier for sound strips.
  (blender/blender@1015bed2fd)

## Breaking Changes

- The Depth socket was removed from the non-multilayer image node,
  viewer, and composite output nodes. This was a legacy feature,
  replaced by more general multilayer OpenEXR output.
  (blender/blender@e1b60fdb913ebe60a57e2459f0d1fbc1f921e643)
- The "Distort" socket was renamed to "Distortion" in the Lens
  Distortion compositing node.
  (blender/blender@fe62eebba45ac865179f29993529438d3240b439)
- Color multiplication does not affect alpha by default. 
  To multiply alpha channel, "Multiply Alpha" option has to be 
  checked.
  (blender/blender@14827de2a9303dc660341090b616cb2378dc4780)

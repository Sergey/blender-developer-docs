# Blender 4.0: Modeling

## Modifiers

The Add Modifier menu has been changed to a standard menu and now
includes [geometry nodes assets](geometry_nodes.md).

A new "Shift A" shortcut opens the Add Modifier menu in the properties
editor.

## Snap and Navigation

You can now press **B** to set **base point** when you transform
objects. This allows for a fast and precise snap from vertex to vertex.

Additionally you can navigate while transforming while holding **Alt**,
and there are different snap symbols for the different snap types
(vertex, mid-point, perpendicular, ...).

<figure>
<video src="../../../videos/All_Snap_Improvements.mp4" controls="">Video</video>
<figcaption>All Snap Improvements</figcaption>
</figure>

- It is now possible to navigate while transform (Move, Rotate, Scale,
  Edge Slide, Vert Slide, and Shrink/Fatten), by default you have to
  hold `Alt` and navigate.
  (blender/blender@33c13ae6e3,
  blender/blender@017d4912b2).
- Transform operations now have a new feature to edit the 'Snap Base'
  (blender/blender@3010f1233b).
- Bones now support snapping with "Align Rotation to Target"
  (blender/blender@dea93845af
  and
  blender/blender@4d1593c4ad).
- New snap symbols
  (blender/blender@9c2e768f5baf,
  blender/blender@fb556c75df4c).

### User Interface

![Snapping Menu](../../images/Snapping_Menu.gif){ align=right }

The snapping menu has been reworked
(blender/blender@8e059b569b).

- *Snap With* was moved to the beginning of the popover
- *Align Rotation to Target* and *Backface Culling* were moved closer to
  the snap targets
- *Snap With*, *Target Selection* and *Align Rotation to Target* are no
  longer hidden by varying the mode and options
- *Project Individual Elements* has been replaced with the *Face
  Project* option
- *Face Nearest* has been moved to stick together with the *Face
  Project* option

<br clear="both"/>

## Shape Keys

- The Blend From Shape and Propagate To Shapes operators now respect X
  symmetry
  (blender/blender@0bd95dd96331,
  blender/blender@4d0dbab5b1bfcef)

## UV Editor

- Add Invert Pins operator.

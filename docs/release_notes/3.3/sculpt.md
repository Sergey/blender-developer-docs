# Sculpt, Paint, Texture

## UDIM

Packing UDIM texture sets into .blend files is now supported.
(blender/blender@578771ae4dc)

## UI

- Use constant size for voxel remesh size edit by default
  (blender/blender@5946ea938a0).
- Add operator to duplicate the active color attribute layer
  (blender/blender@9c28f0eb37a)

<!-- -->

- There are new icons for the trimming tools
  (blender/blender@17fc8db104f).

![](../../images/Trim_icons_temp.png "../../images/Trim_icons_temp.png")
*The old icon was too similar to existing selection tool icons. We
updated the icon to avoid confusion, especially with planned future
features.*

## New Features

- Add elastic mode to transform tools
  (blender/blender@e90ba74d3eb826abab4ec65b82fe0176ce3b7d1b).

## Performance

- Drastically improve drawing performance when sculpting with EEVEE
  enabled
  (blender/blender@285a68b7bbf).

# Pipeline, Assets & I/O

## General

- Improved performance when importing USD, Alembic, OBJ files with
  massive amounts of objects
  (blender/blender@230f72347a,
  blender/blender@5b5811c97b,
  blender/blender@94323bb42,
  blender/blender@7f8d05131a,
  blender/blender@092732d11).
  Importing a production USD scene with 260 thousand objects went from
  3.5 hours down to 1.5 minutes.

## Alembic

- The export operator can now have presets
  (blender/blender@1d668b635632)

## OBJ

- Importer and exporter support .obj vertex colors now
  (blender/blender@1b4f35f6a588).
  Importer can import both "xyzrgb" and "#MRGB" vertex color formats;
  exporter optionally writes "xyzrgb" format.
- The Python based OBJ importer/exporter is marked as "legacy" in the
  menu item now; the new C++ based one got "experimental" label removed.
  Addons using `bpy.ops.import_scene.obj` and
  `bpy.ops.export_scene.obj` APIs are strongly encouraged to switch to
  `bpy.ops.wm.obj_import` and `bpy.ops.wm.obj_export`.

## STL

- New experimental STL (.stl) importer
  (blender/blender@7c511f1b47d8).
  The new importer is written in C++ and is about 8 times faster than
  the Python importer.

## USD

- OpenVDB volumes can now be exported to USD
  (blender/blender@ce3dd12371f5).

## glTF 2.0

glTF I/O is an python addon, change logs can be retrieved on [Addon
page](add_ons.md)

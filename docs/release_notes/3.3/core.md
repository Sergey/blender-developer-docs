# Core

## Library Overrides

The main focus for this release is to improve the usability of Library
Overrides, in particular the UI/UX.

This includes fixes and refinements on the Outliner views introduced in
Blender 3.2 (the `Properties` and `Hierarchy` ones), and major
simplification of the menus.

![Library Override menu in the Outliner, Blender 3.2 vs. 3.3](../../images/3.2_vs._3.3_liboverride_outliner_menu.png){style="width:1200px;"}

Library overrides now have their own sub-menu, with only three main
operations (make, reset and clear). This is exposed consistently in all
three places where the user can control library overrides (the Outliner,
the 3D Viewport for objects, and the ID selection widget in e.g. the
Properties editor).

### Detailed Changes

- Added full support for camera's background images
  (blender/blender@3437cf155e7c).
- When creating partial override hierarchies, dependencies are
  considered in both directions, so e.g. if one overrides an armature,
  the meshes that depend on it will also be overridden now
  (blender/blender@f0b4aa5d59e3).
- Added 'editable/clear' toggle to ID template, using
  <span class="hotkeybg"><span class="hotkey">⇧
  Shift</span><span class="hotkey" style="padding-left: 3px;">LMB
  ![](../../images/Template-LMB.png "../../images/Template-LMB.png")</span></span>,
  to allow user to easily toggle between editable override, and
  non-editable, cleared override statuses
  (blender/blender@e4dd644d6be0).
- Added/reworked View3D menu into its own sub-menu, with same three main
  operations as in the Outliner (Make, Reset and Clear, see below for
  details)
  (blender/blender@a42896a8a142).
- LibOverride: Add Make/Reset/Clear entries to IDTemplate contextual
  menu, see below for details
  (blender/blender@fec254364884).

#### Rework Outliner Contextual Menu

Follow-up to design discussions here at the studio, add library override
operations into their own sub-menu, with three main entries:

- Make: Create, or enable for user editing, override hierarchies.
- Reset: Keep overrides data, but reset all local changes to the
  reference linked data values.
- Clear: like reset, but also turn editable overrides back to system
  overrides (aka non user editable).

Those three options can all operate either on the selected items, their
content only, or both.

Advanced operations are moved into a "Troubleshoot" sub-menu, where one
can resync, resync enforced, and fully delete library overrides. Those
operations always affect a whole override hierarchy, regardless of which
items are selected or not.

Ref.
blender/blender@c73cc15e07ea,
blender/blender@a93f6a5429d6.

## Data Management

- Isolated dependency islands of data-blocks (e.g. an Object using a
  Material, which has a driver using this same Object) are now properly
  detected and deleted by the recursive purging operation
  ([\#T98029](http://developer.blender.org/TT98029),
  blender/blender@97dd10707097).
- The ‘Purge’ button of the Orphaned view of the Outliner now uses
  recursive purge (instead of the ‘one-level’ one only deleting
  data-blocks with no users)
  (blender/blender@a27024e36d87).
- The way Blender ensures that all data blocks have unique names has
  been rewritten, and scales much better to high object counts
  (blender/blender@7f8d05131a).
  This speeds up many operations, e.g. importing large scenes,
  duplicating many objects, etc.

## Others

- JPEG preview thumbnails created faster and with less RAM.
  (blender/blender@8960c6e06017)
- EXR preview thumbnails created for all types and with less RAM.
  (blender/blender@f600a2aa6df1)
- Improved report about invalid relation in the dependency graph.
  (blender/blender@2da7977e3ef)
- Font fallback support. No noticeable changes until we approve a stack
  of fonts.
  (blender/blender@524a9e3db810)
- Variable / Multiple Master Font support. No user-facing changes yet.
  (blender/blender@b9c0eed206b0)
- Duplicating many objects is faster now
  (blender/blender@2d041fc468,
  blender/blender@7f8d05131a).
  Shift+D duplicating 10 thousand cubes went from 14 seconds down to 2
  seconds.

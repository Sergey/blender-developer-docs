# Blender 3.3 LTS Release Notes

Blender 3.3 LTS was released on September 7, 2022.

It will be maintained until September 2024.

Check out out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-3/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/3-3/) for a list of bugfixes
included in each version.

## [Animation & Rigging](animation_rigging.md)

## [Core](core.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Python API & Text Editor](python_api.md)

## [Render & Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [VFX & Video](vfx.md)

## [Add-ons](add_ons.md)

## [More Features](more_features.md)

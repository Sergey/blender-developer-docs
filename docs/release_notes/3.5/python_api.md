# Python API

## Breaking changes

### BGL Deprecation

BGL is a direct wrapper around OpenGL calls. The BGL module is
deprecated and since Blender 3.4 has been replaced with the `gpu`
module. From this release it will not work when using the Metal backend.
Add-ons and scripts that uses the bgl module are warned about the
deprecation and should use the [gpu
module](https://docs.blender.org/api/3.5/gpu.html) in stead.

### Class Registration

Registering classes with the names matching existing built-in types now
raises an error
(blender/blender@52f521dec4491feb746b458e120d0f3e8d05b19a).

### Node Group Sockets

Virtual sockets are the extra sockets added to group input and output
nodes. Previously connecting a link to them would create an input or
output in the corresponding node group. That feature was meant as a
UI/UX feature though, and is now implemented differently in Blender and
doesn't happen automatically. The correct way to add a group input from
Python is to use `node_tree.inputs.new(...)`
(blender/blender@70260960994d).

### Grease Pencil

Auto-masking settings were moved from brush to tool settings, for
details see
blender/blender@4c182aef7ce0.

### Motion Tracking

The internal storage of the optical center (principal point) has been
changed in
blender/blender@7dea18b3aa
to a normalized space.

The normalized space refers to coordinate `(0, 0)` corresponding to
the frame center, `(-1, -1)` the left bottom frame corner, `(1, 1)`
is the top right frame corner. This notation is available via
`clip.tracking.camera.principal_point` property.

For some algorithms it is convenient to operate in the pixel space. For
those usecases the new `clip.tracking.camera.principal_point_pixels`
property has been added.

The old `clip.tracking.camera.principal` has been removed to avoid
ambiguous naming in the API.

### Armature Modifier

An old bug where the effect of the Invert Vertex Group toggle of the
Armature modifier was inverted when Multi-Modifier was active was fixed.
(blender/blender@ea1c31a24438)

Scripts creating complex armature modifier setups need updating to check
the exact blender version:

``` Python
modifier.use_multi_modifier = True
if bpy.app.version_file < (3, 5, 8):  # Blender bug T103074
    modifier.invert_vertex_group = True
```

### Data Transer

For `bpy.ops.object.datalayout_transfer operator`, `VCOL` has been
split into `COLOR_VERTEX` and `COLOR_CORNER` for different color
domains
(blender/blender@93d84e87b26c,
blender/blender@eae36be372a6).

## Internal Mesh Format

The mesh data structure refactoring from earlier releases has continued
in 3.5. See the similar section in the [3.4 release
notes](../3.4/python_api.md#internal-mesh-format).

- The active and default (for render) color attributes are now stored as
  strings, making it easier to change them and access the correct
  attributes
  (blender/blender@6514bb05ea5a).
- Mesh vertex positions are now accessible via the builtin `position`
  attribute
  (blender/blender@1af62cb3bf46).
- UV layers are now stored as generic 2D vector attributes on the face
  corner domain
  (blender/blender@6c774feba2c9)
- The `MeshUVLoop` Python type is deprecated and will be removed in
  4.0. Its `data` collection member is also deprecated, replaced with
  separate collection members named `uv`, `vertex_selection`,
  `edge_selection`, and `pin`. Accessing the `data` member is
  emulated for backwards compatibility for now, but the emulation comes
  with a performance penalty
  (blender/blender@a82c12ae32d9).
- The sharp edge property is now stored as a generic attribute,
  accessible with the `sharp_edge` name
  (blender/blender@dd9e1eded0d4).
- Loose edge status is stored separately internally
  (blender/blender@1ea169d90e39).
  - The `MeshEdge.is_loose` property is no longer editable.
  - Loose edge status can be recalculated with
    `Mesh.update(calc_edges_loose=True)`
- Data access is generally faster than before when accessed with the
  attribute API (i.e. `mesh.attributes["sharp_edge"]`), but slightly
  slower than before when accessed with the old properties.

## Bundled Libraries

Python bindings for the following libraries are now bundled with
Blender, and available to use by add-ons.

- [USD](https://graphics.pixar.com/usd/release/index.html)
- [OpenVDB](https://www.openvdb.org/documentation/doxygen/python.html)
- [OpenImageIO](https://openimageio.readthedocs.io/en/latest/pythonbindings.html)
- [OpenColorIO](https://opencolorio.readthedocs.io/en/latest/)
- [MaterialX](https://materialx.org/docs/api/index.html)

## Other Changes

- The users site-packages are now available by default without having to
  use `--python-use-system-env`
  (blender/blender@72c012ab4a3d2a7f7f59334f4912402338c82e3c)
- New `ModifierData.execution_time` property that can be used for
  profiling and optimization. It's not exposed in the UI yet
  (blender/blender@8adebaeb7c3c663ec775fda239fdfe5ddb654b06).
- New `object.modifiers.move()` method for reordering modifiers
  without using operators.
  (blender/blender@f7dd7d545472)
- The active catalog of Asset Browsers can be changed via
  `bpy.types.FileAssetSelectParams.catalog_id`, it's no longer
  read-only.
  (blender/blender@80249ce6e4f9)
- Custom node tree now have a default name set to `NodeTree.bl_label`.
  (blender/blender@59ce7bf)

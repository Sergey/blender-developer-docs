# Nodes & Physics

## Geometry Nodes

### General

- A new **Image Info** node allows retrieving various information about
  an image
  (blender/blender@efcd587bc28d).
- There is a new **Image** input node
  (blender/blender@a3251e66a723).
- The *Named Attribute* input node now has an "Exists" output to tell
  whether the attribute exists
  (blender/blender@0d3a33e45eb3).
- A new **Blur Attribute** nodes allows mixing attribute values of
  neighboring elements
  (blender/blender@d68c47ff347bbb3824).
- The *Store Named Attribute* node can now store 2D vector attributes
  (blender/blender@f0dc4d67e56dbdd13).
- The *Image Texture* node has a new mirror extension type
  (blender/blender@a501a2dbff797829).
- Field utility nodes have been renamed
  (blender/blender@4961e5f91d98).
  - *Interpolate Domain* -\> *Evaluate on Domain*
  - *Field at Index* -\> *Evaluate at Index*
- The modifier user interface has been improved in various ways
  - Exposed properties from node groups no longer have hard min and max
    values
    (blender/blender@cb92ff7b2d50).
  - Checkboxes are used for exposed boolean sockets
    (blender/blender@2ea47e0def6a).
  - The "Use Attribute" toggles have moved to the right edge of the
    panel
    (blender/blender@68625431d5d0).

![The technology for checkboxes was finally invented! ](../../images/Boolean_inputs_for_the_modifier.png){style="width:500px;"}

- Drag and drop is supported for geometry node group assets in the
  viewport
  (blender/blender@bfa7f9db0e797a1fc5d06aa0a38fbbfd046aac44).
- A new operator "Move to Nodes" creates a new node group wrapping the
  modifier's group
  (blender/blender@5ca65001ea9e90c4d).

### Curves

- New **Interpolate Curves** node allows generating child curves between
  a set of guides
  (blender/blender@85908e9edf3df).
- The *Trim Curves* node now has a selection input
  (blender/blender@11f6c65e61a2).
- Making simple procedural changes can be much faster in some cases, and
  non-topology changing sculpt brushes are slightly faster
  (blender/blender@7f958217ada2).

### Mesh

- The new **Edges to Face Groups** node finds groups of faces surrounded
  by selected edges
  (blender/blender@50dfd5f501d3e07915d1e9ad8656a52b3902b6ab)
- The mesh primitive nodes now output a UV map (previously that was
  stored as named attribute with a hard-coded name
  (blender/blender@f879c20f72d9ce58a2e0).
- The *Split Edges* node is over two times faster
  (blender/blender@e83f46ea7630).
- For mesh objects, applying modifier will now give an error message if
  the geometry it creates doesn't contain a mesh
  (blender/blender@b1494bcea7b6).

### Instances

- Caching of geometry bounds can make viewport display of many geometry
  instances 2-3x faster
  (blender/blender@e8f4010611e7).

## Node Editor

### User Interface

- The context menu has been significantly improved, giving quick access
  to more features
  (blender/blender@2c096f17a690).
- Automatically attaching nodes after adding or moving them can be
  disabled by holding `Alt`
  (blender/blender@ae886596a0f1).
- For Copy & Paste, nodes are placed at the mouse position rather than
  their original locations
  (blender/blender@7355d64f2be5).
- The Geometry Nodes add menu is reorganized to make it easier to
  explore
  (blender/blender@d4e638baac43,
  blender/blender@789e549dbae8).

<video src="../../../videos/2_geonodes_menu_after.mp4" width="1000" controls=""></video>

- Node links can be swapped between sockets by holding `Alt` while
  connecting them
  (blender/blender@89aae4ac82e0).
  This replaces the auto-swapping behavior.

<video src="../../../videos/node_link_swapping.mp4" title="Hold `Alt` to swap links." width="800" controls=""></video>

## Cloth Simulation

- Self-collision was optimized with a 25% overall fps gain on some
  collision-heavy tests
  (blender/blender@0796210c8df3,
  blender/blender@a3ac91da27dd,
  blender/blender@e1df731c91bc).

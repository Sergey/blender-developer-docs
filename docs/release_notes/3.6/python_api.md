# Python API & Text Editor

## Custom Script Directories

![](../../images/Release_notes_multiple_script_dirs_prefs.png "../../images/Release_notes_multiple_script_dirs_prefs.png")
Multiple script directory paths can now be configured in the Preferences
(*File Paths* section). Each of these directories supports the regular
script directory layout with a startup file (or files?), add-ons,
modules and presets.
(blender/blender@ba25023d22)

Compatibility notes:

- `bpy.types.PreferencesFilePaths.script_directory` is deprecated. The
  directories are exposed in `bpy.types.script_directories` now.
- `bpy.utils.script_path_pref` is removed, use
  `bpy.utils.script_paths_pref` instead.

  

## Blender Handlers

- Added `bpy.app.handlers` (`save_post_fail`, `load_post_fail`),
  so the `*_pre` handles will always call a `*_post` handler,
  even on failure
  (blender/blender@46be42f6b16314f59a37ebb430d77d12e7a88461).
- File load handlers (`save_{pre/post/post_fail}`,
  `load_{pre/post/post_fail}`) now accept a filepath argument so the
  file being loaded or saved is known
  (blender/blender@46be42f6b16314f59a37ebb430d77d12e7a88461)

## Internal Mesh Format

The mesh data structure refactoring from earlier releases has continued
in 3.6. See the similar sections in the
[3.4](../3.4/python_api.md#internal-mesh-format)
and
[3.5](../3.5/python_api.md#internal-mesh-format)
release notes.

- The vertex and edge indices stored for mesh face corners
  (`MeshLoop`/`MLoop`) are now stored as separate attributes, named
  `.corner_vert` and `.corner_edge`
  (blender/blender@16fbadde363c8074ec725fa1e1079add096bd741).
- Mesh faces (polygons) are now stored with a single integer internally,
  rather than the `MPoly` type.
  - This means that the order of faces is always consistent with the
    order of face corners (loops).
  - The `MeshPolygon.loop_total` property is no longer editable.
    Instead the size of each face should be changed with the next face's
    `loop_start` property.
- Mesh edges are now stored in a generic attribute named `.edge_verts`
  (blender/blender@2a4323c2f51f92fc1c88561cbf23004b42138ad2).
  - A new 2D integer vector attribute type is added to store edge data
    (blender/blender@988f23cec3912dac96595c652a5f4e427d7550c8).
- UV seams are now stored as a generic attribute, accessible with the
  `.uv_seam` name on the edge domain
  (blender/blender@cccf91ff832d119dbf048b0518a696b9aa83bce4).
- The smooth/sharp status for faces is now stored as a generic attribute
  with the `sharp_face` name
  (blender/blender@5876573e14f434a4cd8ae79c69afbe383111ced9).
  - In some cases, meshes are now **smooth by default** (when created
    from scratch, without `from_pydata`).
    - To simplify getting the previous behavior, new API functions
      `Mesh.shade_flat()` and `Mesh.shade_smooth()` have been added
      (blender/blender@ee352c968fd165fafd17adcc696e98bb0efa844a).

## Other Changes

- New `bpy_extras.node_utils.connect_sockets()` function to allow
  creating links between virtual sockets (grayed out sockets in Group
  Input and Group Output nodes)
  (blender/blender@81815681d0aeaae719c6cb736f0326201c87ba4a).
- New `action_tweak_storage` property in `AnimData`, which exposes
  the temporary storage used to stash the main action when tweaking an
  NLA clip
  (blender/blender@997ad50b4996fb410733acb53eb5c5e6caedbe61).

# Pipeline, Assets & I/O

## Assets

- It is now possible to use relative path when importing assets.
- The option exists in the Preferences per asset library
  (blender/blender@5d0595fded69).

![](../../images/Asset_library_relative_path.jpg)

## Stanford PLY

- New C++ based PLY importer and exporter
  (blender/blender@43e9c900,
  blender/blender@6c6edaf5,
  blender/blender@48496f14).
  About 4x-20x faster export, 8x-30x faster import compared to the
  Python addon. It also fixes and improves some things:
  - Importing point clouds with vertex colors now works
  - Importing PLY files with non standard line endings
  - Exporting multiple objects (previous exporter didn't take the vertex
    indices into account)
  - The importer has the option to merge vertices
  - The exporter supports exporting loose edges and vertices along with
    UV map data
  - Vertex colors can be treated as either sRGB or Linear color space.
  - Importer supports models that use "tristrips" element instead of
    "face".

## USD

- `Path Mask` import option now supports multiple primitive paths
  (blender/blender@239af1705d).
- Author `opacityThreshold` `USD Preview Surface` material attribute
  when exporting USD
  (blender/blender@3c74575dac).
- Fixed incorrect texture alpha mapping when exporting `USD Preview Surface` materials
  (blender/blender@109c1b92cd).
- New `Prim Path` USD export option to add a root transform primitive
  to the stage
  (blender/blender@42342124f8).
- Set `emissiveColor` `USD Preview Surface` material input when
  exporting USD
  (blender/blender@ce83b26a33).
- Fixed bug creating duplicate shader nodes when importing USD
  (blender/blender@74b5e62d2ae).
- New Curves/Hair Export Support
  (blender/blender@1164976dd8).

## FBX

- Geometry export performance was made faster by using Python's numpy
  (blender/blender-addons@994c4d91,
  blender/blender-addons@6054d1be,
  blender/blender-addons@aa7d1d71,
  blender/blender-addons@4269b4ad,
  blender/blender-addons@d1556507,
  blender/blender-addons@3e783620,
  blender/blender-addons@58740ec8,
  blender/blender-addons@2a0791a4).
  E.g. exporting Snow (without any animation) is about twice as fast as
  before.
- Geometry import performance was made faster by using Python's numpy
  (blender/blender-addons@47da0ad56b,
  blender/blender-addons@5da3d41c27,
  blender/blender-addons@66390ced12,
  blender/blender-addons@9859e253b5,
  blender/blender-addons@2438ae5c27).
- It is now possible to export the active color layer in first position,
  in case other software only import one of these
  (blender/blender-addons@4c397ede).

## glTF 2.0

### Import

- Fix zero sum weights for skinning
  (blender/blender-addons@83357ab3b923a9b5bfe343959fe61bf2bb726f80)
- Fix empty shapekey names
  (blender/blender-addons@6ac724926a7508955fe9ee879fcd870518660e6e)
- Fix custom attribute import when vertices are merged or shared
  accessors
  (blender/blender-addons@2403f4c6b90c64c894ab1d210b73ef6fea93e318,
  blender/blender-addons@45bfb99ffe4ab5bbc78c0464011d84eed1e37c0b)

### Export

- Big animation refactoring
  (blender/blender-addons@8dfe73800d5cb6cb27e2c078c4e5f4e27f684034)
- Export right materials when changed by modifiers
  (blender/blender-addons@8b57a74629333852f3f7cbbafee63d747ff0c989)
- Fix normal normalization
  (blender/blender-addons@0920b3e329073ed09ec1e28995300a9633840bf5)
- Reset sk values when switching sk action
  (blender/blender-addons@0086ce9d67048900bcc09e05202bd574961d7b85)
- Add hook to change primitive attribute order
  (blender/blender-addons@874240f2753eaad4a2b2105bba120d395dde6f27)
- Fix uri encoded, only for uri, not filename
  (blender/blender-addons@8c776517997c9ef07d3d4dc2ad87e7402d1f2677)
- Convert light option was not saved
  (blender/blender-addons@17f1e4d8481cd7ce1d35aad9654308f535fca4f0)
- Avoid crash when sequence image (not managed by glTF)
  (blender/blender-addons@789a200cd7eaf7247b83bc91fffe402385d9a90f)
- Fix exporting children of instance collections
  (blender/blender-addons@932ea1d71928ddba2d026a3b32a19f5d56239808)
- Avoid crash when collision name in attributes
  (blender/blender-addons@e2b556cbe8dfbe279fb9ef3ae41591709213e942)
- Initialize filter when user saved export preferences
  (blender/blender-addons@776b4a3a3f4119108489c852e231479ff89227ec)
- Add a hook for node name
  (blender/blender-addons@f12999046e9823cff9ea88fbc12fdbf20ee4bf3d)

# Blender 2.82: Textures

## UDIM

Blender now supports UDIM textures, a standard that allows to spread a
single texture over multiple files, each with its own UV area
(blender/blender@c30d6571bb).

- UDIM Textures can be created, opened and saved just like regular
  images.
- UDIM Tiles can be added and removed from textures.
- The Image and UV editors support displaying all tiles at once.
- Eevee and Cycles support rendering UDIM textures.
- Painting onto UDIM textures is supported, both in the Image Editor as
  well as in the 3D View.

When using UDIM it could happen that in certain cases the images on the
GPU aren't updated directly. This is mostly noticeable when that the
textures render differently when zooming in/out. You can work around
this issue by enabling the `Limit Size` option in `Preferences / Viewport / Textures`.

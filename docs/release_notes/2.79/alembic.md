# Blender 2.79: Alembic

The Alembic import & export has been improved, both in stability and
compatibility (revisions
blender/blender@56cfd3d5a7d..blender/blender@c684fab8a6ea)

#### Fixes

- Fixed errors in matrix conversion (most Alembic files use Y=up, while
  Blender uses the more sensible Z=up).
- Re-implemented part of the importer, so that it has to guess less,
  improving compatibility with non-Blender generated files (fixes
  [T50403](https://developer.blender.org/T50403))
- The importer is now more lenient to unknown types.
- Improved performance by removing some unnecessary computations.
- Prevented crash when cancelling import
  (blender/blender@3748ca432d2
  &
  blender/blender@43a910abce5).
- Fixed "flat" import (i.e. all objects in world coordinates, no
  parenting)
  (blender/blender@642728b3395a)
- Fixed [T50227](https://developer.blender.org/T50227): Alembic uv
  export/load issue
  (blender/blender@699a3e2498)
- Added simple unit tests
  (blender/blender@6af131fa5)
- Greatly improved performance of curve/hair import
  (blender/blender@e12c3110024dc)
- Changing cache modifier path no longer discards object paths
  (blender/blender@9c02990ac13a)
- Meshes with animation data are now saved on every frame
  (blender/blender@6715bfee92e72d,
  [T51351](https://developer.blender.org/T51351)). This allows you to
  change a mesh procedurally, for example using a Python script, and
  have it exported every frame, simply by ensuring the mesh has some
  animation data on it.
- Support for sub-frame sampling
  (blender/blender@35f4abcf53dad
  and
  blender/blender@24a0b332e2a72).
- Fixed missing material assignment when an animated mesh changed vertex
  count
  (blender/blender@45d7513f84bd3f).
- Importing now takes the "inherit transform" flag into account
  correctly when constructing parent-child relationships
  (blender/blender@32edfd53d978b34
  and [T52022](https://developer.blender.org/T52022)).
- Sequences of Alembic files are now handled properly
  (blender/blender@0900914d9644
  and [T51820](https://developer.blender.org/T51820)).

#### New Features

- Exporting of linked-in dupli-groups is now possible. This supports the
  common character animation workflow used at the Blender Institute,
  where a character, its rig, and the custom bone shapes are all part of
  a group. This group is then linked into the scene, the rig is
  proxified and animated. Such a group can now be exported. Use
  "Renderable objects only" in the export options to prevent writing the
  custom bone shapes to the Alembic file.
  (blender/blender@ebb30451140,
  [T50688](https://developer.blender.org/T50688))
- Exporting hair & particle systems is now optional. The export still
  takes more time with disabled hair than when there is no hair, as the
  particle systems themselves aren't disabled during the export. It's
  only the writing to the Alembic file that's skipped.
  (blender/blender@b148ac5cf77a869bc)
- The Import and Export operators now take a new flag
  `as_background_job`, which is `True` by default. When set to `False`,
  the operator will block Blender during the import. This allows Blender
  scripts to perform tasks after the import/export has completed.
  (blender/blender@2dac8b3ee043)
- Empties can now also be exported to Alembic (both static and animated)
  (blender/blender@9b3e3d4defc66).
- Exporting simple child hairs is now possible.
  (blender/blender@6ed15c5a411)
- Added support for face-varying vertex colors (read: compatibility with Houdini)
  (blender/blender@7b25ffb618dd,
  [T51534](https://developer.blender.org/T51534))
- Support for empty meshes (which can get vertices later in their
  animation)
  (blender/blender@6715bfee92e72d,
  [T51351](https://developer.blender.org/T51351))
- Loading an Alembic file stored in the obsolete HDF5 format will now
  give an error message telling you the format is not supported in
  Blender
  (blender/blender@9dadd5ff937,
  [T51292](https://developer.blender.org/T51292)). Such files can be
  converted using
  [AbcConvert](https://github.com/alembic/alembic/tree/master/bin/AbcConvert).
  The current format, Ogawa, is [4x to 25x faster than
  HDF5](http://exocortex.com/blog/alembic_is_about_to_get_really_fast),
  and was [introduced almost four years
  ago](https://groups.google.com/forum/#!topic/alembic-discussion/FTG1HuuO_qA).

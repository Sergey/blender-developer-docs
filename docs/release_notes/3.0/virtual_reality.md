# Blender 3.0: Virtual Reality

## Controller Support

Blender 3.0 offers a brand new set of VR controller-based functionality,
including the ability to visualize controllers (VR, regular 3D viewport)
and the ability to navigate (teleport/fly/grab) one's way through a
scene in VR using controller inputs. This functionality is available via
the updated [VR Scene Inspection add-on](https://docs.blender.org/manual/en/3.0/addons/3d_view/vr_scene_inspection.html).

<figure>
<video src="../../../videos/Blender_3.0_virtual_reality.mp4" width="800" height="430" controls=""></video>
<figcaption>Classroom by Christophe Seux</figcaption>
</figure>

![Race Spaceship by Alessandro Chiffi / ONdata Studio](../../images/Xr_controller_support.png){style="width:800px;"}

## Other Changes & Additions

- Session uses stage reference space (user-defined tracking bounds)
  instead of local reference space (position at application launch),
  when available
  (blender/blender@36c0649d32aa).
- New "Absolute Tracking" session option which skips eye offsets that
  are normally added for placing users exactly at landmarks. This allows
  the tracking origin to be defined independently of the headset
  position
  (blender/blender@36c0649d32aa).

![](../../images/Xr_absolute_tracking.png)

- New `Custom Object` type for VR landmarks (replaces old `Custom Camera`),
  which enables any object to be used as a base pose
  reference for the VR viewer. In addition, a viewer reference scale can
  now be set for landmarks of type `Custom Object` or `Custom Pose`
  (blender/blender-addons@823910c50d1c).

![](../../images/Xr_landmark.png)

- Higher color depth for VR-displayed images when supported by runtime
  (blender/blender@eb278f5e12cf).
- Support for Varjo quad-view and foveated headsets (Varjo VR-3, XR-3)
  (blender/blender@07c6af413617).

## Fixes

- Fixed pink screen issue on Windows when using the SteamVR runtime with
  AMD graphics cards
  (blender/blender@82ab2c167844).
- Fixed render artifacts (geometry occluded for one eye) when using VR
  with EEVEE and viewport denoising
  (blender/blender@c41b93bda532).

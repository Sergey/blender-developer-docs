# Blender 2.80: Add-ons

Most add-ons for 2.79 are available for 2.80 as well. A few new ones
were added, and a few were removed.

## BlenderKit

This new addon provides access to an Online Library, into which all
users can contribute.

- Access Materials, Models, Brushes and more with this exciting new
  addon.
- Search, download, upload, and rate directly from Blender.
- You can find the addon in the Addons Add Mesh Category.
- Once activated, the panel is in the 3d View \> Sidebar \> BlenderKit
  tab.
- More info about the project can be found on
  [www.blenderkit.com](https://www.blenderkit.com).

![BlenderKit interface showing material preview](../../images/BlenderKit_interface.jpg){style="width:600px;"}
![Drag-drop of a material from the BlenderKit online library.](../../images/BlenderKit_drag_drop.gif){style="width:600px;"}

## Object Scatter

The old "Grease Scatter Objects" addon has been removed. There is a new
"Object Scatter" addon as replacement. There is a [short
video](https://www.youtube.com/watch?v=CTbiJiCoeu0) that shows how the
addon works.

To use the addon, it has to be activated. Then you have to select the
objects that should be scattered. Lastly you have to select the object
to scatter onto (this one has to the active object). Then activate the
"Scatter Objects" operator using the search and start to draw strokes.
When ESC is pressed, the operator is cancelled. When ENTER is pressed,
new objects are created using the dupli system of Blender.

## Bone Selection Sets v. 2.1.0

New features (implemented in
[D3049](https://developer.blender.org/D3049)):

- Shift+Alt+W shows a popup menu with the currently available selection
  sets. This allows animators to quickly use the selection sets from
  within the 3D View. This feature was merged from [Sybren's personal
  branch](https://github.com/sybrenstuvel/random-blender-addons/blob/a806ba33e7f13f4c664a8557e8f1dc02b3f70e98/selection_set_selector.py).
- Copy & Paste of selection sets. The copied Selection Set is stored as
  JSON document on the clipboard, like
  `{"name": "SelectionSet", "bones": ["Bone", "Bone.001"]}`. Using JSON
  allows the animator to copy/paste selection sets through text-based
  communication channels (IRC, email, bug reports, etc.). When pasting,
  a new Selection Set is always made, regardless of whether one with the
  same name already exists. In such a case a suffix like `.001` is
  added to the name.

## Rigify

Rigify now supports installing custom rig packages from zip files, with
associated API and utility additions to make coding custom rigs easier
([D4364](https://developer.blender.org/D4364)).

## Tri-Lighting

This new addon is from the advanced objects addon from 2.79.

- It adds a basic 3 point light set with Key, Fill and Back Lights. The
  lights created are pointed to the active/selected object.
- You can find the addon in the Addons Lighting Category.
- Once activated, the menu is in the 3D View \> Add \> Lights \> 3 Point
  Lighting.

## Align Tools

This new addon is from addons contrib and nightly builds.

- Features: Align Location, Rotation and Scale in Object Mode
- Works with Multiple Objects.
- Advanced Align Operator with extended align options.
- You can find the addon in the Addons 3D View Category.
- Once activated, the panel is in the 3d View \> Sidebar \> Item Tab \>
  Align Tools.

![Align tools interface](../../images/Addons_RN28_align_tools.jpg){style="width:600px;"}

## Simplify Curves+

This addon has been updated with new features.

- New "Merge by Distance"
- Works in Curve Edit mode, similar to the edit mesh "Merge by Distance"
- Useful for cleaning up large messy imported curves
- You can find the addon in the Addons Add Curve Category.
- Once activated, the new option is in the 3d View \> Curve Edit Mode \>
  Curve Context menu (right click) \> Merge by Distance and Simplify
  Curves.

## Add Mesh

Changes were made to the Add Mesh Extra Objects Addon.

- Discombobulator is now a Standalone addon again.

![Discombobulator interface](../../images/Addons_Discombob.jpg){style="width:600px;"}

- Geodesic Domes is also a standalone addon again.
- These long standing addons both go above the normal "Add Mesh" object
  types by providing complex functions and methods.
- You can find the addons in the Addons Add Mesh Category.
- Once activated, the menu is in the 3d View \> Add Menu

## Atomic Blender .pdb/.xyz Import

This addon was updated for 2.8.

- New Optional Utilities Panel. You can activate the panel in the Addons
  Preferences.
- Import a .pdb molecule file and use the utilities panel to change
  object types and more.
- You can find the addon in the Addons Import Export Category.
- Once activated, the menu is in the 3d View \> File \> Import menu.
- The Utilities panel is in the Sidebar \> Create tab.

## Edit Mesh Tools

This addon was updated for 2.8.

- This addon has been streamlined with many 2.79 features removed.
- Focus is on Fillets, Extrudes and Mesh component manipulation.
- Some built in tools are in the menu's coupled with tools the addon
  provides.
- You can find the addon in the Addons Mesh Category.
- Once activated, the menu is in the 3d View \> Edit Mode \> Context
  Menu (right click).
- The main panel is in the Sidebar \> Edit tab.

## Mesh F2

This addon was updated for 2.8.

- Thanks to 1D_Inc for updating this addon.
- Now matching his studio tool F2 has new workflow options in the Addons
  Preferences.
- A solid update ensuring the continuation of this great tool.
- Primary use is fast face creation in edit mode.
- You can find the addon in the Addons Mesh Category.
- Once activated, The addon is in use via the F key in mesh edit mode.

## Inset Straight Skeleton

In 2.79 this was Inset Poygon addon. The addon was changed for 2.8.

- Now using the Straight Skeleton method for insets.
- You can find the addon in the Addons Mesh Category.
- Once activated, the menu is in the 3d View \> Search.

## Mesh Tissue

This addon has been updated to 2.8 and new features added.

- Focusing on Advanced Computational Design, Alessandro Zomparelli
  updates the old tools and adds some new.
- Keep in mind this is an advanced tool, and as such may take
  computational time if used on large vert counts.
- You can find the addon in the Addons Mesh Category.
- Once activated, the menu is in the Sidebar \> Tissue Tab.
- After Tessellation, there is also a panel in the Properties Editor \>
  Object Data Tab
- Tools for Tessellation, Weights, Vert Groups and more.

## Node Wrangler

This addon has been updated to 2.8 and some conflicting hotkeys changed.

- Node Wrangler: Fix T65202: keymap conflicts
- Since CTRL+LMB is now taken by 'cut links' in blenders default
  keymap,this was conflicting with nodewranglers 'lazy connect' and
  unfortunately we have to make changes here.
- Changed shortcuts:
  - lazy connect (CTRL+LMB --\> ALT+RMB)
  - lazy connect with menu (CTRL+SHIFT+LMB --\> ALT+SHIFT+RMB)
  - lazy mix (ALT+RMB --\> CTRL+SHIFT+RMB)
- This keeps the two 'lazy Connect' on a common "base" key and just
  moves 'lazy mix'

## Object Assign Shape keys

This addon is new to 2.8.

- This addon easily allows you to morph Curves or Text using shape keys.
- You can find the addon in the Addons Object Category.
- Once activated, the panel is in the 3d View \> Sidebar \> Tool Tab

## Addons Removals

Many Addons have been removed from Blender 2.8 that were in 2.79

- As the 2.7 series closed to an end, we added many addons to "Showcase"
  the end of an era.
- In 2.8, the primary focus was on addons with an active
  developer/maintainer and the sub task of bundling less addons in
  Blender 2.8.
- It's very important that each addon in Blender has an active developer
  to work on and maintain when needed.
- Without active developers, many addons were broken and removed into
  nightly builds "addons_contrib" experimental section.
- They have an oppertunity to be either fixed by their active developer
  or removed from nightly builds before 2.81.
- The removal of addons is documented here:
  <https://developer.blender.org/T63750>
- If your missing your favorite addon I've found they are often updated
  by external developers or small studios that use them. You can often
  find these updates on the forums.
- The addons team is small and mostly volunteer based, we hope you
  understand we could not fix everything.

## Special Thanks

Thanks to the Addons Developers and the Addons Community for making this
release possible.

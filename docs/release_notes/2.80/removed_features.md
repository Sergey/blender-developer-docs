# Blender 2.80: Removed Features

A number of features that were no longer under active development or did
not fit in the new design were removed. By removing the maintenance
burden, developers can spend more time on new features and redesign the
user interface and implementation to be more optimized.

- Blender Internal was removed, replaced by Eevee and Cycles renderers.
- The Blender Game Engine was removed. We recommend using more powerful,
  open source alternatives like [Godot
  Engine](https://godotengine.org/).
- Dupliframes and slow parent were removed, as these are incompatible
  with the new dependency graph and never worked reliably in the old
  one.

# Blender 4.5 LTS: Grease Pencil

# Operators

 * Added two new options "Mask with Layer Above" and "Mask with Layer Below" in the "Extras" menu in the layers panel. Used to easily add masking layers from above or below to the active layer. (blender/blender@f702eba53f0f6a0cb5bbd2587974abd63bc85ceb)

# Python API

 * The `bpy.types` `GreasePencilLayer` and `GreasePencilLayerGroup` are now subclasses of a new `bpy.types.GreasePencilTreeNode`. Common properties between layers and groups are now part of this base class. This also means that layer groups now have the `channel_color` and `select` property. (blender/blender@3ef2ee7c53a36517096f3b03684abdf4640f0dd2)
 * The new `GreasePencilTreeNode` type now has two new properties (`next_node` and `prev_node`) to get the next/previous nodes at the same level in the tree. The next node is the node "above" in the tree and the previous node is the one "below". If the node is at the very top of its group, the next node is `None`. When it's at the very bottom, the previous node is `None`. (blender/blender@4229d7fa655aa8e9aca54f71c24e38a358b4e54c)
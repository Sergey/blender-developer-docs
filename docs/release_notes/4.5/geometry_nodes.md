# Blender 4.5 LTS: Geometry Nodes

### Performance
* Vertex to edge attribute domain interpolation is roughly 1.7x faster (blender/blender@33db2d372fc5).
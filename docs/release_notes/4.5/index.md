# Blender 4.5 LTS Release Notes

Blender 4.5 LTS is currently in **Alpha** until June 4,
2025.
[See schedule](https://projects.blender.org/blender/blender/milestone/25).

Under development in [`main`](https://projects.blender.org/blender/blender/src/branch/main).

* [Animation & Rigging](animation_rigging.md)
* [Compositor](compositor.md)
* [Core](core.md)
* [Cycles](cycles.md)
* [EEVEE & Viewport](eevee.md)
* [Geometry Nodes](geometry_nodes.md)
* [Grease Pencil](grease_pencil.md)
* [Modeling & UV](modeling.md)
* [Pipeline, Assets & I/O](pipeline_assets_io.md)
* [Python API & Text Editor](python_api.md)
* [Rendering](rendering.md)
* [Sculpt, Paint, Texture](sculpt.md)
* [User Interface](user_interface.md)
* [Video Sequencer](sequencer.md)

## Compatibility

* [Intel macOS support is deprecated.](https://devtalk.blender.org/t/deprecation-and-removal-of-macos-intel-builds-in-blender-5-0/38835)
   * Due to the high maintance cost of tracking down and fixing graphics realted issues specific to Intel and AMD GPU
     Macs, it was decided that this will be the last release with an official Intel macOS build of Blender.
* Big Endian platform & blendfiles [is deprecated](https://devtalk.blender.org/t/big-endian-support-deprecation-removal/39098) and will be removed in Blender 5.0.
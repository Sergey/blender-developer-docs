# Blender 4.5 LTS: Sequencer

## Other
- Over Drop effect and blend mode was removed (it was doing exactly the same thing as regular Alpha Over since 2006).
  Usage of this effect in existing files is now changed to Alpha Over.
  (blender/blender!134342)

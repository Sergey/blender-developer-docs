# Blender 4.5 LTS: Sculpt, Paint, Texture

* Node tools performance for simpler changes (when only positions, masks, or face sets were changed) has been improved significantly (blender/blender@0b891a68b101).
# Blender Release Notes

What's new in each Blender version.
Visit [blender.org](https://www.blender.org/download/releases/) for the complete list.

Every Blender release is supported until its successor, approximately
every [four months](../handbook/release_process/release_cycle.md).
Once a year, a [Long-term Support](https://code.blender.org/2020/02/release-planning-2020-2025/) (LTS)
version is released and maintained for two years.

## Currently Active

These versions are actively maintained or under development.

- [Blender 4.5 LTS](4.5/index.md) *(alpha)*
- [Blender 4.4](4.4/index.md) *(beta)*
- **[Blender 4.3](4.3/index.md)** *(current stable release - November 19, 2024)*
- [Blender 4.2 LTS](4.2/index.md) *(July 16, 2024, supported until July 2026)*
- [Blender 3.6 LTS](3.6/index.md) *(June 27, 2023 supported until June 2025)*

## Previous Versions

While no longer supported, these versions can still be
[downloaded](https://www.blender.org/download/previous-versions/) and used on older setups.
See the list of [requirements](https://www.blender.org/download/requirements/#old) for past versions.

- [Blender 4.1](4.1/index.md) *- March 26, 2024*
- [Blender 4.0](4.0/index.md) *- November 14, 2023*

- [Blender 3.5](3.5/index.md) *- March 29, 2023*
- [Blender 3.4](3.4/index.md) *- December 7, 2022*
- [Blender 3.3 LTS](3.3/index.md) *- September 7, 2022, supported until September 2024*
- [Blender 3.2](3.2/index.md) *- June 8, 2022*
- [Blender 3.1](3.1/index.md) *- March 9, 2022*
- [Blender 3.0](3.0/index.md) *- December 3, 2021*

- [Blender 2.93 LTS](2.93/index.md) *- June 2, 2021, supported until June 2023*
- [Blender 2.92](2.92/index.md) *- February 25, 2021*
- [Blender 2.91](2.91/index.md) *- November 25, 2020*
- [Blender 2.90](2.90/index.md) *- August 31, 2020*

- [Blender 2.83 LTS](2.83/index.md) *- June 3, 2020, supported until June 2022*
- [Blender 2.82](2.82/index.md) *- February 14, 2020*
- [Blender 2.81](2.81/index.md) *- November 21, 2019*
- [Blender 2.80](2.80/index.md) *- July 30, 2019*

- [Blender 2.79](2.79/index.md) *- September 12, 2017*
- [Release Notes Archive](https://archive.blender.org/wiki/2015/index.php/Dev:Ref/Release_Notes/)

## Compatibility Changes

- [Compatibility Changes](compatibility/index.md)

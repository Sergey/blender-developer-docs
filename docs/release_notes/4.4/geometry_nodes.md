# Blender 4.4: Geometry Nodes

## General

* The "Limit Surface" option from the modifier is available in the *Subdivision Surface* node
  (blender/blender@7074daed047a, blender/blender-manual@ea32abe7e9b7).
* The *Normal* input node now outputs proper face corner normals instead of just face normals (blender/blender@b36eb6903879).
  * For existing files, the old behavior turned on automatically with a boolean property in the sidebar.
* **Collection** and **Object** input nodes have been added
  (blender/blender@65b1ab43bf9f, blender/blender-manual@42cee13fc3c2).
* The **Find in String** node has been added (blender/blender@1098f434b091) .
* The *Join Geometry* and *Realize Instances* nodes now preserve the vertex group status of input attributes (blender/blender@84c7684871d7).
* The text overlay in the 3D view works for matrix attributes now (blender/blender@95ef33059c39b9).

## Performance

* Porting the triangulate node from BMesh to Mesh gave a over a 30-100x performance improvement
  (blender/blender@ea875f6f3278).
* The *Sort Elements* node is 50% faster in common scenarios (blender/blender@9d13e39585ee).

## Node Editor

* The *Warning* node now has a dynamic label depending on the selected type
  (blender/blender@6e9d988018ef).
* Snapping is supported when resizing nodes horizontally
  (blender/blender@4de34c8205a4).

# Blender 4.4: Animation & Rigging

## Slotted Actions

![The icon for Action Slots](images/action_slot.svg){style="width:10rem;"}

The structure of [Actions](https://docs.blender.org/manual/en/4.4/animation/actions.html#actions)
has changed, introducing the concept of
[Slots](https://docs.blender.org/manual/en/4.4/animation/actions.html#action-slots)
(blender/blender@43d7558e5b00a14b39e473aec345e5080a5e76d2).
In short, **slots make it possible to store the animation of multiple "things" in the same Action**.

![Action editor, showing one Action with two Slots,
one for the Camera Object and one for the Camera Data.
The slots are distinguished by the icon on the right-hand side of the channel list.
The camera is both moving and zooming in.](images/anim_slotted_actions_action_editor.png)

### Example Uses

- Mesh objects: location in the scene (object), together with shape key values
  (belongs to the object data), and the locations of hook empties (different objects).
- Camera objects: transform (object) and lens parameters (object data).
- Meta-ball transforms, where all objects in a group of meta-balls can share the same Action.
- Materials, which effectively consist of two data-blocks
  (the Material itself, and its shader node tree).


### Details

- All Actions are 'slotted' now. Actions in old blend files are upgraded to slotted Actions.
- Creating an Action by keying (via the UI, operators, or the `rna_struct.keyframe_insert` function)
  will try and share Actions between related data-blocks. ([Details](https://projects.blender.org/blender/blender/pulls/126655))
- Assigning an Action to a data-block will attempt to auto-assign a suitable Action Slot when
  appropriate. There are cases where this does _not_ automatically assign a slot, and thus the
  Action will effectively _not_ animate the data-block. See below for details.

### Action & Slot data-block types

Actions in Blender 4.3 and older were tied to specific data-block types. An Action that animated an
Object could not be assigned to a Material, and vice versa. This is quite useful, as it shortens the
available choices in the Action selector to actually-compatible ones.

Now that a single Action can animate multiple data-blocks at the same time, this restriction has
been moved from the Action as a whole, to each individual slot. Any Action can be assigned to any
data-block. Its slots are bound to specific types, so like the example above, a slot that animates a
Material cannot be used by an Object, and vice versa.

### Auto-Assignment Rules

Effort has been spent to make Action selection work both reliably for Blender users as well as
keep the behavior the same for Python scripts. Where these two goals did not converge, reliability
and understandability for users was prioritized.

Auto-selection of the Action Slot upon assigning the Action works as follows. The first rule to find
a slot wins.

1. The data-block remembers the slot name that was last assigned. If the newly assigned Action has a
    slot with that name, it is chosen.
2. If the Action has a slot with the same name as the data-block, it is chosen.
3. If the Action has only one slot, and it has never been assigned to anything, it is chosen.
4. If the Action is assigned to an NLA strip or an Action constraint, and the Action has a single
    slot, and that slot has a suitable ID type, it is chosen.

For regular Action assignments (contrary to NLA strips and Action constraints), this last rule ("*if
the Action has a single slot, just use it*") does not apply. This means a subtle backward
incompatibility: with legacy Actions in Blender 4.3 the final state ("*it is animated by this
Action*") may differ from the final state with slotted Actions ("*if no slot was auto-assigned, it is not
animated*"). This is intentionally done to support the following workflow:

- Create an Action by animating Cube.
- In order to animate Suzanne with that same Action, assign the Action to Suzanne.
- Start keying Suzanne. This auto-creates and auto-assigns a new slot for Suzanne.

If rule 4. above would apply in this case, the 2nd step would automatically select the Cube slot for
Suzanne as well, which would immediately overwrite Suzanne's properties with the Cube animation.


### Working with Slotted Actions

**Merging Animation**

From the Action Editor it is possible to merge the animation of all selected objects into a single Action
(<span class="literal">Action</span> » <span class="literal">Merge Animation</span>).
The data is always merged into the Action of the active object. This functionality will visit related
datablocks of each selected object and merge those too. That means it is possible to merge e.g. Object
and Shapekey animation of even a single object. (blender/blender@34a18c7608, [Manual](https://docs.blender.org/manual/en/4.4/editors/dope_sheet/modes/action.html#action-menu))

**Separating Animation**

Also from the Action Editor it is possible to move Slots, selected in the Channels Region, into a new Action
(<span class="literal">Action</span> » <span class="literal">Move Slots to new Action</span>).
All Slots moved that way will be moved into a single Action. Users of those Slots will be
re-assigned to the new Action. (blender/blender@95b6cf0099, [Manual][separate-slots])

[separate-slots]: https://docs.blender.org/manual/en/4.4/editors/dope_sheet/modes/action.html#action-menu

It is also possible to separate each Slot into its own Action
(<span class="literal">Action</span> » <span class="literal">Separate Slots</span>).
As with moving specific Slots out of the Action,
this will re-assign all users of those Slots to the new Action.
(blender/blender@85e4bd19f6, [Manual](https://docs.blender.org/manual/en/4.4/editors/dope_sheet/modes/action.html#action-menu))


### Upgrading Old Projects to Slotted Actions

You don't need to do anything special to upgrade old projects. Blender will
automatically upgrade Actions from old blend files into Slotted Actions, and
this will work properly and preserve animation assignment even in multi-file
projects that link Actions between blend files, regardless of the order in which
you open and save the files.

- All upgraded Actions will have one slot, named "Legacy Slot".
- This slot will have the same type as the Action used to have. So when the Action animated a
  Material, the new slot will also be tied to the Material type.
- No Actions are merged by the upgrade. All Actions remain separate Actions.

### Forward Compatibility

Blender 4.4 will save slotted Actions in a way that is compatible with older versions of Blender.
**Blender 4.3 and older will only see the first slot**. If you then save the blend file the second
and subsequent slots, with all their animation data, will be lost. Also slot names will be lost.

If you have multi-slotted Actions and want to save them in a way that is compatible with older
Blenders, use the [Separate Slots][separate-slots] operator to split them up into multiple
single-slotted Actions.

### Python API

See the [Python API & Text Editor](python_api.md) for the technical changes to the Python API. This
section just describes some conceptual changes in the API.

- A new Python API for slots, layers, strips, and channelbags is introduced in Blender 4.4.
- The existing Python API for accessing F-Curves and Action Groups is still available, but now
  branded as "backward-compatible legacy API". This will operate on the F-Curves/Groups for the
  first slot only. The creation of an F-Curve or Group via this API will also create a slot named
  "Legacy Slot".
- The new data model also adds layers and strips, to supported layered animation in the future.
  However since this is not actually supported yet, there are the following restrictions:
  - An Action can have zero or one layer,
  - That layer can have zero or one strip,
  - That strip must be of type 'keyframe' and be infinite with zero offset.

  For more information on slotted Actions within the context of layered animation, see [Multi Data-Block Animation][techdocs-slotted].

[techdocs-slotted]: ../../features/animation/animation_system/layered.md#phase-1-multi-data-block-animation


## Pose Library

The Pose Library has been updated to enable a smoother workflow when working with Pose Assets (blender/blender@358a0479e8, [Manual](https://docs.blender.org/manual/en/4.4/animation/armatures/posing/editing/pose_library.html#pose-library)).

* The created pose will no longer be set as the current action.
* Pose Assets can be created to an asset library that is not the current file.
* It is possible to update Pose Assets from the current pose.
* Pose Assets can easily be deleted.
* Pose Assets can store the data for more than one armature using [Slots](https://docs.blender.org/manual/en/4.4/animation/actions.html#action-slots).

> **_NOTE:_**  Pose Assets created to an external library will be saved as one file per asset.
> These files cannot be edited by opening them in Blender. The provided operators need to be used instead.
> This is a known limitation and is tracked in
> [#133878: WIP: Support Editing External Blendfile Data using PartialWriteContext](https://projects.blender.org/blender/blender/issues/133878).
> Due to this, the operators will only work on assets that are either local to the blend file
> or have been created to an external library using the operator in Blender.

### Operators

**Create Pose Asset**

Creating a Pose Asset will show a Pop Up window in which the asset name, library and catalog can be specified.

This can now also be accessed from the 3D Viewport under
<span class="literal">Pose</span> » <span class="literal">Create Pose Asset</span>.

Running that operator while no Asset Library or Asset Shelf is visible will pop out the Asset Shelf
on the 3D Viewport from which the operator is run.
![The popup that appears when creating a Pose Asset. It allows specifying the asset name, library and catalog.](images/asset_lib_create_popup.jpg)

It is now also possible to create a Pose Asset containing the pose for more than one armature.
Simply go into Pose Mode with two or more armatures and create a Pose Asset with bones from both armatures selected.
This uses [Slots](https://docs.blender.org/manual/en/4.4/animation/actions.html#action-slots) under
the hood to determine which data should be used when applying an action.

> **_PYTHON:_**  For backwards compatibility, the operator is named the exact same as before (poselib.create_pose_asset).
> However the property "activate_new_action" is deprecated, no longer works and will be removed in Blender 5.0.

**Modify Pose Asset**

An asset can be modified in 4 ways

* Adjust: Updates existing channels in the asset, but doesn't add or remove any channels.
* Replace: Completely replace all channels in the Pose Asset with the channels of the current selection.
* Add: Adds the channels of selected bones to the Pose Asset. Existing channels will be updated.
* Remove: Remove chhanels of the current selection from the Pose Asset.

This can be found in the right click menu of a pose asset.

**Delete Pose Asset**

From the right click menu a Pose Asset can be deleted. This behaves differently between assets
that are local to the current blend file and in external libraries.
Local assets will just be unmarked as assets, while assets in external libraries will have their file deleted.


## Rigging

- Bone collection membership is now mirrored when symmetrizing an armature. For details
  [see the Manual](https://docs.blender.org/manual/en/latest/animation/armatures/bones/editing/symmetrize.html).
  (blender/blender@16c2e71ab0)


## Graph Editor

- The algorithm for generating the values of the F-Curve Noise Modifer has been updated. This was in response
  to a [bug report](https://projects.blender.org/blender/blender/issues/123875).
  Now the values don't exceed the -0.5/0.5 range anymore. Doing so added two new
  properties that control the look of the noise: Lacunarity and Roughness.
  The default values are chosen to closely resemble the previous behavior.
  There is a checkbox to use the old noise,
  which old files will automatically have enabled to not change the look of the scene.
  (blender/blender@2536ddac0d,
  [Manual](https://docs.blender.org/manual/en/latest/editors/graph_editor/fcurves/modifiers.html#noise-modifier))


## Constraints

- Relationship Lines for constraints (between the constrained object/bone and the constraint target) are no longer drawn when there is no target (blender/blender@266d1e8d2fdd2cc821fa5993d983c0893e5fc428). Previously these lines were drawn to the world origin when there was no target.

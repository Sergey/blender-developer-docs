# Blender 4.4: Modeling & UV

* The "Select Linked Pick" operator (`L` and `Shift-L`)
  is now supported in curves edit mode (blender/blender@c7cebf5f6d3e).
* An optimization to face corner normal calculation can give up to 15% faster playback
  in files with sharp edges or custom normals (blender/blender@59f9e93413d987d7424e095).

## Select Poles

Adds a new "Select by Trait" option to select all 3-poles, 5-poles, etc.
Given the impacts of 3 & 5-poles in topology, operator default is to
select all poles which do not have 4 edges to allow easy inspection.

(blender/blender@51eb5e2803c97eafa87d835130ec5d10896f9f44).


## New Topology Influence for Join Triangles

Joining triangles to quads can now prioritize quad dominant topology,
so joining triangles favors creating quads that form a "grid".

This can be controlled via a topology influence factor.

(blender/blender@dc57693b0c88b93847ec206f692bf51e0173839c).

# Blender 4.4: Core

## Environment Variables

- `BLENDER_SYSTEM_SCRIPTS` now supports multiple paths. Paths are separated by
   `;` on Windows and `:` on macOS and Linux. (blender/blender!132812)
- New `BLENDER_CUSTOM_SPLASH` to replace the splash screen with the given image file, and `BLENDER_CUSTOM_SPLASH_BANNER` to overlay an image on the splash. In production environments a modified splash screen can help communicate that Blender was configured for a particular studio or project. (blender/blender!132812)

## Other

- Videos can now be rendered using H.265/HEVC codec. (blender/blender!129119)
- EXR images that use DWAA/DWAB compression codec now have a Quality setting,
  previous behavior always saved them at 97 quality. (blender/blender!128790)
- "Render Audio" operator can now render to AAC (`.aac`) format, and also when
  using Matroska or Ogg format it can use Opus audio codec. (blender/blender!132877)
- Auto-save and quit.blend files are now always saved with compression. (blender/blender!132685, blender/blender!133694)

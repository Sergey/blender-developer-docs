# Blender 4.4: Grease Pencil

## Draw Mode

* Added "Stroke placements" back (from 4.2 LTS) (blender/blender@cde64dbdd5df60885407850d6cc4ce18a70dfefa)

## Sculpt Mode

* Added auto masking options back (from 4.2 LTS) (blender/blender@1eb39a868928735b8dcc63f7825546b4ff8f42b3)

## Operators

Restored operators that were missing in Blender 4.3.

* Added the "Fixed", "Sample", and "Merge" modes from the "Simplify" operator back (from 4.2 LTS)
  (blender/blender@8fb12839f254a4331ed24eee84b6241bd751af87)
* Added the "Set Start Point" operator back (from 4.2 LTS) (blender/blender@46cd7afcda3c58b27243f28a1d7f9be93fea4970)
* Added the "Lasso/Box" erase operators back (from 4.2 LTS) (blender/blender@612be514c63d8a5edb70b412f5e40839063a2516)
* Added conversion from Curve to Grease Pencil (from 4.2 LTS) (blender/blender@6191bc4f2208976371e841cf8e3904f8c728aba1)
* Added conversion from Mesh to Grease Pencil (from 4.2 LTS) (blender/blender@9d509de52a99a24d129f41beddaedc056d456fd5)
* Added the "Delete Breakdown Frames" operator in the dopesheet (from 4.2 LTS) (blender/blender@2d18046bbcc3968179dde2ee87ea0071e65f0d95)
* Added the "Paste by Layer" option back (from 4.2 LTS) (blender/blender@971f3e0699fc8b0cf47a4ff03732c4240869577a)

## Modifiers

* Previously, invisible layers were part of evaluated data, but this wasn't an intentional decision. Now, invisible layers are removed from evaluated data. This means for example that they are no longer listed in the context of Geometry Nodes. Technically, this is a breaking change. In the unlikely case that hidden layers were expected to be evaluated within modifiers, their visibility needs to be toggled. (blender/blender@https://projects.blender.org/blender/blender/pulls/133973)

## UI

* The preview icon of locked materials is no longer grayed out (blender/blender@7878b3908b7e54ec82a7053e4b30e31528914c8c).
* Properties of locked materials can be edited (similar to properties of locked layers) (blender/blender@d4d046a67334b7aacf85b5e7267b95dda10c76b2).
* In the dopesheet, layers and layer groups have a channel color (blender/blender@03b2fc744e6048127a7cac08eccb6bf2a6a7aa7f).

## Other

* The "Lock all" and "Unlock all" operators only worked for layers. Now this also affects layer groups (blender/blender@53255bcf6c5079af319849e5f24411bd5d397319).
* The "Hide Others" operator now also considers layer groups (blender/blender@b16e7dec28297929d1aa9dda691d8d0c868fa0b6).
* Vertex colors and layer tinting render in "Solid" shading mode.

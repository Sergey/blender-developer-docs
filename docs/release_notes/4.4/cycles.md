# Blender 4.4: Cycles

## OptiX Denoiser
The NVIDIA OptiX denoiser was updated to improve denoising quality.
(blender/blender@793918f2b1ea0f46a8b0977fda46d508c87cfcf1)

* Improvements in denoising quality can manifest as more consistent denoising,
more accurate colours, better retention of detail, and less denoising blotching.
In some situations denoising quality can decrease.
* This change only impacts users with older GPU drivers as newer drivers have
already switched to this denoiser type internally.

|Old Denoiser|New Denoiser (Scene by Metin Seven)|
|-|-|
|![](images/cycles_optix_denoiser_old.webp)|![](images/cycles_optix_denoiser_new.webp)|

## Sample Subset
To correctly distribute the blue noise sampling pattern across multiple computers,
the sample offset feature has been modified. There is now a Sample Subset toggle
with both an Offset and Sampling length. (blender/blender@f09fd9bdef3242a726667eba12f96514d33aba0a)

Scripts for distributed rendering using this feature need to be updated.

Please refer to the [Blender manual](https://docs.blender.org/manual/en/4.4/render/cycles/render_settings/sampling.html#advanced)
for details.

## GPU Rendering
* **Intel**: The minimum driver version required on Intel GPUs has increased to 101.6557 on Windows and 31740.8 on Linux. (blender/blender!133457)
* **NVIDIA**: Pre-compile kernels for GeForce RTX 50x0 series (Blackwell). (blender/blender!134170)

### AMD
* Added support for gfx12 GPU architecture (blender/blender!133129)
* Upgraded HIP-RT library to version 2.5 which solves some regression tests (blender/blender!133129)
* The minimum ROCm version required has increased to 6.0 (blender/blender!133129) (blender/blender!130153)

## Other
* **Open Shading Language**: Improved closure compatibility with MaterialX. (blender/blender!133612, blender/blender!133597, blender/blender!133575, blender/blender!133797, blender/blender!133845)
* **Baking**: Speed up Selected to Active. (blender/blender!128964)

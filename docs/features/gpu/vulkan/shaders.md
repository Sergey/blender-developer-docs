# Shaders

Shaders are programs that run directly on the GPU. There are graphical shaders and compute shaders.
A shader can have one or more shader modules associated to them like (vertex, geometric, fragment or compute).

## Compilation

Blender assemble GLSL code for the shader modules. This is done at runtime so it can determine the 
capabilities of the system it runs on and make some adjustments based on them. (`VKShader` and `VKShaderInterface` are
responsible to assemble the GLSL code).

The GLSL code is then compiled into a SPIR-V module using `shaderc. This is often called the
frontend compilation step as SPIR-V is still GPU independent. The SPIR-V binary is loaded 
into a Shader module. (`VKShaderCompiler` allows serial and parallel compilation and `VKShaderModule`
is a container that can store the binary SPIR-V module and compilation results).

When using a pipeline is constructed the SPIR-V binary has to be
converted into the machine language that the GPU understands. During this step the physical
properties (memory, available registries, etc) of a GPU are considered, but also specialization
constants are inlined. `VKPipelinePool` contains all unique pipelines that are constructed. When
a pipeline is created it is checked if that specific pipeline is already created 
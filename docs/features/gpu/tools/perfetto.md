# Perfetto

When launching Blender with the `--profile-gpu` option, it generates a `profile.json` file inside the execution directory in the [Trace Event Format](https://profilerpedia.markhansen.co.nz/formats/trace-event-format/), with CPU and GPU metrics based on GPU debug groups (`GPU_debug_group_begin/end`).

> At the time of writing, only OpenGL profiling is supported.

The profiles are best viewed at [perfetto.dev](https://ui.perfetto.dev/).

![perfetto.png](../../../images/perfetto.png)

# Tools

There are tools that help when developing inside the GPU or drawing code.
Here is a list of commonly used tools.

- Validate shaders when compiling Blender using [Shader builder](./shader_builder.md)
- GPU debugging [Renderdoc](./renderdoc.md)
- Use [Shader printf](./printf.md) to debug values and codeflow inside shaders without external tools.
- Simulate XR headsets without the hardware [Monado](./monado.md).
- Generate performance profiles and inspect them in [Perfetto](./perfetto.md).

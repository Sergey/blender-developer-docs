# Eevee

Eevee is a draw/render engine that uses game engine like techniques to provide fast rendering and feedback.

## Design principles

Eevee is build to be a realtime alternative to Cycles with more flexibility in its rendering pipeline. Frame render time is the first focus to be able to deliver fast animation render. Eevee is also used to draw the material preview mode for Cycles. Therefore the shading is tweaked to ensure that it follows Cycles as close as possible, but still be fast to render.

## Classes
- [Views](view.md)

## Modules
- [World](modules/world.md)
- [Lookdev](modules/lookdev.md)
- [Sphere Light-Probes](modules/lightprobe_sphere.md)
- [Shadow](modules/shadow.md)
- [Film](modules/film.md)
- [Pipeline](modules/pipeline.md)

## Pipelines
- [Background](pipelines/background.md)
- [World](pipelines/world.md)
- [DeferredProbePipeline](pipelines/probe.md)
- [Deferred](pipelines/deferred.md)

## Shaders


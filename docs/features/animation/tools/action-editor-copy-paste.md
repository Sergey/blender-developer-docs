# Animation Tools: Copy-Paste in Action Editor

This chapter describes how the copy-pasting of animation keys works in the Action editor.

<style>
  .copy {
    color:#0af;
  }
  .paste {
    color:#0fa;
  }
</style>

Since some parts of the behavior are determined by what was copied, and other parts depend on what is selected when pasting, colors are used to distinguish between those:

- <span class="copy">blue</span> indicates information at the time of copying, and
- <span class="paste">green</span> indicates information at the time of pasting.

Copy-Paste of animation keys in the Action editor works as follows:

1. <span class='copy'>Key selection</span> indicates what gets copied. For copying, the selection state of the channels (F-Curves, slots, etc.) is irrelevant.
2. Pressing Ctrl+C copies those keys, recording the F-Curves and Action slots they came from.
3. <span class='paste'>Channel selection (F-Curves, slots)</span> indicates what gets pasted, and where. For pasting, key selection is irrelevant.

Note that pasting should **never**:

- **Create new F-Curves**. Only already-existing curves should be pasted into.
- **Paste into invisible F-Curves**. Collapsed groups and other summary-like channels also count as "visible". But when "Only Show Selected" is enabled, the animation of non-selected objects/bones should not be touched.


## F-Curve Matching

The diagram below describes how target F-Curves (i.e. the ones being pasted into) are chosen, and how they are matched to the copied keyframes.

```mermaid
flowchart TD

  classDef copy stroke:#0af;
  classDef paste stroke:#0fa;
  classDef choice stroke-width:2px;

  num_fcurves_selected("How many F-Curves selected to paste into?"):::paste
  original_ri["RI of copied F-Curve(s)"]:::choice

  fcurves_copied("How many F-Curves copied?"):::copy
  slots_copied("Copied from how many slots?"):::copy

  ri_of_target["Selected F-Curve"]:::choice
  broaden["Broaden (F, P, I)<br>matching copied to selected F-Curves"]:::choice
  broaden_within_slot["Broaden (F, P, I)<br>but keep within slot<br>of the same name"]:::choice

  num_fcurves_selected -->|None| original_ri
  num_fcurves_selected -->|Single| fcurves_copied
  num_fcurves_selected -->|Multiple| slots_copied

  fcurves_copied -->|Single| ri_of_target
  fcurves_copied -->|Multiple| broaden

  slots_copied -->|Single| broaden
  slots_copied -->|Multiple| broaden_within_slot

```

The "Broaden" behavior works as follows. The first rule that matches anything "wins", and in that case subsequent rules are not visited any more.


1. `F`: **Full** match on the entire RNA data path and array index. So if you copied from `pose.bones["Root"].location[1]` and that is among the selected F-Curves too, this rule matches.
2. `P`: **Property** name match, ignoring things like bone names. So in the above example, it would also match if you have `pose.bones["Arm_L"].location[1]` selected because both target the `.location` property and array index `[1]`.
3. `I`: **Index** match, which only looks at the array index like `[1]`. So if you have Location X/Y/Z copied, and selected an F-Curve animating a "Y thing", it will paste the Y location keys there.

Historical note: This gradually broadening search was already implemented in Blender before slotted Actions were introduced.


## Slot Matching

The diagram below describes how the target slot is chosen when pasting.

```mermaid
flowchart TD

  classDef copy stroke:#0af;
  classDef paste stroke:#0fa;
  classDef choice stroke-width:2px;

  any_fcurve_selected(Any F-curve selected<br>to paste into?):::paste
  any_slot_selected(Any slot selected<br>to paste into?):::paste
  copied_from_slots(Copied from slots?):::copy

  slot_of_target_fcurves["Use slot(s) of<br>selected F-curves"]:::choice
  all_selected_slots["Duplicate into<br>all selected slots"]:::choice
  match_by_name["Match slots by name<br>(fallback to active)"]:::choice

  any_fcurve_selected -->|One or more| slot_of_target_fcurves
  any_fcurve_selected -->|None| any_slot_selected

  any_slot_selected -->|One or more| copied_from_slots
  any_slot_selected -->|None| match_by_name

  copied_from_slots -->|One slot| all_selected_slots
  copied_from_slots -->|Multiple slots| match_by_name

```

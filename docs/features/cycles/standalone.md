# Cycles Standalone

Cycles is integrated into Blender, but can also be built as a standalone
renderer. This can be used for testing purposes, but also allows an
integration into other 3D programs and pipelines.

Note though that the Cycles standalone application is a work in
progress, and is not ready for production usage. It is recommended to
use the C++ API directly when integrating into other software.

## Download

No precompiled binaries are available at the moment. Instead you can get
the source code (see below) and compile your own.

## Usage

Cycles needs to be started from a terminal: **cycles \[options\]
file.xml**

Use **cycles --help**, to get an overview of the available options.

When rendering with the GUI, you can press *H* to get an overview of
available shortcuts. You can pause, cancel or restart a render or try
the interactive mode, which allows basic camera navigation.

## For developers

#### Source code

The Cycles source code can be found
[here](https://projects.blender.org/blender/cycles). It's a standalone
repository which just contains the Cycles engine. You can find more info
and compile instructions there.

#### XML API

Cycles standalone has a XML API, which is used for mesh data and
shader/settings import. There is no real API documentation at the
moment, as the API is subject to change, but you can check the source
code to get an idea:

**app/cycles_xml.cpp:** Main XML reading API. *xml_read_file()* is the
main function here, *xml_read_scene()* calls the different XML loading
routines for Geometry, Shaders and Settings then.

**render/nodes.cpp:** All shader nodes are declared here, check the
strings in the *add_input()* and *add_output()* calls, these are the
names used for shader parameters in XML (such as *Scale* or *Radius* for
the Subsurface Scattering node).

#### Get Involved

Please see the current standalone code and its API as a proof of concept
only.

We hope to get feedback and/or patches from developers, who are
interested in implementing Cycles into other applications. Please get in
contact via [chat.blender.org](https://chat.blender.org/#/room/#module-render-cycles:blender.org) or write a comment on
our developer site if you like to get involved:
<https://projects.blender.org/blender/blender/issues/38279>

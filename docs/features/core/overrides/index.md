# Overrides

Override is a generic term used to design the usage of a reference data,
with some local modifications to it.

Key ideas:

- When the reference data changes, those changes also get applied to its
  overrides, as much as possible.
- A reference data can be used by several overrides at the same time.

In Blender currently we have [Library
Overrides](library/index.md), designed
to replace and extend the possibilities of the old proxy system.

A future project is to also have 'Dynamic Overrides', that
could be used on local data as well as directly on linked data.

------------------------------------------------------------------------

Archived for historical interest:

- [Library Overrides: Functional Design](library/functional_design.md)
- [Experiment: Using Library Overrides for USD Mapping](library/usd_mapping.md)

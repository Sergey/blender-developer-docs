# Implicit Sharing

Implicit sharing is a technique that reduces memory usage and improves performance by removing unnecessary data copies. For example, traditionally when copying a mesh, all the attribute arrays had to be copied as well. This is wasteful because now we have the same arrays twice. With implicit sharing the copy is not necessary, because both meshes can reference the same attribute arrays.

Sharing is possible by "attaching" sharing info (`ImplicitSharingInfo`) to the data that should be shared. This is essentially just a user count. The most important aspect is that when there is a single user the data is mutable. However, when there are more users the data is read-only. If some code wants to edit data with multiple users, it has to be copied first.

```mermaid
graph
  mesh[Mesh] -->|owns| sharing[Sharing Info, Users: 1]
  mesh -->|write-access| data[Attribute Data]
  sharing -->|owns| data
```

```mermaid
graph
  meshA[Mesh] -->|owns| sharing[Sharing Info, Users: 2]
  meshA -->|read-access| data[Attribute Data]
  meshB[Mesh Copy] -->|owns| sharing
  meshB -->|read-access| data
  sharing -->|owns| data
```

APIs that support implement sharing typically have two methods to access the same piece of data. One that gives read-only access (e.g. `Mesh.vert_positions() -> Span<float3>`) and one that gives write access and typically has `_for_write` in the name (e.g. `Mesh.vert_positions_for_write() -> MutableSpan<float3>`).

For raw data, the sharing info and the actual data are often separate allocations. However, in some cases it's benefitial to put both into the same allocation. For example, this is done for `GeometryComponent`. Here the entire geometry component can be implicitly shared between multiple geometry sets.

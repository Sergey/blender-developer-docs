# Viewer Node

The Viewer node is used to debug a node-tree while editing it. It is
present in the following editors:

- Compositor
- Geometry Nodes

Although each editor has their own peculiarities, the idea is the same,
the node allows the node-tree to be inspected at that point, bypassing
further evaluation steps.

![](../../images/Viewer_Node.png)

## Viewing the Viewer Node

Although the Viewer node is created and made active in the node editors,
its result is available in different places:

- Compositor (backdrop view)
- Image Editor
- [Spreadsheet Editor](../interface/editors/spreadsheet.md)
- Viewport (planned for Blender 3.4)

In order to calculate the result of the Viewer node the node tree needs
to be computed separated from the main evaluated result. In other words,
just viewing the Viewer node should trigger its evaluation.

!!! Performance
    It is very possible to only evaluate the viewer data if any editor is
    in need of the data to display. The downside of this is that it means
    users may need to wait for the data to be computed when the viewport is
    to show the Viewer node. This is not different than waiting for the
    dependency graph when switching View Layers.</p>

## Active Viewer Node

**Changes planned for 3.4, starting with Geometry Nodes.**

The information about which Viewer node is active is stored per
workspace. This way this is treated as user interface data.

So for every window there is only one active Viewer node per type
(compositor, geometry nodes) which is displayed in the relevant editors.

Within a node editor it should be possible to quickly activate a Viewer
node (e.g., by clicking on it, or creating a new one with
Ctrl+Shift+Click in a node). Deactivating the node should also be
possible from the same place (e.g., by clicking again on it).

## Quality of Life Improvement

When a user activates a Viewer node, or connect an output to it, it can
be assumed that they want to see the result of this Viewer node. As such
Blender can automatically make sure the other open editors (e.g.,
Viewport) enable viewing the Viewer node results.

For Geometry Nodes and the viewport the same behavior will apply (in
Blender 3.4).

## Future Plans

- Compositor Viewer node result in the viewport (for the new real-time
  compositor).
- Multiple Viewer nodes: In the future Blender can also support multiple
  (properly named) Viewer nodes to be visible in the editors for a more
  advanced debug setup.
- Support for Viewer node for shading node-trees.

# Asset Editing

Under certain conditions, it is possible to edit assets that are not stored in the currently open
file. This happens through special asset system managed files (as opposed to normal user managed
files), and the `BKE_asset_edit.hh` API. Every such editable asset is saved in a single such file,
together with its dependencies and external files packed.

Only brush assets support this currently.

## Asset System Files (`.asset.blend` Files)

Asset system files are the only files that assets can be stored to from any Blender instance,
without opening the file itself.

These files are indicated by the `G_FILE_ASSET_EDIT_FILE` flag stored in the file's "global" data
section, and use the `.asset.blend` file extension. Only files that use both the extension and the
flag are considered asset system files, so users may still use the `.asset.blend` extension for
normal files. The extension is an indicator for users. Plus, Blender doesn't have to open open all
files to check for the flag; it can immediately discard files without the extension.

Instead of updating asset system files to save changes, they get regenerated entirely. This keeps
the updating process simple, and avoids many potential issues with partially changing .blend file
data, especially if they could contain user edited data.

The downside is, that user made changes to these files could get lost easily once the asset system
regenerates the files. For this reason, asset system files get special protections: Users can open,
but not save them (but use "Save As" to create a new, normal file). The UI shows some warnings to
make users aware of this.

# Extension Listing API

Extension repositories in the Blender preferences point to a remote URL. This URL returns the list of extensions that are available to be installed. As an example, see <https://extensions.blender.org/api/v1/extensions/>.

The specification for this Extension Listing API follows below. When newer versions of the API are added, Blender should be able to keep supporting previous versions.

| Version           | Blender Version Initial                      | Blender Version Final                        |
| ----------------- | -------------------------------------------- | -------------------------------------------- |
| [v1](v1.md) | [4.2.0](../../../release_notes/4.2/index.md) | - |

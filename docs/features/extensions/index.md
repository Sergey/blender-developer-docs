# Extensions

[Extensions Platform](https://extensions.blender.org/) integration and design topics.

* [Manifest Schema](schema/index.md)
* [Listing API](api_listing/index.md)
* [CI/CD](ci_cd.md)
* [Moderation](moderation/index.md)
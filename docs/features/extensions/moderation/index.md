# Moderation

To help with extensions moderation:

* Read over the [Guidelines](guidelines.md).
* Visit the [Approval Queue](https://extensions.blender.org/approval-queue/).
* Help testing and reviewing the Awaiting Review extensions (ideally in a safe virtual environment).
* For any questions, join the team on the [#extension-moderators](https://chat.blender.org/#/room/#extensions-moderators:blender.org) room.
* Remember to be polite and assume good intentions all around :)

---

* [Canned Responses](canned_responses.md)
* [Guidelines](guidelines.md)

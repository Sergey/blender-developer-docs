# Rendering

Sequencer uses mostly CPU to produce images (exception is text and scene
strip type). Because of this, only very basic image processing is viable
for real time preview and many functions are optimized to do minimum
possible amount of work.

## Rendering context

`SeqRenderData` data structure is used to set up how image should be
rendered. Most important fields are `rectx` and `recty` to define
image resolution of final image. Fields `use_proxies` with
`preview_render_size` are useful to configure what proxy size should
be used or to downscale image resolution for better performance. See
`ESpaceSeq_Proxy_RenderSize`.

## Strip stack rendering

Strips are rendered in sequence from bottom to top according to their
channel (`seq->machine`). This is done in function
`seq_render_strip_stack()` where strips are traversed from top to
bottom first. If any strip can produce image on it's own, or it is
stored in cache, traversing direction is reversed and strips are blended
according to their blend mode. Result of strip stack rendering is always
1 image, which is stored in cache separately as final image.

## Individual strip rendering

This is handled by function `seq_render_strip()`. First cache is
checked if preprocessed or raw image exists. If no image is found in
cache, function `do_render_strip_uncached()` dispatches rendering
function based on strip type. This should give "raw" image which may not
have correct resolution and does not have any image processing applied
like modifiers or transformation. To apply required image processing,
`seq_render_preprocess_ibuf()` is used. If no processing is required,
image is returned as-is.

## Preprocessing stage

This stage is mainly used to apply corrections on original image. This
includes:

- Deinterlacing
- Transformation and cropping
- Flipping
- Adjusting saturation
- Byte -> float conversion
- Pixel value multiplication
- Modifiers

Due to sequencer design, this stage is also used, when image size does
not match rendering context size. This is because it can be done during
transformation along with user defined properties at once and therefore
much faster than doing these steps separately.

Note: Effect strips can also use preprocessing stage. Strips that
support Multiviev rendering need to apply preprocessing stage for each
rendered image.

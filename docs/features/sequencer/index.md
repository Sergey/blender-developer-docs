# Video Sequence Editor

## Code Layout

VSE code is split between "core" sequencer code and "editor" code.

Sequencer core is under [`source/blender/sequencer`](https://projects.blender.org/blender/blender/src/branch/main/source/blender/sequencer)
and handles rendering of strips, modifiers and effects, proxies, caching, prefetching, and implements the actual logic of sequencer operations.

Sequencer editor is under [`source/blender/editors/space_sequencer`](https://projects.blender.org/blender/blender/src/branch/main/source/blender/editors/space_sequencer)
and implements sequencer preview and timeline area UIs, operators, drag & drop, etc.

Generally, code should try to not access (and especially modify) sequencer related DNA struct fields directly.
Some of them have complex relationships with other fields, or their modifications should always be followed
by some code to update caches or related data structures.

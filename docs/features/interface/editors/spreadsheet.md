# Spreadsheet Editor

The spreadsheet editor allows users to debug and navigate the data in
the Blender file. That includes object geometry as well as scene data
such as objects.

**Core features**

- Inspection of data.
- Fast display of missions of rows.
- Write and read-only.
- One data-set at a time
- Multiple view modes

![Spreadsheet editor big picture](../../../images/Spreadsheet-High-Level.png){width=550, align=right}

## View Mode

**What is the Context**

It always shows all the available data for the active context.

- To compare different datasets users need to open multiple editors

It shows different data (columns) based on the mode.

- All the available attributes
- All the lamp RNA properties

It shows original (editable), driven, overridden and linked data.

  
**Example of View Modes**:

- Object
- Geometry
- Modifier Geometry
- Node Geometry
- Scene Objects
- Collection Objects

## Data Sets

**What to show**

Outliner-like region to select which data to show for a given view mode.

| Object    | Geometry               | Scene                       |
|-----------|------------------------|-----------------------------|
| Bones     | Mesh → Point           | Objects (Object Properties) |
| Materials | Mesh → Edge            | Lamps (Lamp Data)           |
| Shapekeys | Volume → Voxel         | Cameras (Camera Data)       |
|           | Curve → Splines        |                             |
|           | Curve → Control Points |                             |
|           | Grease Pencil → ...    |                             |

The amount of properties for each one of these view modes is a lot. The
idea is to show only a handful of hand picked ones, and let users expose
(or hide) more properties.

## Filters

**What to hide**

Rows

*The row filters are aimed for continuously debugging and inspecting the
data. As such they are not designed to be easily dismissed. But instead
to be a constant presence in the side bar where users can tweak values
and see more/less data.*

- Selected (Vertices, Objects)
- Cells Value Outside Range

Columns

*It is important to keep the separation of UI and user data, and to only
store "strings" in the editor, when referring to attributes. This way
the same spreadsheet editor can be used for different objects and still
have the same filters working.*

- Column Name
- Category

> NOTE: **Status bar**
> When filtering the data it is important to tell users what is the
information that is being left out. The status bar inside the editor is
intended for that and inspection of selected cell.

## Mockups

![Spreadsheet editor mockup](../../../images/Spreadsheet-mockup.png){width=1024}

![](../../../images/Spreadsheet-editor-display-filters.png){width=500}

![](../../../images/Spreadsheet-Editor-Rules-Mockup.png){width=800}

  

## User Stories

- I as a <u>teacher</u> want to <u>see/edit vertices and weightgroup</u>
  to <u>know what happens when I weightpaint</u>.
- I as a <u>lighting artist</u> want to <u>see/edit the power of all the
  lights</u> to <u>prevent overexposure</u>.
- I as a <u>rigger</u> want to <u>find which vertices belong to which
  group</u> to <u>find why a vertex moved</u>.
- I as a <u>set dresser</u> want to <u>see the random value of a
  point</u> to <u> debug the rotation of instances</u>.
- I as an <u>animator</u> want to <u>set "drivers" visually by selecting
  values from different objects in the spreadsheet</u> to <u>inspect the
  relation between the objects</u>.
- I as a <u>set dresser</u> want to <u> see if my geometry has
  points</u> to <u>know when instances become real to optimize my
  nodetree</u>.

# Icons

There are multiple types and classes of icons in Blender:

1.  UI Icons - Small icons used throughout the interface.
2.  Alert Icons - Large icons used in dialogs and confirmations.
3.  File System Icons - Large icons to represent file system items while in File Browser thumbnail view.
4.  Miscellaneous Icons - Large icons and images used for specialized purposes.
5.  Brush Icons - PNG images used for the built-in brushes (sculpt, Grease Pencil, etc)
6.  Tool Icons - Medium-sized icons used for tool, in the 3D View Toolbar

## UI Icons

![Ui Icons](../../images/UI_icons.png)

These are the small icons used throughout the interface, including in menus and on buttons.

These are scalable vector SVG images found in the `datafiles\icons_svg` folder. During compile time these source files are embeded into the Blender executable as XML data. At runtime they are treated similarly to text glyphs in that they are rasterized to the requested bitmap size on first use and then saved to our text system's glyph cache. This allows them to be shown optimally at any size.

### Design

Generally these icons will be a design comprising 1400x1400 units in
a document of size 1600x1600, using a grid size of 100 units. Although
SVG documents are unitless, these are considered "pixel" units in
programs like Inkscape.

More specifically, all the icons have one grid (100 units) of padding on
a document that is no taller than 1600 units, but of any width. Narrow
designs should be just wide enough for the content plus one grid of padding
to the left and right.  Short designs should similarly be just as tall as
needed plus one unit of spacing above and below.

In most uses the icons will be drawn centered in their available space. If
you have multiple icons that need to line up with each other but differ in
size, and aren't centered, then give each a document width that allows this.
But otherwise there should be no more than one grid unit of empty space
around icons. Although this design seems less convenient than a fixed document
size, this allows for better centering as resolution scale increases, non-square
large designs, and allows for the inference of actual design extents.

For icons meant for general (small) usage, the minimum feature size is one
grid (100 units) and lines should be exactly that wide, aligned to the grid.
These are meant to be viewed at very small sizes so are purposefully simple
and chunky. These designs can include curves and circles, but these should
also line up the grid as best as possible.

The icons should comprise full white areas and strokes over transparency.
Areas of lesser importance should have _lowered opacity_. This way the icons
can be recolored (and even made black) and will keep the same contrast.

If in doubt just copy the style and construction of an existing icon. See the
[Human Interface Guidelines](human_interface_guidelines/icons.md) on best
practices to design Blender icons. Before adding new icons please
consult with members of the
[User Interface module](https://projects.blender.org/blender/blender/wiki/Module:-User-Interface).

### Technical details and toolchain considerations
This use of SVG icons directly within Blender is new to us. Therefore some details
and recommendations may change as we use them.

All of the current icons have been optimized to be as small as possible (by
a program called SVG Cleaner, a GUI for [svgcleaner](https://github.com/RazrFalcon/svgcleaner)).
This results in dramatically smaller file sizes.

However, since these icons are white on transparent they can be impossible to see
when you first load them into a program like Inkscape.  Because of that we have added
in the bare minimum boilerplate to allow them to show a contrasting background and grid lines.
This can be copied from an existing icon, but the following shows the start of a typical
icon XML:

```
<svg height="1600" viewBox="0 0 1600 1600" width="1600" xmlns="http://www.w3.org/2000/svg" xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape" xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"><sodipodi:namedview pagecolor="#303030" showgrid="true"><inkscape:grid id="grid5" units="px" spacingx="100" spacingy="100" color="#4772b3" opacity="0.2" visible="true" /></sodipodi:namedview><g fill="#fff"><g ... </g></g></svg>
```
Eventually we will probably creating icons with specific id names on groups so you
don't want to run an optimization that shortens IDs.  It is also possible that we
may one day add our own namespace for custom attributes.

### How to add a new icon
1. Add the SVG file to the `release/datafiles/icons_svg` folder
    - Name should be lowercase, **not** start with "icon", no spaces (underscore instead)
2. Add the file name to /source/blender/editors/datafiles/CMakeLists.txt
    - In a list called `SVG_FILENAMES_NOEXT`. The order does not matter, but respect the current alphabetical order.
    - Just the file name, in lowercase, without any trailing ".svg".
3. Define the icon in source/blender/editors/include/UI_icons.hh
    - Anywhere between `DEF_ICON(NONE)` and `DEF_ICON_BLANK(LAST_SVG_ITEM)`
    - The order doesn't matter, but find somewhere that make sense. Keep on/off pairs together.
    - Most icons should be defined with `DEF_ICON` but some are colored by the theme based on their category.
       - `DEF_ICON_SCENE(<name>)`
       - `DEF_ICON_COLLECTION(<name>)`
       - `DEF_ICON_OBJECT(<name>)`
       - `DEF_ICON_OBJECT_DATA(<name>)`
       - `DEF_ICON_MODIFIER(<name>)`
       - `DEF_ICON_SHADING(<name>)`
       - `DEF_ICON_FOLDER(<name>)`
4. Compile Blender.
5. Your new icon will be accessible just like regular icons.
    - `layout.prop(sc, "example_prop", text="My Prop", icon='EXAMPLE_NAME')`

## Alert Icons

![](../../images/Alert_Icons.png)

These are large icons for use in dialogs, confirmations, info, and alert boxes.

These are made and used identically to other UI icons, but are designed to be used at larger sizes.
They should still be made within the same 1600x1600 document, should have a slightly less coarse design, with line
widths about 75 units and feature sizes no smaller than about 50 units.
- cancel_large.svg - Stop symbol to indicate error conditions or important warnings
- warning_large.svg - Hazard symbol used for most normal warning conditions
- question_large.svg - Question mark
- info_large.svg - Used for informational text.

## File Browser Icons

![](../../images/Prvicons.png)

These are large icons for use in the File Browser. They represent file system items while in thumbnail views.

These are made and used identically to other UI icons, but are designed to be used at larger sizes.
They should still be made within the same 1600x1600 document, should have a less coarse design, with minimum
feature sizes (like line widths) about half a grid (50 units) and no smaller than about a quarter (25 units).
- file_large.svg - File system items (a smaller icon is added to indicate file type)
- file_folder_large.svg - Folder/Directory
- file_parent_large.svg - Parent of current folder
- disc_large.svg - Optical drives
- disk_drive_large.svg - Local internal storage
- external_drive_large.svg - Local externally attached storage
- network_drive_large.svg - Remove network shares

## Miscellaneous Icons

These are large icons used for specialized purpose.

These are made and used identically to other UI icons, but are designed to be used at larger sizes. These
are still designed for the same grid size, but do not have any feature size limitations.  Non-square items
should still fit vertically within the 1600 unit high document, but can be any width.
- blender_large.svg - Blender logo symbol
- blender_logo_large.svg - Blender text logo

## Tool Icons

![](../../images/Geomicons.png)

This type of icon, used for toolbar tools, is larger and scalable.

These are created in a blend file and exported as vertex data, color in
8-bit RGBA and position scaled to 8-bit (so an effective resolution of
256x256). The display process involves rasterizing (with CPU) at the
requested size and caching the results for performance. One flaw is that
this single cache is rebuilt if the size changes, so this will happen
constantly if you show multiple sizes simultaneously. We could display
them faster without caching if they were treated as 3D geometry and used
the GPU, but the jittering required for antialiasing messes up the
layers of opacity used in their design. Another issue is that the only
color information is the RGBA values, so we can't make highlight colors
consistent through theming. So this area could use improvement.

You can find the sources of these icons in `release/datafiles/assets/icons/toolbar.blend`.
This is the [blender-assets](https://projects.blender.org/blender/blender-assets) repository,
used as a Git submodule in the Blender repository.

### Creating / Modifying New Icons
- Object names are used to generate filenames: e.g. `ops.gpencil.draw.eraser`
  will be written to `${BLENDER}/release/datafiles/icons/ops.gpencil.draw.eraser.dat`
- An objects children are recursively included when exporting to allow easily sharing
  geometry between icons.

  These may be transformed as long as they remain in the icon bounds.

- Material colors are multiplied by vertex colors when exporting.
- Geometry depth is used so closer faces draw on top.
- Vertex alpha can be used.
- Try to keep the file well organized.

### Exporting Icons

#### Requirements
- Objects must be meshes to be exported
- Icon objects and any shared children must be in the **Export** collection to be considered for exporting.
- Icon detail should be kept low since more complex icons will take more time to draw (while cached,
  avoid unnecessary overhead).

After the file has been committed to the `blender-assets` repository, the icons can be generated
for use inside Blender via the following process.

- Run: `make icons_geom`
- Commit: `${BLENDER}/release/datafiles/icons/<icon_name>.dat` and
  `${BLENDER}/source/blender/editors/datafiles/CMakeLists.txt`

## Built-in Assets

When adding matcaps, HDRIs or other EXR files, they must be compressed
to be smaller before being committed using the following command:

`oiiotool in.exr --ch R,G,B -d half --compression dwab -o out.exr`

## Blender Icon Set for Penpot

There is an official Penpot Blender Icon Set to help with mockups.

For more details check the [UI Toolkit documentation](../../handbook/design/toolkit/icons.md).
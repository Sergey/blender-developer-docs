# Experimental Features

When Developer Extras are enabled in the Interface tab, a new
Preferences experimental tab is shown. It contains non-production ready
features that need feedback and design iterations.

![](../../images/Dev-Interface-ExperimentalFeatures.png)

!!! note "Rule of Thumb"
    Any experimental features should not change how a file renders,
    regardless of the user preference. These preferences are only to be used
    to show or hide features in the user interface, and not to disable
    functionality.

## DNA Changes and Check

The DNA struct `UserDef_Experimental` contains the variables that
enable/disable experimental features in the user interface:

```  c
typedef struct UserDef_Experimental {
  char use_tool_fallback;
  char virtual_reality;
  char _pad0[6];
} UserDef_Experimental;
```

Although this is not the most efficient way of handling multiple
variables, it is very easy to remove a setting when it is no longer
experimental.

Use the macro `USER_EXPERIMENTAL_TEST` to test whether the features
are enabled.

## RNA Changes

In the `rna_userdef.cc` file you need to create a new property in
`rna_def_userdef_experimental`, but its get function can be
auto-generated with the macro `RNA_USERDEF_EXPERIMENTAL_BOOLEAN_GET`:

``` c
RNA_USERDEF_EXPERIMENTAL_BOOLEAN_GET(use_tool_fallback)

(...)

static void rna_def_userdef_experimental(BlenderRNA *brna)
{
  StructRNA *srna;
  PropertyRNA *prop;

  srna = RNA_def_struct(brna, "PreferencesExperimental", NULL);
  RNA_def_struct_sdna(srna, "UserDef_Experimental");
  RNA_def_struct_nested(brna, srna, "Preferences");
  RNA_def_struct_clear_flag(srna, STRUCT_UNDO);
  RNA_def_struct_ui_text(srna, "Experimental", "Experimental features");

  (...)

  prop = RNA_def_property(srna, "use_tool_fallback", PROP_BOOLEAN, PROP_NONE);
  RNA_def_property_boolean_sdna(prop, NULL, "use_tool_fallback", 1);
  RNA_def_property_boolean_funcs(prop, "rna_userdef_experimental_use_tool_fallback_get", NULL);
  RNA_def_property_ui_text(prop, "Fallback Tool Support", "Allow selection with an active tool");
  RNA_def_property_update(prop, 0, "rna_userdef_update");
}
```

## Operators

Sometimes we have an operator which is experimental, so it will not be
in the user interface at all times. However its `poll()` function
should not be affected by the experimental value.

This way we can make sure anyone building an add-on that calls this
operator will have predictable results regardless of the user
preferences. The downside is that this operator will be visible in the
Operator Search.

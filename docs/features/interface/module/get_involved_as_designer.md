# Getting Involved as a Designer

The UI module is hoping to find experienced UI/UX designers (and icon
designers) to become active team members. It may not be exactly clear
how to get involved, so this page should give some instructions and
advice.

Note that this page isn't a job offer. However, if a designer shows that
they can bring significant contributions to the project, it's certainly
possible for the UI module to request development grants from the
Blender Foundation.

## Communication

Have a look at the [main module
page](https://projects.blender.org/blender/blender/wiki/Module:%20User%20Interface)
for information on who does what in the module, as well as a list of
contact channels.

## Expectations

UI and UX are opinionated fields. It's relatively easy to come up with
design ideas, or to identify existing design problems. There are plenty
of both, that's not really where help is needed.
Blender development is highly active; there is a constant stream of new
UI design challenges every day. At the same time, a number of big
projects bring their own complex design problems. A designer who picks
up these daily tasks and shows "good taste" and reason in addressing
them would add great value to the Blender project. Further, help is
needed to work out the [Human Interface
Guidelines](../human_interface_guidelines/index.md) for Blender, as well
as improving the general design processes.

We hope to find designers who want to understand and embrace the
identity of Blender, together with its design philosophy. *Why are we
making Blender? Who are we making it for?* A "good" designer (whatever
that means) should explore such questions first. A lot is possible on
that basis. You can take part in shaping the future of Blender from
there.

## Getting Started

**Basic platforms to know:**

- Most development and design work happens in the [UI module project on
  projects.blender.org](https://projects.blender.org/blender/blender/wiki/Module:%20User%20Interface).
  - The
    [workboard](https://projects.blender.org/blender/blender/projects/16)
    contains a huge list of tasks.
- The WIP [Human Interface
  Guidelines](../human_interface_guidelines/index.md).
- The [UI devtalk
  forum](https://devtalk.blender.org/c/user-interface/23) is a source of
  additional information on everything usability and UI design. If you
  want to get involved with design/development, it's a good idea to post
  questions there as a way to preserve the conversation for other people
  facing the same problems.
- For more informal chatting use [chat.blender.org](https://chat.blender.org/),
  and the [#module-user-interface](https://chat.blender.org/#/room/#module-user-interface:blender.org)
  room (for discussions on concrete development topics with the
  module - more strictly moderated).

**Possible first tasks:**

- Help out in design review and testing of
  [pull requests](../../../handbook/contributing/pull_requests.md).
  You can ask for test builds if you don't compile Blender yourself.
- Conduct usability tests or user interviews, build personas, write user
  stories, ...
- Create an analysis of (parts of) the current UI and/or its history.
  While this doesn't bring immediate deliverables to a Blender release,
  it will help you understand context, and it shows that you are
  understand the importance of such context for quality design work.
- Contribute to (ideally active) design tasks from the huge backlog of
  [tasks on the
  workboard](https://projects.blender.org/blender/blender/projects/16).
- Create proposals to address existing design problems in Blender and
  post them in the [UI section on
  devtalk](https://devtalk.blender.org/c/user-interface/23). Please ask
  for permission in the module chat first.

**General advice:**

- Familiarize yourself with the [Blender UI design
  paradigms](../human_interface_guidelines/paradigms.md) and other
  guidelines.
- Get involved in projects which you think are important for Blender and
  where you can be of most help, not just projects you find the most fun
  to work on. This shows good judgement when picking priorities.
- Be patient. UI developers are busy with various projects so they are
  often not available for elaborate feedback. Your involvement may help
  addressing this on the longer run!
- Don't spend a lot of time on design tasks that were not approved. Some
  people like to have initial working version before they present it,
  but this is always at your own risk. Plenty of times did
  designs/patches have to be rejected after somebody put significant
  work into them. This is quite awkward and not a nice experience for
  anybody involved.
- While not strictly necessary for design work, it's useful to have your
  own [Blender build set up](../../../handbook/building_blender/index.md), and to know
  how [checkout pull requests](../../../handbook/contributing/pull_requests.md)

markdown-callouts~=0.3.0
markdown-captions~=2.1.2
mkdocs-material~=9.5.29
mkdocs-literate-nav~=0.6.1
mkdocs-section-index~=0.3.8
pygments~=2.16.1
mdx_truly_sane_lists~=1.3.0
mkdocs-glightbox~=0.3.5
mkdocs-redirects~=1.2.1
